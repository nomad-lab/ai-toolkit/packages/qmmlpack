# qmmlpack
# (c) Matthias Rupp, 2006-2017.
# See LICENSE.txt for license.

"""Representations.

Coulomb matrix.
Many-body tensor representation.

Part of qmmlpack library."""

import numpy as np
import numbers
import itertools

from .lib import QMMLException, _qmml, is_sequence
from .distances_pnorm import distance_euclidean
from .numerics import lower_triangular_part

#  ########################
#  #  Auxiliary routines  #
#  ########################

def _linearize_raw_input(z, r, b = None):
    """Linearizes raw input for processing by C/C++ routines.

    Parameters:
      z - Atomic numbers of atoms in dataset (list of lists)
      r - Atom coordinates in Angstroms (list of list of [x,y,z] lists)
      b - Basis vectors (list of 3x3 matrices) (three row vectors)

    Returns:
      (zz,rr,bb,ssizes,sinds) where zz, rr, bb are one-dimensional arrays and ssizes and sinds are sizes (in atoms) and starting indices (0-based) for each system

    Remarks:
      QMMLException is raised if input has wrong format.
    """

    # Check format
    if not is_sequence(z): raise QMMLException("Invalid input for atomic numbers: z must be a sequence")
    if (len(z) > 0) and (not is_sequence(z[0])): raise QMMLException("Invalid input for atomic numbers: z must be a sequence of sequences")
    ns = len(z)  # number of systems

    if not is_sequence(r): raise QMMLException("Invalid input for atom coordinates: r must be a sequence")
    if (len(r) > 0) and (not is_sequence(r[0])): raise QMMLException("Invalid input for atom coordinates: r must be a sequence of sequences")
    if (len(r) > 0) and (len(r[0][0]) != 3): raise QMMLException("Invalid input for atom coordinates: r must be a sequence of sequences of coordinates")
    if len(r) != ns: raise QMMLException("Invalid input: number of atomic numbers and number of atom coordinates do not match")

    if not(b is None):
        if not is_sequence(b): raise QMMLException("Invalid input for basis vectors: b must be a sequence")
        if len(b) != ns: raise QMMLException("Invalid input: number of atomic numbers and number of basis vectors do not match")
        if len(b) > 0:
            if not is_sequence(b[0]): raise QMMLException("Invalid input for basis vectors: b must be a sequence of sequences")
            if len(b[0]) != 3: raise QMMLException("Invalid input for basis vectors: b must be a sequence of three vectors")
            if (not is_sequence(b[0][0])) or (len(b[0][0]) != 3): raise QMMLException("Invalid input for basis vectors: b must be a sequence of three vectors of length three")

    # sizes and indices
    ssizes = np.empty((ns), dtype=np.int_)
    sinds = np.empty((ns), dtype=np.int_)
    ind = 0; tna = 0; # total number of atoms
    for i in range(ns):
        ssizes[i] = len(z[i])
        sinds[i] = ind
        ind = ind + ssizes[i]
        tna = tna + ssizes[i]

    # zz, rr, bb
    zz = np.fromiter( itertools.chain.from_iterable(z), np.int_ )
    rr = np.fromiter( itertools.chain.from_iterable( [itertools.chain.from_iterable(r0) for r0 in r]), np.float )  # .reshape( (tna, 3) )
    bb = np.empty([]) if b is None else np.fromiter( itertools.chain.from_iterable( [itertools.chain.from_iterable(b0) for b0 in b]), np.float)

    return (zz,rr,bb,ssizes,sinds)

#  ####################
#  #  Coulomb matrix  #
#  ####################

def coulomb_matrix(z, r, unit='Angstroms', padding=False, flatten=False, sort=True, post=None):
    """Computes Coulomb matrix representation of molecules.

    If you use this representation, please cite

    Matthias Rupp, Alexandre Tkatchenko, Klaus-Robert Müller, O. Anatole von Lilienfeld:
    Fast and Accurate Modeling of Molecular Atomization Energies with Machine Learning, Physical Review Letters 108(5): 058301, 2012. DOI 10.1103/PhysRevLett.108.058301

    Parameters:
      z - Atomic numbers of atoms for each molecule.
      r - Cartesian coordinates of atoms for each molecule.

      unit [Angstroms] - Unit of atom coordinates ('Picometers', 'Angstroms', or 'BohrRadius', with synonyms 'pm', 'A', 'a0').
      padding [False] - Pads matrix with rows and columns of 0s for a given total number of rows/columns. If fewer than number of atoms, matrix is trimmed.
      flatten [False] - True vectorizes matrix by concatenating rows of lower triangular part. Passing k effectively returns flatten[lower_triangular_part(...,k).
      sort [True] - If and how to sort the matrix. False does not sort, resulting in input order.
        - True sorts by row norms.
        - Ordering function p(cm) returning sorting permutation (like numpy.argsort()).
      post [None] - Post-processing function applied to each Coulomb matrix entry (after sorting), called with parameters cm_ij, Z_i, Z_j, d_ij, i, j.

    Returns:
      Coulomb matrices for

    Remarks:
      Order of steps is matrix calculation, sorting, post processing, padding, flattening.
      In particular, post processing is applied after sorting.

    Examples:
    >>> coulomb_matrix([[1,1]], [[[0,0,0],[0.74,0,0]]]) == [[0.5,0.715],[0.715,0.5]]
    >>> coulomb_matrix([[1,1]], [[[0,0,0],[74,0,0]]], unit='Picometers', padding=3, flatten=True) == [0.5,0.715,0.5,0.,0.,0.]
    """

    # parameters processing
    (cf, padding, vect, vectk, sort, postf) = coulomb_matrix_parse_options(z, r, unit, padding, flatten, sort, post)

    # calculation of Coulomb matrix
    return np.asfarray( [molecule_coulomb_matrix(zz, rr, cf, padding, vect, vectk, sort, postf) for (zz,rr) in zip(z,r)] )

def _coulomb_matrix_default_sortf(*args):
    """Implementation of default sorting of Coulomb matrix."""
    if len(args) == 1: return list(reversed(np.argsort(np.linalg.norm(args[0], axis=1))))
    elif len(args) == 3: return np.argsort(args[0][args[2]])
    else: raise QMMLException("Internal error (invalid number of arguments to default sorting function of Coulomb matrix representation.)")

def coulomb_matrix_parse_options(z, r, unit, padding, flatten, sort, postf):
    """Processes arguments for Coulomb matrix representation."""

    # z
    if not is_sequence(z): raise QMMLException("Invalid atomic number specification for Coulomb matrix representation.")
    if len(z) == 0: return []
    if not is_sequence(z[0]): raise QMMLException("Invalid atomic number specification for Coulomb matrix representation.")

    # r
    if not is_sequence(z): raise QMMLException("Invalid atom position specification for Coulomb matrix representation.")
    if len(r) != len(z): raise QMMLException("Number of atomic numbers and atom coordinates does not match.")
    if not is_sequence(r[0]): raise QMMLException("Invalid atom position specification for Coulomb matrix representation.")
    if not is_sequence(r[0][0]): raise QMMLException("Invalid atom position specification for Coulomb matrix representation.")

    # unit conversion factor
    if unit in ('pm', 'Picometers'): cf = 0.01889726124559  # CODATA 2010
    elif unit in ('A', 'Angstroms', 'angstrom', 'Ångström'): cf = 1.889726124559  # CODATA 2010
    elif unit in ('BohrRadius', 'bohr', 'a0', 'a.u.'): cf = 1.
    else: raise QMMLException("Unknown unit")

    # padding
    if padding in (False, None): padding = None
    elif isinstance(padding, (int, np.integer)) and padding >= 1: padding = padding
    else: raise QMMLException("Invalid padding specification.")

    # flatten
    if flatten is False or flatten is None: (vect, vectk) = (False, 0)  # flatten in (False, None) would fail
    elif flatten is True: (vect, vectk) = (True, 0)
    elif isinstance(flatten, (int, np.integer)): (vect, vectk) = (True, flatten)
    else: raise QMMLException("Invalid flattening specification.")

    # sorting
    if sort in (False, None): sort = False  # no sorting, flag instead of identity permutation for performance
    elif sort is True: sort = _coulomb_matrix_default_sortf
    elif sort == 'ZWeightedEuclideanDistance': sort = lambda dm, z, i: np.argsort(dm[i]/z)
    else: sort = sort

    # post processing
    pass

    return (cf, padding, vect, vectk, sort, postf)

## molecule Coulomb matrix

def molecule_coulomb_matrix(z, r, cf, padding, vect, vectk, sort, postf):
    """Implements Coulomb matrix for finite systems like molecules."""

    # number of atoms
    n = len(z)
    if n < 1: raise QMMLException("Invalid number of atoms")
    if n != len(r): raise QMMLException("Incompatible dimensions of z and r")
    assert r.shape == (n,3)

    # Coulomb matrix entries
    dm = distance_euclidean(r*cf)
    cm = np.empty((n, n))
    for i in range(n):
        for j in range(n):
            cm[i,j] = 0.5*z[i]**2.4 if i == j else z[i]*z[j]/dm[i,j]

    # sorting
    if sort is not False:
        order = sort(cm)
        cm = cm[order,:][:,order]
        # above solution using numpy indexing is potentially slow.
        # fastest way likely is to implement efficient permutation in C++
        # search keywords are "in place permutation locality"
        # see also https://en.wikipedia.org/wiki/In-place_matrix_transposition

    # post-processing
    if postf is not None:
        if sort is False: order = range(n)
        #postf = np.vectorize(postf, otypes=(np.float_,))
        for i in range(n):
            for j in range(n):
                cm[i,j] = postf(cm[i,j], z[order[i]], z[order[j]], dm[order[i],order[j]], i, j)

    # padding
    if padding is not None:
        cm = cm[:padding,:][:,:padding] if padding <= n else np.pad(cm, (0, padding-n), 'constant')

    # flattening
    if vect: cm = lower_triangular_part(cm, vectk)

    return cm

#  #####################################
#  #  Many-body tensor representation  #
#  #####################################

def many_body_tensor(z, r, d, kgwdcea, basis=None, elems=None, acc=1e-3, flatten=False):
    """Computes many-body tensor representation (MBTR) [1] for molecules or crystals.

    For periodic systems only, specify basis vectors. Coordinates are always Cartesian, not fractional.

    Parameters:
      z - Atomic numbers of atoms [[z11, z12, ...], [z21, ...], ...]
      r - Cartesian coordinates of atoms [[[x11, y11, z11], [x12, y12, z12], ...], [[x21, y21, z21], ...], ...]
      d - Discretization [xmin, deltax, xdim], where xmin is starting value of discretized axis, deltax is bin width, and xdim is number of bins
      kgwdcea - Parametrization [k, geomf, weightf, distrf, corrf, eindexf, aindexf]. See below for possible choices.

      basis [None] - basis vectors for periodic systems [[[b11x, b11y, b11z], [b12x, b12y, b12z], [b13x, b13y, b13z]], [[b21x, b21y, b21z], ...], ...]
      elems [None] - atomic numbers of elements to use; can be a superset, but not a strict subset of union of z
      acc [1-e3] - accuracy to which MBTR is computed
      flatten [False] - dimensionality of returned tensor; False: #systems x #element axes x xdim; True: #systems x (#element axes * xdim)

    Returns:
      Many-body tensors for all systems.

    Remarks:
      None.

    Parametrization:
      Geometry functions: 'unity', 'count', '1/distance', '1/dot', 'angle', 'cos_angle', 'dot/dotdot', 'dihedral', 'cos_dihedral'
      Weighting functions: 'unity', 'identity, 'identity^2', 'identity_root', '1/identity', 'delta_1/identity', 'exp_-1/identity', 'exp_-1/identity^2', '1/count', '1/normnorm', '1/dotdot', '1/normnormnorm', '1/dotdotdot', 'exp_-1/normnormnorm', 'exp_-1/norm+norm+norm'
      Distribution functions: 'normal'
      Correlation functions: 'identity'
      Element indexing functions: 'full', 'noreversals'
      Atom indexing functions: 'full', 'noreversals'

    Examples:
    >>> many_body_tensor([[1,1]], [[[0,0,0],[0.74,0,0]]], ...)  == TODO

    References:
      [1] H. Huo, M. Rupp: Unified Representation for Machine Learning of Molecules and Crystals, https://arxiv.org/abs/1704.06439v2
    """

    # parameters processing
    (zz, rr, bb, ssizes, sinds) = _linearize_raw_input(z, r, basis)
    ns = len(z)  # number of systems

    if elems is None:
        elements = np.unique(zz) # sorted
    else:
        elements = np.asarray(sorted(elems))
        assert np.in1d(np.unique(zz), elements).all(), "Specified tensor elements must include all elements from input Z."
    ne = elements.size

    (xmin, deltax, xdim) = d

    k = kgwdcea[0]
    (geomf  , geomfp  ) = (kgwdcea[1], np.empty([0])) if isinstance(kgwdcea[1], str) else (kgwdcea[1][0], np.asfarray(kgwdcea[1][1]))
    (weightf, weightfp) = (kgwdcea[2], np.empty([0])) if isinstance(kgwdcea[2], str) else (kgwdcea[2][0], np.asfarray(kgwdcea[2][1]))
    (distrf , distrfp ) = (kgwdcea[3], np.empty([0])) if isinstance(kgwdcea[3], str) else (kgwdcea[3][0], np.asfarray(kgwdcea[3][1]))
    (corrf  , corrfp  ) = (kgwdcea[4], np.empty([0])) if isinstance(kgwdcea[4], str) else (kgwdcea[4][0], np.asfarray(kgwdcea[4][1]))
    (eindexf, eindexfp) = (kgwdcea[5], np.empty([0])) if isinstance(kgwdcea[5], str) else (kgwdcea[5][0], np.asfarray(kgwdcea[5][1]))
    (aindexf, aindexfp) = (kgwdcea[6], np.empty([0])) if isinstance(kgwdcea[6], str) else (kgwdcea[6][0], np.asfarray(kgwdcea[6][1]))

    eindexf = 'dense_' + eindexf
    aindexf = ('periodic_' if (bb.size != 1) else 'finite_') + aindexf

    # calculation of many-body tensor
    return np.array(_qmml._representation_mbtr(k, ssizes, sinds, bb, zz, rr, elements, xmin, deltax, xdim, geomf, geomfp,
        weightf, weightfp, distrf, distrfp, corrf, corrfp, eindexf, eindexfp, aindexf, aindexfp, acc, flatten), copy = False)
