# qmmlpack
# (c) Matthias Rupp, 2006-2018.
# See LICENSE.txt for license.

"""Numerics utilities.

Part of qmmlpack library."""

import numpy as np

from .lib import QMMLException, _qmml, is_sequence
from .kernels_centering import center_kernel_matrix

#  ###############
#  #  linearize  #
#  ###############

def linearize(a, subset=None):
    """Linearizes a ragged array.

    See Algorithm for "Linearizing and delinearizing arrays".

    Parameters:
      a - ragged array (array of arrays)
      subset - subset indices to consider, None to select all entries

    Returns:
      2-tuple of linearized array and indices of starting positions.

    The starting positions follow the convention of having
    n+1 entries, the last one being the size of a."""

    # default is to select all entries
    if subset is None:
        subset = np.arange(len(a))

    # create index array
    k = len(subset); c = 0; s = np.empty(k+1, dtype=int);
    for j in range(k):
        s[j] = c
        c = c + len(a[subset[j]])
    s[k] = c

    # create linearized array
    b = np.empty(s[k], dtype=type(a[0][0]))
    for j in range(k):
        b[s[j]:s[j+1]] = a[subset[j]]

    return (b, s)

def delinearize(a, pos, subset=None, shape=None):
    """Delinearizes a linear array, yielding a ragged array.

    See Algorithm for "Linearizing and delinearizing arrays".

    Parameters:
      a - linear NumPy array or Python list
      pos - indices of starting positions of subarrays
      subset - subset of indices to consider, None to select all entries
      shape - shape of entries, same syntax as NumPy's reshape function;
              default is linear

    Returns:
      ragged array (array of arrays)

    The pos array should follow the convention of having
    n+1 entries, the last one being the size of a."""

    # ensure subset is a NumPy array
    if subset is None:
        subset = np.arange(len(pos)-1)
    else:
        if not isinstance(subset, np.ndarray): subset = np.asarray(subset)

    # default are linear subarrays
    if shape is None: shape = -1

    # create ragged array
    return np.asarray([a[pos[ii]:pos[ii+1]].reshape(shape) for ii in subset])

#  ###########################
#  #  Lower triangular part  #
#  ###########################

def lower_triangular_part(M, k=0):
    """Returns vector containing the lower triangular part of a matrix.

    Can be restricted to elements on and below the k-th main diagonal.

    >>> lower_triangular_part([[1,2],[3,4]]) == [1,3,4]
    >>> lower_triangular_part([[1,2],[3,4]], k=-1) == [3]"""
    v = np.empty((_qmml._lower_triangular_part_nelems(M.shape[0], M.shape[1], k),))
    _qmml._lower_triangular_part(v, M, k)
    return v

#  ################
#  #  Symmetrize  #
#  ################

def symmetrize(M):
    """Symmetrizes quadratic matrix.

    >>> symmetrize(np.asarray([[1,2],[4,5]])) == np.asarray([[1,3],[3,5]])"""
    S = np.copy(M)
    _qmml._symmetrize(S)
    return S

#  #######################################
#  #  Forward and backward substitution  #
#  #######################################

def forward_substitution(L, b):
    """Solves linear system of equations L . x = b for lower triangular matrix L.

    x and b can be either vectors or matrices.

    >>> forward_substitution(L, b)  # vector right hand side, returns solution vector
    >>> forward_substitution(L, B)  # matrix right hand side, returns solution matrix
    """
    x = np.empty(b.shape)
    if len(b.shape) == 1:
        _qmml._forward_substitution_v(x, L, b)
    else:
        _qmml._forward_substitution_m(x, L, b);
    return x

def backward_substitution(U, b):
    """Solves linear system of equations U . x = b for upper triangular matrix U.

    x and b can be either vectors or matrices.

    >>> backward_substitution(U, b)  # vector right hand side, returns solution vector
    >>> backward_substitution(U, B)  # matrix right hand side, returns solution matrix
    """
    x = np.empty(b.shape)
    if len(b.shape) == 1:
        _qmml._backward_substitution_v(x, U, b)
    else:
        _qmml._backward_substitution_m(x, U, b);
    return x

def forward_backward_substitution(L, b):
    """Solves linear system of equations L . L^T . x = b for lower triangular matrix L.

    x and b can be either vectors or matrices.

    >>> forward_backward_substitution(L, b)  # vector right hand side, return solution vector
    >>> forward_backward_substitution(L, B)  # matrix right hand side, return solution matrix
    """
    x = np.empty(b.shape)
    if len(b.shape) == 1:
        _qmml._forward_backward_substitution_v(x, L, b)
    else:
        _qmml._forward_backward_substitution_m(x, L, b);
    return x

def backward_forward_substitution(U, b):
    """Solves linear system of equations U^T . U . x = b for upper triangular matrix U.

    x and b can be either vectors or matrices.

    >>> backward_forward_substitution(U, b)  # vector right hand side, return solution vector
    >>> backward_forward_substitution(U, B)  # matrix right hand side, return solution matrix
    """
    x = np.empty(b.shape)
    if len(b.shape) == 1:
        _qmml._backward_forward_substitution_v(x, U, b)
    else:
        _qmml._backward_forward_substitution_m(x, U, b);
    return x
