# qmmlpack
# (c) Matthias Rupp, 2006-2017.
# See LICENSE.txt for license.

"""
qmmlpack: quantum mechanics / machine learning package
------------------------------------------------------

(c) Matthias Rupp, 2006-2018.
See LICENSE.txt for license.

Provides machine learning algorithms for accurate interpolation between quantum-mechanical calculations of poly-atomic systems such as molecules and materials.

Functionality grouped by categories:
    distances
    io (input/output)
    kernels
    numerics
    regression
    representations
    utility
    validation

For documentation, please refer to http://qmml.org
"""

from .lib import QMMLException, is_sequence, parse_options

# io
from .io_extended_xyz import ExtXYZData, import_extxyz, export_extxyz

# utility
from .utility import element_data

# distances
from .distances_pnorm import distance_one_norm, DistanceOneNorm, distance_euclidean, DistanceEuclidean, distance_squared_euclidean, DistanceSquaredEuclidean

# numerics
from .numerics import linearize, delinearize, lower_triangular_part, symmetrize, forward_substitution, backward_substitution, forward_backward_substitution, backward_forward_substitution

# validation
from .validation import stratify, local_grid_search, loss

# representations
from .representations import coulomb_matrix, many_body_tensor

# kernels
from .kernels_centering import center_kernel_matrix, CenterKernelMatrix
from .kernels_linear import kernel_linear
from .kernels_gaussian import kernel_gaussian
from .kernels_laplacian import kernel_laplacian

# regression
from .regression import KernelRidgeRegression
