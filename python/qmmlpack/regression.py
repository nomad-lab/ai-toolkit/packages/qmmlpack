# qmmlpack
# (c) Matthias Rupp, 2006-2016.
# See LICENSE.txt for license.

"""Regression.

Kernel ridge regression and Gaussian process regression (Kriging).

Part of qmmlpack library."""

import numpy as np

from .lib import QMMLException, _qmml, is_sequence
from .kernels_centering import center_kernel_matrix
from .numerics import forward_backward_substitution

#  ###########################
#  #  KernelRidgeRegression  #
#  ###########################

class KernelRidgeRegression:
    """Kernel ridge regression."""

    def __init__(self, K, y, theta, centering=False):
        """Trains a kernel ridge regression model from a kernel matrix K and labels y.

        Hyperparameters theta are the regularization strength, either a non-negative scalar or a vector.

        The following data members can be accessed:
          kernel_matrix  the kernel matrix K as passed (not centered)
          cholesky_decomposition  Cholesky decomposition (n x n upper triangular matrix) of K + lambda I
          labels  centered label vector (as passed if no centering)
          label_mean  mean of label vector (zero if not centering)
          weights  regression coefficients alpha
          theta  hyperparameter vector theta (as passed)
          regularization_strength  regularization strength lambda (processed)
        """

        # parameters and options processing
        assert y.flags.forc, "label vector must be contiguous in memory"
        self._n = len(y); assert self._n > 0, "number of samples must be positive"

        self._K = np.copy(K)  # deep copy to ensure matrix is available during lifetime of KernelRidgeRegression object
        assert self._K.flags.forc, "kernel matrix must be contiguous in memory"
        assert self._K.shape == (self._n, self._n), "incompatible dimensions of kernel matrix and label vector"

        self._theta = np.copy(theta)
        assert len(self._theta) == 1 or len(self._theta) == self._n, "invalid format for regularization strength lambda"
        assert len(self._theta) == 1 or self._theta.flags.forc, "theta parameter vector must be contiguous in memory"

        self._centering = bool(centering);

        # centering of labels
        self._ymean = np.round(np.mean(y), 10) if self._centering else 0.
        self._yc = y - self._ymean

        # centering of kernel matrix
        kkc = center_kernel_matrix(self._K) if self._centering else self._K
        self._regularization_strength = np.full((self._n,), self._theta[0]) if len(self._theta) == 1 else self._theta  # transform lambda to vector if specified as scalar
        kkc[ np.diag_indices(self._n) ] += self._regularization_strength

        # regression coefficients
        try:
            self._ll = np.linalg.cholesky(kkc)  # Kc + lambda I = L L^T
        except np.linalg.LinAlgError:
            raise QMMLException("Failed to train KernelRidgeRegression model due to kernel matrix not being positive definite")
        self._alpha = forward_backward_substitution(self._ll, self._yc)  # alpha = (Kc + lambda I)^-1 y

    def __call__(self, _argone=None, _argtwo=None):
        """krr(L) or krr(L, "Predictions") return predictions for kernel matrix L."""
        if _argone is not None and (_argtwo == "Predictions" or _argtwo is None):
            return self._predict(_argone)
        else:
            raise QMMLException("Invalid arguments to KernelRidgeRegression call.")

    def _predict(self, L):
        """Returns predictions for kernel matrix L."""

        # parameters and options processing
        (n, m) = L.shape

        # center kernel matrix L if required
        Lc = center_kernel_matrix(self._K, L) if self.centering else L

        # predictive mean
        return np.dot(Lc.T, self._alpha) + self._ymean  # if no label centering, mean will be 0

    @property
    def kernel_matrix(self):
        """Kernel matrix K as passed (not centered)."""
        return self._K

    @property
    def cholesky_decomposition(self):
        """Cholesky decomposition (n x n upper triangular matrix) of K + lambda I."""
        return self._ll.T

    @property
    def labels(self):
        """Centered label vector (as passed if no centering)."""
        return self._yc

    @property
    def label_mean(self):
        """Mean of label vector (zero if not centering)."""
        return self._ymean

    @property
    def weights(self):
        """Regression coefficients alpha."""
        return self._alpha

    @property
    def theta(self):
        """Hyperparameter vector theta (as passed)."""
        return self._theta

    @property
    def regularization_strength(self):
        """Regularization strength lambda (processed)."""
        return self._regularization_strength

    @property
    def centering(self):
        """Whether kernel matrix and labels will be centered."""
        return self._centering
