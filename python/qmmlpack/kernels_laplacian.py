# qmmlpack
# (c) Matthias Rupp, 2006-2017.
# See LICENSE.txt for license.

"""Laplacian kernel.

Part of qmmlpack library."""

import numpy as np

from .lib import QMMLException, _qmml, is_sequence

def kernel_laplacian(x, z=None, theta=None, diagonal=False, distance=False):
    """Laplacian kernel k(x,z) = exp(-||x-z||_1/s).

    The length scale s is a positive real.

    >>> kernel_laplacian(x, theta=s)  # kernel matrix K between one set of samples (rows of x)
    >>> kernel_laplacian(x, z, theta=s)  # kernel matrix L between two sets of samples (rows of x and z)
    >>> kernel_laplacian(x, theta=s, diagonal=True)  # diagonal only
    >>> kernel_laplacian(d, theta=s, distance=True)  # from matrix of one-norm distances
    """
    if theta is None:
        raise QMMLException("Hyperparameter must be specified for Laplacian kernel.")
    elif is_sequence(theta):
        if len(theta) != 1: raise QMMLException("Laplacian kernel takes exactly one hyperparameter.")
        sigma = theta[0]
    else:
        sigma = theta

    if z is None:
        # X only
        if distance:
            if diagonal:
                return np.ones(len(x))
            else:
                return _kernel_laplacian_d(x, sigma)
        else:
            if diagonal:
                return _kernel_laplacian_m(x, sigma)
            else:
                return _kernel_laplacian_k(x, sigma)
    else:
        # X and Z
        assert( (not diagonal) and (not distance) )
        return _kernel_laplacian_l(x, z, sigma)

def _kernel_laplacian_d(dd, sigma):
    # kernel matrix L of samples from one-norm distances
    ll = np.empty(dd.shape)
    _qmml._kernel_matrix_laplacian_d(ll, dd, sigma)
    return ll

def _kernel_laplacian_k(xx, sigma):
    # kernel matrix K of samples versus themselves
    kk = np.empty((len(xx), len(xx)))
    _qmml._kernel_matrix_laplacian_k(kk, xx, sigma)
    return kk

def _kernel_laplacian_l(xx, zz, sigma):
    # kernel matrix L of samples versus other samples
    ll = np.empty((len(xx), len(zz)))
    _qmml._kernel_matrix_laplacian_l(ll, xx, zz, sigma)
    return ll

def _kernel_laplacian_m(xx, sigma):
    # diagonal m of kernel matrix of samples versus themselves
    m = np.empty((len(xx),))
    _qmml._kernel_matrix_laplacian_m(m, xx, sigma)
    return m
