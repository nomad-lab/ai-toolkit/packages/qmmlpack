# qmmlpack
# (c) Matthias Rupp, 2006-2017.
# See LICENSE.txt for license.

"""Gaussian kernel.

Part of qmmlpack library."""

import numpy as np

from .lib import QMMLException, _qmml, is_sequence

def kernel_gaussian(x, z=None, theta=None, diagonal=False, distance=False):
    """Gaussian kernel k(x,z) = exp(-||x-z||^2/2s^2).

    The length scale s is a positive real.

    >>> kernel_gaussian(x, theta=s)  # kernel matrix K between one set of samples (rows of x)
    >>> kernel_gaussian(x, z, theta=s)  # kernel matrix L between two sets of samples (rows of x and z)
    >>> kernel_gaussian(x, theta=s, diagonal=True)  # diagonal only
    >>> kernel_gaussian(d, theta=s, distance=True)  # from matrix d of squared Euclidean distances
    """
    if theta is None:
        raise QMMLException("Length scale hyperparameter must be specified for Gaussian kernel.")
    elif is_sequence(theta):
        if len(theta) != 1: raise QMMLException("Gaussian kernel takes exactly one hyperparameter.")
        sigma = theta[0]
    else:
        sigma = theta

    if z is None:
        # X only
        if distance:
            if diagonal:
                return np.ones(len(x))
            else:
                return _kernel_gaussian_d(x, sigma)
        else:
            if diagonal:
                return _kernel_gaussian_m(x, sigma)
            else:
                return _kernel_gaussian_k(x, sigma)
    else:
        # X and Z
        assert( (not diagonal) and (not distance) )
        return _kernel_gaussian_l(x, z, sigma)

def _kernel_gaussian_d(dd, sigma):
    # kernel matrix L of samples from squared Euclidean distances
    ll = np.empty(dd.shape)
    _qmml._kernel_matrix_gaussian_d(ll, dd, sigma)
    return ll

def _kernel_gaussian_k(xx, sigma):
    # kernel matrix K of samples versus themselves
    kk = np.empty((len(xx), len(xx)))
    _qmml._kernel_matrix_gaussian_k(kk, xx, sigma)
    return kk

def _kernel_gaussian_l(xx, zz, sigma):
    # kernel matrix L of samples versus other samples
    ll = np.empty((len(xx), len(zz)))
    _qmml._kernel_matrix_gaussian_l(ll, xx, zz, sigma)
    return ll

def _kernel_gaussian_m(xx, sigma):
    # diagonal m of kernel matrix of samples versus themselves
    m = np.empty((len(xx),))
    _qmml._kernel_matrix_gaussian_m(m, xx, sigma)
    return m
