# qmmlpack
# (c) Matthias Rupp, 2006-2016.
# See LICENSE.txt for license.

"""Centering of kernel matrices.

Part of qmmlpack library."""

import numpy as np

from .lib import QMMLException, _qmml, is_sequence

def center_kernel_matrix(k, l=None, m=None):
    """Centers kernel matrices in kernel feature space.

    Returns a centered kernel matrix. All samples are centered with respect to training samples, i.e., the mean of the training samples is subtracted in feature space.

    >>> center_kernel_matrix(k)       # centers kernel matrix k in feature space
    >>> center_kernel_matrix(k, l)    # centers kernel matrix l with respect to k
    >>> center_kernel_matrix(k, l, m) # centers kernel matrix m with respect to k
    >>> center_kernel_matrix(k, l, m) # if m is a vector, only the diagonal of matrix m is centered
    """
    if l is None:
        return _center_kernel_matrix_k(k)
    elif m is None:
        return _center_kernel_matrix_l(k, l)
    else: return _center_kernel_matrix_m(k, l, m) if m.ndim > 1 else _center_kernel_matrix_mdiag(k, l, m)

def _center_kernel_matrix_k(k):
    kc = np.empty(k.shape)
    _qmml._center_kernel_matrix_k(kc, k)
    return kc

def _center_kernel_matrix_l(k, l):
    lc = np.empty(l.shape)
    _qmml._center_kernel_matrix_l(lc, k, l)
    return lc

def _center_kernel_matrix_m(k, l, m):
    mc = np.empty(m.shape)
    _qmml._center_kernel_matrix_m(mc, k, l, m)
    return mc

def _center_kernel_matrix_mdiag(k, l, m):
    mc = np.empty(m.shape)
    _qmml._center_kernel_matrix_mdiag(mc, k, l, m)
    return mc

class CenterKernelMatrix:
    """Centering of kernel matrices."""
    # Repeated centering with respect to the same K could benefit from storing row/column averages between calls.
    # This optimization is currently not implemented.
    # A potential syntax would be initializing this class with K; subsequent __call__ would then use self.K as first argument.
    # >>> ckm = CenterKernelMatrix(K); ckm(L); ckm(L, M); ckm(L, M.diag)

    def __call__(self, K, L=None, M=None):
        return center_kernel_matrix(K, L, M)
