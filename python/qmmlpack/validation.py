# qmmlpack
# (c) Matthias Rupp, 2006-2016.
# See LICENSE.txt for license.

"""Validation.

Part of qmmlpack library."""

import math
import numpy as np

from .lib import QMMLException, _qmml, is_sequence, parse_options

#  ##############
#  #  stratify  #
#  ##############

def stratify(y, k):
    """Returns list of stratified indices.

    Parameters:
      y - List of numerical values.
      k - Number of strata.

    Returns:
      List of strata indices. Each stratum has either Floor[Length[y]]/k entries or one more (the first Mod[Length[y],k] ones).
      Labels are sorted in ascending order.

    Divides indices of sorted labels into same-size blocks.
    Works for numeric class and regression labels.

    >>> stratify[range(10), 3] == [[0,1,2,3], [4,5,6], [7,8,9]]
    """

    assert 0 < k <= len(y), "number of strata must be positive and at most equal to number of labels"

    n = len(y)
    (div, rem) = divmod(n, k)
    splitind = [ ( i*div + min(i,rem), (i+1)*div + min(i+1, rem) ) for i in range(k)]
    order = np.argsort(y)
    return [order[si[0]:si[1]] for si in splitind]


#  #####################
#  #  LocalGridSearch  #
#  #####################

def local_grid_search(f, variables, resolution=None, evalmonitor=None, maxevals=None):
    """Optimization of a function in several variables via local gradient descent on a logarithmic grid.

    Parameters:
      f - function to be minimized
      variables - list of variables, see below
      resolution - if specified as a number a, f(...) will be rounded to multiples of a
      maxeval - maximum number of evaluations
      evalmonitor - function called after every evaluation of f with arguments trialv, trialf, bestv, bestf, state, where state is a dictionary.

    A variable is a tuple or a dictionary with the following information:

    Index Name        Description   Default     Explanation
      0   'value'     initial value   0.        initial value; real number; exponent to base b
      1   'priority'  priority        1         positive integer; higher priorities are optimized before lower ones; several variables may have the same priority
      2   'stepsize'  step size       1.        positive real number; refers to exponent
      3   'minimum'   minimum value   -infinity lowest allowed value for variable, value in [min,max]
      4   'maximum'   maximum value   +infinity highest allowed value for variable, value in [min,max]
      5   'direction' direction       0         either -1 (small values preferred), 0 (indifferent), +1 (large values preferred)
      6   'base'      base            2.        positive real number

    where value, stepsize, minimum and maximum refer to exponents of base.

    Later entries can be omitted. The logarithmized grid is the intersection between ...,i-2s,i-s,i,i+s,i+2s,... and (min,max).

    If optimizing an expensive function, such as hyperparameters of a machine learning model, it is important that f uses cached values.
    For example, if optimizing performance of a kernel learning model, kernel matrices for different values of kernel hyperparameters should be cached where possible."""

    # the descent can not cross itself
    # if a list of visited values is desired, it should be created using the evaluation monitor.

    # Parsing of variables and options
    def _parse_variable(variable):
        """Parses one variable."""
        defaults = ( ('value', 0.), ('priority', 1), ('stepsize', 1.), ('minimum', -np.inf), ('maximum', +np.inf), ('direction', 0), ('base', 2.) )
        v = parse_options(variable, defaults)

        if v[1] != int(v[1]): raise QMMLException("Non-integer priority specified for optimization variable")
        if v[1] <= 0: raise QMMLException("Invalid priority specified for optimization variable")

        if v[2] <= 0: raise QMMLException("Invalid step size specified for optimization variable")

        if v[5] != int(v[5]): raise QMMLException("Non-integer direction specified for optimization variable")
        if v[5] not in (-1, 0, +1): raise QMMLException("Invalid direction specified for optimization variable")

        if v[6] <= 0: raise QMMLException("Invalid base specified for optimization variable")

        if v[3] > v[4]: raise QMMLException("Invalid bounds specified for optimization variable")
        if not (v[3] <= v[0] <= v[4]): raise QMMLException("Initial value not in range for optimization variable")

        return v

    _vars = np.asarray([_parse_variable(v) for v in variables])
    _resolution = None if resolution is None else float(resolution)
    _maxevals = np.inf if maxevals is None else maxevals
    _evalmonitor = (lambda *_: None) if evalmonitor is None else evalmonitor

    # Subroutine taking one trial step
    def trial_step():
        nonlocal num_evals, best_v, best_f, conv, done

        # determine trial value
        v = np.copy(best_v); v[i] += d * _vars[i, 2]  # trial value
        v[i] = max(_vars[i, 3], min(v[i], _vars[i, 4]))  # clamp to [min, max]
        if v[i] == best_v[i] and d != 0: return False # was already at boundary before

        # call f
        trialpowers = np.power(_vars[:, 6], v)
        ftilde = f(*trialpowers)
        num_evals += 1
        if num_evals == _maxevals: done = True
        if _resolution is not None:
            # rounds to precision of resolution, then to number of significant digits to avoid trailing noise in last digits
            # https://stackoverflow.com/a/28427814/3248771
            ftilde = round( round(ftilde / _resolution) * _resolution, -int(math.floor(math.log10(_resolution))))

        # call evaluation monitor
        state = {
            'variables' : _vars,
            'resolution': _resolution,
            'max_evals' : _maxevals,
            'num_vars'  : num_vars,
            'num_evals' : num_evals,
            'converged' : conv,
            'index'     : i,
            'direction' : d
        }
        _evalmonitor(v, ftilde, v if ftilde < best_f else best_v, ftilde if ftilde < best_f else best_f, state)

        # update if improvement
        if ftilde < best_f:
            best_v[:] = v
            best_f = ftilde
            conv = [False] * num_vars
            return True
        else:
            return False

    # Initialization
    num_vars = len(_vars)  # number k of variables
    conv = [False] * num_vars  # convergence c
    best_v, best_f = np.copy(_vars[:,0]), np.inf
    num_evals = 0  # number of function evaluations
    done = False  # termination condition

    i, d = 0, 0
    trial_step()

    # main loop
    while not done:
        # determine variable to optimize next
        i = [j for (j,cj) in enumerate(conv) if not cj]  # variables currently being optimized
        i = [j for j in i if _vars[j,1] == max(_vars[i, 1])]  # only variables with highest priority
        i = np.random.choice(i)

        d = int(_vars[i, 5])
        if d == 0: d = np.random.choice((-1,+1))

        # optimize variable
        if not trial_step(): d = -d
        while (not done) and trial_step(): pass
        conv[i] = True

        # terminate if all variables are converged
        if all(conv): done = True

    return best_v, best_f


#  ####################
#  #  loss functions  #
#  ####################

# decorators are used to mark functions as suitable for regression or classification,
# as well as to define display names.

def _regression(f):
    """Decorator marks f as for regression labels."""
    f.is_regr = True
    return f

def _classification(f):
    """Decorator marks f as for binary classification labels."""
    f.is_class = True
    return f

def _display_name(name):
    """Decorator sets display name of f."""
    def _display_name_decorator(f):
        f.name = name
        return f
    return _display_name_decorator

def _abbreviation(abbrev):
    """Decorator sets abbreviation of f."""
    def _abbreviation_decorator(f):
        f.abbrev = abbrev
        return f
    return _abbreviation_decorator

# loss functions

@_regression
@_display_name('root_mean_squared_error')
@_abbreviation('rmse')
def _loss_rmse(true, pred):
    """Root mean squared error."""
    return math.sqrt(np.mean((true - pred)**2))

@_regression
@_display_name('mean_absolute_error')
@_abbreviation('mae')
def _loss_mae(true, pred):
    """Mean absolute error."""
    return np.mean(np.fabs(true-pred))

@_regression
@_display_name('median_absolute_error')
@_abbreviation('medianae')
def _loss_medianae(true, pred):
    """Median absolute error."""
    return np.median(np.fabs(true-pred))

@_regression
@_display_name('maximum_absolute_error')
@_abbreviation('maxae')
def _loss_maxae(true, pred):
    """Maximum absolute error."""
    return max(np.fabs(true-pred))

@_regression
@_display_name('one_minus_correlation')
@_abbreviation('1-r')
def _loss_1mr(true, pred):
    """1 - R."""
    return 1. - _util_r(true, pred)

@_regression
@_display_name('one_minus_correlation_squared')
@_abbreviation('1-r2')
def _loss_1mr2(true, pred):
    """1 - R^2."""
    return 1. - _util_r2(true, pred)

# utility functions

@_regression
@_display_name('correlation')
@_abbreviation('r')
def _util_r(true, pred):
    """Correlation coefficient.

    Also known as product-moment correlation coefficient, Pearson's correlation.
    """
    assert len(true) == len(pred) and len(true) > 1
    n, tmean, pmean = len(true), np.mean(true), np.mean(pred)
    covar = np.dot(true, pred)/n - tmean*pmean
    vart, varp = np.dot(true, true)/n - tmean**2, np.dot(pred, pred)/n - pmean**2
    return covar / math.sqrt(vart * varp)

@_regression
@_display_name('correlation_squared')
@_abbreviation('r2')
def _util_r2(true, pred):
    """Squared correlation coefficient.

    Also known as square of product-moment correlation coefficient, Pearson's correlation.
    """
    return _util_r(true, pred)**2

# auxiliary functions

@_regression
@_classification
@_display_name('number_of_samples')
@_abbreviation('n')
def _aux_n(true, pred):
    """Number of samples."""
    assert len(true) == len(pred)
    return len(true)

@_regression
@_display_name('standard_deviation')
@_abbreviation('stddev')
def _aux_stddev(true, pred):
    """Standard deviation of residuals.

    Square root of variance. See aux_var() for details.
    """
    return math.sqrt(_aux_var(true, pred))

@_regression
@_display_name('variance')
@_abbreviation('var')
def _aux_var(true, pred):
    """Variance of residuals.

    Uses maximum likelihood estimate (division by n) by default
    (as opposed to unbiased estimate (division by n-1)).
    """
    return np.var(true-pred, ddof=0)

# helper structures containing all loss functions

_loss_all_f = [_loss_rmse, _loss_mae, _loss_medianae, _loss_maxae, _loss_1mr, _loss_1mr2, _util_r, _util_r2, _aux_n, _aux_stddev, _aux_var]
_loss_all_n = [f.name for f in _loss_all_f]  # display names of all loss functions
_loss_all_a = [f.abbrev for f in _loss_all_f]  # abbreviations of all loss functions
_loss_all_na = { **{ f.abbrev: f for f in _loss_all_f }, **{ f.name: f for f in _loss_all_f } }  # index by name and abbreviation

# TODO extend to classification, add auto-detection for ptype, use dictionary that can always be queried with either abbreviation or name

def loss(true, pred, lossf=None, ptype='regression'):
    """Returns loss functions.

    Loss/cost and utility/reward functions are not distinguished. Use correspondingly.

    Parameters:
      true - vector of true function values
      pred - vector of predicted function values
      lossf - sequence of loss functions to compute.
              default is to compute all loss functions.
              if a string, a single number is returned.

    Returns:
      dictionary with computed loss functions."""

    # special case of single string
    if isinstance(lossf, str): return loss(true, pred, (lossf,), ptype=ptype)[lossf]

    # default is to return all valid loss functions
    if lossf is None: return loss(true, pred, lossf=_loss_all_n, ptype=ptype)

    # compute loss functions
    res = { key: _loss_all_na[key](np.asarray(true), np.asarray(pred)) for key in lossf }

    return res
