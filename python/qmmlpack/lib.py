# qmmlpack
# (c) Matthias Rupp, 2006-2018.
# See LICENSE.txt for license.

"""qmmlpack package-wide functionality.

Part of qmmlpack library.

This code always available everywhere in qmmlpack.
"""

import importlib, platform
_qmml = importlib.import_module(".libqmmlpackpy0", "qmmlpack")

class QMMLException(Exception):
    """Exception base class.

    All exceptions in qmmlpack derive from this class."""
    pass

_qmml._register_exception(QMMLException) # registers exception with C++ code

def is_sequence(arg):
    """True if arg is a list, tuple, array or similar object, but not a string, dictionary or set."""
    # checking for attributes '__getitem__' or '__iter__' erroneously accepts NumPy floating point variables
    if isinstance(arg, dict) or isinstance(arg, set): return False
    try:
        iter(arg)
        return not isinstance(arg, str)
    except TypeError:
        return False

def parse_options(values, defaults):
    """Parses options given as dictionary or list, using default values where necessary.

    Example:
    >>> parse_options((1., 2), ( ('float', 0.), ('int', 1), ('string', 'hello'))) == (1., 2, 'hello')
    >>> parse_options({ 'int' : 17 }, ( ('float', 0.), ('int', 1), ('string', 'hello'))) == (0., 17, 'hello')"""
    if is_sequence(values):
        return tuple([values[i] if i < len(values) else d[1] for (i,d) in enumerate(defaults)])
    else:
        return tuple([values[d[0]] if d[0] in values else d[1] for d in defaults])
