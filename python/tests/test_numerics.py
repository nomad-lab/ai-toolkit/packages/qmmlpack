# qmmlpack
# (c) Matthias Rupp, 2006-2018.
# See LICENSE.txt for license.

"""Unit tests for numerics utilities.

Part of qmmlpack library.
"""

import pytest
import numpy as np

import qmmlpack as qmml

def _equal(a, b):
    # Equality test for floating point arrays
    return np.allclose(a, b)

#  ############################
#  #  linearize, delinearize  #
#  ############################

class TestLinearizeDelinearize:
    def test_linearize_example1(self):
        # all indices, no subset
        ragged = np.asarray([[1,2,3], [4,5,6,7], [8], [], [9,10], [11]])
        (linearized, startpos) = qmml.linearize(ragged)
        assert all( linearized == np.arange(1, 12) )
        assert all( startpos == [0, 3, 7, 8, 8, 10, 11] )

        # subset
        subset = [1, 3, 5]
        (linearized, startpos) = qmml.linearize(ragged, subset=subset)
        assert all( linearized == [4,5,6,7,11] )
        assert all( startpos == [0, 4, 4, 5] )

    def test_delinearize_example1(self):
        # all indices, no subset
        linearized = np.arange(1, 12)
        startpos = np.asarray([0, 3, 7, 8, 8, 10, 11])
        ragged = qmml.delinearize(linearized, startpos)
        assert all( [all(a == b) for (a,b) in zip(ragged, [[1,2,3], [4,5,6,7], [8], [], [9,10], [11]])] )

        # subset
        subset = [1, 3, 5]
        ragged = qmml.delinearize(linearized, startpos, subset=subset)
        assert all( [all(a == b) for (a,b) in zip(ragged, [[4,5,6,7], [], [11]])] )

    def test_linearize_delinearize_example1(self):
        # all indices, no subset
        ragged = np.asarray([[1,2,3], [4,5,6,7], [8], [], [9,10], [11]])
        (linearized, startpos) = qmml.linearize(ragged)
        ragged2 = qmml.delinearize(linearized, startpos)
        assert all( [all(a == b) for (a,b) in zip(ragged, ragged2)] )

        # subset #1
        subset = [1, 3, 5]
        (linearized, startpos) = qmml.linearize(ragged, subset=subset)
        ragged2 = qmml.delinearize(linearized, startpos)
        assert all( [all(a == b) for (a,b) in zip(ragged[subset], ragged2)] )

        # subset #2
        (linearized, startpos) = qmml.linearize(ragged)
        ragged2 = qmml.delinearize(linearized, startpos, subset=subset)
        assert all( [all(a == b) for (a,b) in zip(ragged[subset], ragged2)] )

    def test_delinearize_linearize_example1(self):
        # all indices, no subset
        linearized = np.arange(1, 12)
        startpos = np.asarray([0, 3, 7, 8, 8, 10, 11])
        ragged = qmml.delinearize(linearized, startpos)
        (linearized2, startpos2) = qmml.linearize(ragged)
        assert all( linearized == linearized2 )
        assert all( startpos == startpos2 )

#  ###########################
#  #  lower_triangular_part  #
#  ###########################

class TestLowerTriangularPart:
    def test_3x3(self):
        M = np.array( [ [ 1, 2, 3 ], [ 4, 5, 6 ], [ 7, 8, 9 ] ], dtype=np.float_ )
        assert all( qmml.lower_triangular_part(M) == qmml.lower_triangular_part(M, 0) )
        assert all( qmml.lower_triangular_part(M,  0) == np.array( [1,4,5,7,8,9] ) )
        assert all( qmml.lower_triangular_part(M,  1) == np.array( [1,2,4,5,6,7,8,9] ) )
        assert all( qmml.lower_triangular_part(M,  2) == np.arange(1, 10) )
        assert all( qmml.lower_triangular_part(M,  3) == M.flatten() )
        assert all( qmml.lower_triangular_part(M,  4) == M.flatten() )
        assert all( qmml.lower_triangular_part(M, -1) == np.array( [4,7,8] ) )
        assert all( qmml.lower_triangular_part(M, -2) == np.array( [7] ) )
        assert all( qmml.lower_triangular_part(M, -3) == np.array( [] ) )
        assert all( qmml.lower_triangular_part(M, -4) == np.array( [] ) )

    def test_4x3(self):
        M = np.array( [ [ 1, 2, 3 ], [ 4, 5, 6 ], [ 7, 8, 9 ], [ 10, 11, 12 ] ], dtype=np.float_ )
        assert all( qmml.lower_triangular_part(M) == qmml.lower_triangular_part(M, 0) )
        assert all( qmml.lower_triangular_part(M,  0) == np.array( [1,4,5,7,8,9,10,11,12] ) )
        assert all( qmml.lower_triangular_part(M,  1) == np.array( [1,2,4,5,6,7,8,9,10,11,12] ) )
        assert all( qmml.lower_triangular_part(M,  2) == np.arange(1, 13) )
        assert all( qmml.lower_triangular_part(M,  3) == M.flatten() )
        assert all( qmml.lower_triangular_part(M,  4) == M.flatten() )
        assert all( qmml.lower_triangular_part(M, -1) == np.array( [4,7,8,10,11,12] ) )
        assert all( qmml.lower_triangular_part(M, -2) == np.array( [7,10,11] ) )
        assert all( qmml.lower_triangular_part(M, -3) == np.array( [10] ) )
        assert all( qmml.lower_triangular_part(M, -4) == np.array( [] ) )
        assert all( qmml.lower_triangular_part(M, -5) == np.array( [] ) )

    def test_3x4(self):
        M = np.array( [ [ 1, 2, 3, 4 ], [ 5, 6, 7, 8 ], [ 9, 10, 11, 12 ] ], dtype=np.float_ )
        assert all( qmml.lower_triangular_part(M) == qmml.lower_triangular_part(M, 0) )
        assert all( qmml.lower_triangular_part(M,  0) == np.array( [1,5,6,9,10,11] ) )
        assert all( qmml.lower_triangular_part(M,  1) == np.array( [1,2,5,6,7,9,10,11,12] ) )
        assert all( qmml.lower_triangular_part(M,  2) == np.array( [1,2,3,5,6,7,8,9,10,11,12] ) )
        assert all( qmml.lower_triangular_part(M,  3) == M.flatten() )
        assert all( qmml.lower_triangular_part(M,  4) == M.flatten() )
        assert all( qmml.lower_triangular_part(M, -1) == np.array( [5,9,10] ) )
        assert all( qmml.lower_triangular_part(M, -2) == np.array( [9] ) )
        assert all( qmml.lower_triangular_part(M, -3) == np.array( [] ) )
        assert all( qmml.lower_triangular_part(M, -4) == np.array( [] ) )

    def test_5x1(self):
        M = np.array( [ [ 1 ], [ 2 ], [ 3 ], [ 4 ], [ 5 ] ], dtype=np.float_ )
        assert all( qmml.lower_triangular_part(M) == qmml.lower_triangular_part(M, 0) )
        assert all( qmml.lower_triangular_part(M,  0) == M.flatten() )
        assert all( qmml.lower_triangular_part(M,  1) == M.flatten() )
        assert all( qmml.lower_triangular_part(M, -1) == np.array( [2,3,4,5] ) )
        assert all( qmml.lower_triangular_part(M, -2) == np.array( [3,4,5] ) )
        assert all( qmml.lower_triangular_part(M, -3) == np.array( [4,5] ) )
        assert all( qmml.lower_triangular_part(M, -4) == np.array( [5] ) )
        assert all( qmml.lower_triangular_part(M, -5) == np.array( [] ) )
        assert all( qmml.lower_triangular_part(M, -6) == np.array( [] ) )

    def test_1x5(self):
        M = np.array( [ [ 1, 2, 3, 4, 5 ] ], dtype=np.float_ )
        assert all( qmml.lower_triangular_part(M) == qmml.lower_triangular_part(M, 0) )
        assert all( qmml.lower_triangular_part(M,  0) == np.array( [1] ) )
        assert all( qmml.lower_triangular_part(M,  1) == np.array( [1,2] ) )
        assert all( qmml.lower_triangular_part(M,  2) == np.array( [1,2,3] ) )
        assert all( qmml.lower_triangular_part(M,  3) == np.array( [1,2,3,4] ) )
        assert all( qmml.lower_triangular_part(M,  4) == M.flatten() )
        assert all( qmml.lower_triangular_part(M,  5) == M.flatten() )
        assert all( qmml.lower_triangular_part(M, -1) == np.array( [] ) )
        assert all( qmml.lower_triangular_part(M, -2) == np.array( [] ) )


#  ################
#  #  Symmetrize  #
#  ################

class TestSymmetrize:
    def test_1x1(self):
        assert ( qmml.symmetrize(np.asarray([ [ 1. ] ], dtype=np.float_)) == [ [ 1. ] ] ).all()

    def test_2x2_nochange(self):
        assert ( qmml.symmetrize(np.asarray([ [ 1, 3 ], [ 3, 5 ] ], dtype=np.float_)) == [ [ 1, 3 ], [ 3, 5 ] ] ).all()

    def test_2x2_change(self):
        assert ( qmml.symmetrize(np.asarray([ [ 1, 2 ], [ 4, 5 ] ], dtype=np.float_)) == [ [ 1, 3 ], [ 3, 5 ] ] ).all()

    def test_randomized(self):
        n = 100
        M = np.random.uniform(-1, 1, (n,n))
        M = qmml.symmetrize(M)
        assert ( M == M.T ).all()


#  ##########################
#  #  forward_substitution  #
#  ##########################

class TestForwardSubstitionV:
    def test_one(self):
        L = np.array( [ [ 1 ] ] );
        b = np.array( [ 1 ] );
        r = np.array( [ 1 ] );
        assert _equal(qmml.forward_substitution(L, b), r)

    def test_three_diagonal(self):
        L = np.array( [ [ 1, 0, 0 ], [ 0, 2, 0 ], [ 0, 0, 3 ] ] );
        b = np.array( [ 1, 2, 3 ] );
        r = np.array( [ 1, 1, 1 ] );
        assert _equal(qmml.forward_substitution(L, b), r)

    def test_three_nondiagonal(self):
        L = np.array( [ [ 1, 0, 0 ], [ 2, 3, 0 ], [ 4, 5, 6 ] ] );
        b = np.array( [ 1, 8, 32 ] );
        r = np.array( [ 1, 2, 3 ] );
        assert _equal(qmml.forward_substitution(L, b), r)

class TestForwardSubstitionM:
    def test_one_one(self):
        L = np.array( [ [ 1 ] ] );
        B = np.array( [ [ 1 ] ] );
        r = np.array( [ [ 1 ] ] );
        assert _equal(qmml.forward_substitution(L, B), r)

    def test_three_three_diagonal(self):
        L = np.array( [ [ 1, 0, 0 ], [ 0, 2, 0 ], [ 0, 0, 3 ] ] );
        B = np.array( [ [ 1, 1, 1 ], [ 2, 2, 2 ], [ 3, 3, 3 ] ] );
        r = np.array( [ [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ] ] );
        assert _equal(qmml.forward_substitution(L, B), r)

    def test_three_three_nondiagonal(self):
        L = np.array( [ [ 1,  0, 0 ], [ 2,  3,  0 ], [  4,   5,  6 ] ] );
        B = np.array( [ [ 1, -1, 2 ], [ 8, -8, 16 ], [ 32, -32, 76 ] ] );
        r = np.array( [ [ 1, -1, 2 ], [ 2, -2,  4 ], [  3, - 3,  8 ] ] );
        assert _equal(qmml.forward_substitution(L, B), r)

    def test_three_two_nondiagonal(self):
        L = np.array( [ [ 1,  0, 0 ], [ 2,  3,  0 ], [  4,   5,  6 ] ] );
        B = np.array( [ [ 1, -1 ], [ 8, -8 ], [ 32, -32 ] ] );
        r = np.array( [ [ 1, -1 ], [ 2, -2 ], [  3, - 3 ] ] );
        assert _equal(qmml.forward_substitution(L, B), r)

    def test_three_four_nondiagonal(self):
        L = np.array( [ [ 1,  0, 0 ], [ 2,  3,  0 ], [  4,   5,  6 ] ] );
        B = np.array( [ [ 1, -1, 2, -2 ], [ 8, -8, 16, -16 ], [ 32, -32, 76, -76 ] ] );
        r = np.array( [ [ 1, -1, 2, -2 ], [ 2, -2,  4, - 4 ], [  3, - 3,  8, - 8 ] ] );
        assert _equal(qmml.forward_substitution(L, B), r)

#  ###########################
#  #  backward_substitution  #
#  ###########################

class TestBackwardSubstitionV:
    def test_one(self):
        U = np.array( [ [ 1 ] ] );
        b = np.array( [ 1 ] );
        r = np.array( [ 1 ] );
        assert _equal(qmml.backward_substitution(U, b), r)

    def test_three_diagonal(self):
        U = np.array( [ [ 3, 0, 0 ], [ 0, 2, 0 ], [ 0, 0, 1 ] ] );
        b = np.array( [ 3, 2, 1 ] );
        r = np.array( [ 1, 1, 1 ] );
        assert _equal(qmml.backward_substitution(U, b), r)

    def test_three_nondiagonal(self):
        U = np.array( [ [ 6, 5, 4 ], [ 0, 3, 2 ], [ 0, 0, 1 ] ] );
        b = np.array( [ 32, 8, 1 ] );
        r = np.array( [ 3, 2, 1 ] );
        assert _equal(qmml.backward_substitution(U, b), r)

class TestBackwardSubstitionM:
    def test_one_one(self):
        U = np.array( [ [ 1 ] ] );
        B = np.array( [ [ 1 ] ] );
        r = np.array( [ [ 1 ] ] );
        assert _equal(qmml.backward_substitution(U, B), r)

    def test_three_three_diagonal(self):
        U = np.array( [ [ 3, 0, 0 ], [ 0, 2, 0 ], [ 0, 0, 1 ] ] );
        B = np.array( [ [ 3, 3, 3 ], [ 2, 2, 2 ], [ 1, 1, 1 ] ] );
        r = np.array( [ [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ] ] );
        assert _equal(qmml.backward_substitution(U, B), r)

    def test_three_three_nondiagonal(self):
        U = np.array( [ [  6,   5,  4 ], [ 0,  3,  2 ], [ 0,  0, 1 ] ] );
        B = np.array( [ [ 32, -32, 76 ], [ 8, -8, 16 ], [ 1, -1, 2 ] ] );
        r = np.array( [ [  3,  -3,  8 ], [ 2, -2,  4 ], [ 1, -1, 2 ] ] );
        assert _equal(qmml.backward_substitution(U, B), r)

    def test_three_two_nondiagonal(self):
        U = np.array( [ [  6,   5,  4 ], [ 0,  3,  2 ], [ 0,  0, 1 ] ] );
        B = np.array( [ [ 32, -32 ], [ 8, -8 ], [ 1, -1 ] ] );
        r = np.array( [ [  3,  -3 ], [ 2, -2 ], [ 1, -1 ] ] );
        assert _equal(qmml.backward_substitution(U, B), r)

    def test_three_four_nondiagonal(self):
        U = np.array( [ [  6,   5,  4 ], [ 0,  3,  2 ], [ 0,  0, 1 ] ] );
        B = np.array( [ [ 32, -32, 76, -76 ], [ 8, -8, 16, -16 ], [ 1, -1, 2, -2 ] ] );
        r = np.array( [ [  3,  -3,  8, - 8 ], [ 2, -2,  4, - 4 ], [ 1, -1, 2, -2 ] ] );
        assert _equal(qmml.backward_substitution(U, B), r)

#  ###################################
#  #  forward_backward_substitution  #
#  ###################################

class TestForwardBackwardSubstitutionV:
    def test_one(self):
        L = np.array( [ [ 1 ] ] );
        b = np.array( [ 1 ] );
        r = np.array( [ 1 ] );
        assert _equal(qmml.forward_backward_substitution(L, b), r)

    def test_three_diagonal(self):
        L = np.array( [ [ 1, 0, 0 ], [ 0, 2, 0 ], [ 0, 0, 3 ] ] );
        b = np.array( [ 1, 2, 3 ] );
        r = np.array( [ 1., 1/2., 1/3. ] );
        assert _equal(qmml.forward_backward_substitution(L, b), r)

    def test_three_nondiagonal(self):
        L = np.array( [ [ 1, 0, 0 ], [ 2, 3, 0 ], [ 4, 5, 6 ] ] );
        b = np.array( [ 1, 8, 32 ] );
        r = np.array( [ -2/3., -1/6., 1/2. ] );
        assert _equal(qmml.forward_backward_substitution(L, b), r)

#  ###################################
#  #  backward_forward_substitution  #
#  ###################################

class TestBackwardForwardSubstitutionV:
    def test_one(self):
        U = np.array( [ [ 1 ] ] );
        b = np.array( [ 1 ] );
        r = np.array( [ 1 ] );
        assert _equal(qmml.backward_forward_substitution(U, b), r)

    def test_three_diagonal(self):
        U = np.array( [ [ 3, 0, 0 ], [ 0, 2, 0 ], [ 0, 0, 1 ] ] );
        b = np.array( [ 3, 2, 1 ] );
        r = np.array( [ 1/3., 1/2., 1. ] );
        assert _equal(qmml.backward_forward_substitution(U, b), r)

    def test_three_nondiagonal(self):
        U = np.array( [ [ 6, 5, 4 ], [ 0, 3, 2 ], [ 0, 0, 1 ] ] );
        b = np.array( [ 32, 8, 1 ] );
        r = np.array( [ 283/81., 86/27., -71/9. ] );
        assert _equal(qmml.backward_forward_substitution(U, b), r)
