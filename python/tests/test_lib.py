# qmmlpack
# (c) Matthias Rupp, 2006-2017.
# See LICENSE.txt for license.

"""Unit tests for library main file.

Part of qmmlpack library.
"""

import pytest
import numpy as np

import qmmlpack as qmml

def _equal(a, b):
    # Equality test for floating point arrays
    return np.allclose(a, b)

#  #################
#  #  is_sequence  #
#  #################

class TestIsSequence:

    def test_tuple(self):
        assert qmml.is_sequence(())
        assert qmml.is_sequence((1,))
        assert qmml.is_sequence(('a','b'))

    def test_list(self):
        assert qmml.is_sequence([])
        assert qmml.is_sequence([1])
        assert qmml.is_sequence(['a','b'])
        assert qmml.is_sequence([[]])

    def test_string(self):
        assert not qmml.is_sequence('')
        assert not qmml.is_sequence('abc')

    def test_np_float(self):
        a = np.arange(0,3)
        assert qmml.is_sequence(a)
        assert not qmml.is_sequence(a[0])

    def test_set(self):
        assert not qmml.is_sequence(set())
        assert not qmml.is_sequence({'a', 'b'})

    def test_dict(self):
        assert not qmml.is_sequence({})
        assert not qmml.is_sequence({'a': 1, 'b': 2})

    # dictionaries not tested here

#  ###################
#  #  parse_options  #
#  ###################

class TestParseOptions:

    def test_case_01(self):
        values = (1., 2)
        defaults = ( ('float', 0.), ('int', 1), ('string', 'hello'))
        assert qmml.parse_options(values, defaults) == (1., 2, 'hello')

    def test_case_02(self):
        values = [1., 2]
        defaults = ( ('float', 0.), ('int', 1), ('string', 'hello'))
        assert qmml.parse_options(values, defaults) == (1., 2, 'hello')

    def test_case_03(self):
        values = { 'int' : 17 }
        defaults = ( ('float', 0.), ('int', 1), ('string', 'hello'))
        assert qmml.parse_options(values, defaults) == (0., 17, 'hello')

    def test_case_04(self):
        values = {}
        defaults = ( ('float', 0.), ('int', 1), ('string', 'hello'))
        assert qmml.parse_options(values, defaults) == (0., 1, 'hello')
