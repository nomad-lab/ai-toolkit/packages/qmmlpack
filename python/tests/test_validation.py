# qmmlpack
# (c) Matthias Rupp, 2006-2017.
# See LICENSE.txt for license.

"""Unit tests for validation.

Part of qmmlpack library.
"""

import pytest
import math
import numpy as np
from numpy import array as npa

import qmmlpack as qmml

def _equal(a, b):
    # Equality test for floating point arrays
    return np.allclose(a, b)

#  ####################
#  #  Stratification  #
#  ####################

def check_stratification(y, lst, n, k, check_range = True):
    """Checks stratification indices for correctness."""

    # 1) Number of lists must be k
    cond1 = (len(lst) == k)
    if not cond1: print("Stratification condition 1 failed for", lst)

    # 2) Lists must have right lengths
    (m, h) = divmod(n, k)
    tmp = [m + (1 if i < h else 0) for i in range(k)]
    cond2 = [len(l) for l in lst] == tmp
    if not cond2: print("Stratification condition 2 failed for", lst)

    # 3) Each index must be used once
    tmp = [b for a in lst for b in a]  # flatten lst
    cond3 = ( np.sort(tmp) == list(set(tmp)) ).all()
    if not cond3: print("Stratification condition 3 failed for", lst)

    # 4) Index range must be 1,...,n
    cond4 = ( np.sort(tmp) == range(n) ).all() if check_range else True
    if not cond4: print("Stratification condition 4 failed for", lst)

    # 5) Labels must increase per list
    cond5 = all([max(y[lst[i]]) <= min(y[[b for a in lst[i+1:] for b in a]]) for i in range(k-1)] )
    if not cond5: print("Stratification condition 5 failed for", lst)

    return cond1 and cond2 and cond3 and cond4 and cond5

class TestStratify:
    def test_case_01(self):
        y = np.arange(9, -1, -1)
        assert check_stratification(y, qmml.stratify(y, 3), len(y), 3)

    def test_case_02(self):
        y = np.arange(11)
        assert check_stratification(y, qmml.stratify(y, 3), len(y), 3)

    def test_case_03(self):
        y = np.arange(12)
        assert check_stratification(y, qmml.stratify(y, 3), len(y), 3)

    def test_case_04(self):
        y = np.random.random((30,))*2-1
        assert check_stratification(y, qmml.stratify(y, 17), len(y), 17)

    def test_case_05(self):
        y = np.arange(3)
        assert check_stratification(y, qmml.stratify(y, 3), len(y), 3)

    def test_case_06(self):
        aa = qmml.stratify(np.arange(10), 3)
        bb = [[0,1,2,3], [4,5,6], [7,8,9]]
        assert all([all(a == b) for (a,b) in zip(aa,bb)])

    def test_case_07(self):
        aa = qmml.stratify(np.asarray([8,0]), 1)
        bb = [[1,0]]
        assert all([all(a == b) for (a,b) in zip(aa,bb)])


#  #######################
#  #  Local grid search  #
#  #######################

class TestLocalGridSearch:
    def test_case_01(self):
        # 1d, quadratic, find minimum in 3 steps
        def f(arg): return (arg-1)**2;
        _state = []
        def g(trialv, trialf, bestv, bestf, state): _state.append(state)
        best_v, best_f = qmml.local_grid_search(f, [{'value': 2, 'stepsize': 1, 'direction': -1}], evalmonitor=g)
        assert best_v == (0.,) and best_f == 0
        assert _state[-1]['num_evals'] == 4

    def test_case_02(self):
        # 2d, linear, bounded
        def f(a, b): return 3*a + 2*b;
        best_v, best_f =  qmml.local_grid_search(f, [[0, 1, 1, -5, 5, +1, 2.718], [2, 1, 1, -5, 5, 0, 2.718]])
        assert all(best_v == (-5., -5.))

    def test_case_03(self):
        # corner cases
        def f(a,b): return 1.
        _state = []
        def g(trialv, trialf, bestv, bestf, state): _state.append(state)
        best_v, best_f = qmml.local_grid_search(f, [[0, 1, 1, 0, 0], [0, 1, 1, 0, 0]], evalmonitor=g)
        assert _state[-1]['num_evals'] == 1

    def test_case_04(self):
        # direction must not influence result for non-degenerate cases
        def f(a): return a
        best_v, best_f = qmml.local_grid_search(f, [[3., 1, 1., -5, +5, +1, 2.]])
        assert best_v == (-5.)

        best_v, best_f = qmml.local_grid_search(f, [[3., 1, 1., -5, +5, -1, 2.]])
        assert best_v == (-5.)

        best_v, best_f = qmml.local_grid_search(f, [[3., 1, 1., -5, +5, 0, 2.]])
        assert best_v == (-5.)

    def test_case_05(self):
        # maximum number of iterations
        def f(a): return (a-1)**2;
        _state = []
        def g(trialv, trialf, bestv, bestf, state): _state.append(state)
        best_v, best_f = qmml.local_grid_search(f, [{'value': 2, 'stepsize': 1, 'direction': -1}], maxevals=3, evalmonitor=g)
        assert best_v == (0.,) and best_f == 0
        assert _state[-1]['num_evals'] == 3

#  ####################
#  #  Loss functions  #
#  ####################

class TestLoss:
    def test_loss_rmse1(self):
        # dimensionality one
        a, b = npa( [1.] ), npa( [-3.] )
        assert qmml.loss(a, b, 'rmse') == 4. \
           and qmml.loss(b, a, 'rmse') == qmml.loss(a, b, 'rmse')

    def test_loss_rmse2(self):
        # more dimensions
        a = npa( [1., 2., 3.] )
        b = npa( [4., 6., 8.] )
        assert qmml.loss(a, b, 'root_mean_squared_error') == math.sqrt(50./3) \
           and qmml.loss(b, a, 'root_mean_squared_error') == qmml.loss(a, b, 'root_mean_squared_error')

    def test_loss_mae1(self):
        # one dimension
        a, b = npa( [1.] ), npa( [-3.] )
        assert qmml.loss(a, b, 'mae') == 4. \
           and qmml.loss(b, a, 'mae') == qmml.loss(a, b, 'mae')

    def test_loss_mae2(self):
        # more dimensions
        a = npa( [1., 2., 3.] )
        b = npa( [4., 6., 8.] )
        assert qmml.loss(a, b, 'mean_absolute_error') == 12./3 \
           and qmml.loss(b, a, 'mean_absolute_error') == qmml.loss(a, b, 'mean_absolute_error')

    def test_loss_medianae1(self):
        a = npa( [1,-1, 0,1,1000] )
        b = npa( [0,-2,-1,0,   1] )
        # 1,1,1,1,999
        assert _equal(qmml.loss(a, b, 'medianae'), 1.) \
           and _equal(qmml.loss(b, a, 'medianae'), 1.) # abs part

    def test_loss_medianae2(self):
        a = npa( [1, 2, 3, 4] )
        b = npa( [2, 4, 0, 8] )
        # 1, 2, 3, 4
        assert _equal(qmml.loss(a, b, 'median_absolute_error'), 2.5) \
           and _equal(qmml.loss(a, b, 'median_absolute_error'), qmml.loss(b, a, 'median_absolute_error'))

    def test_loss_maxae1(self):
        a = npa([1,-1, 0,1,1000])
        b = npa([0,-2,-1,0,   1])
        # 1,1,1,1,999
        assert _equal(qmml.loss(a, b, 'maxae'), 999.) \
           and _equal(qmml.loss(a, b, 'maxae'), qmml.loss(b, a, 'maxae'))

    def test_loss_maxae2(self):
        a = npa([1, 1, 1, 1, 1])
        b = npa([2, 3, 4, 5, 6])
        assert _equal(qmml.loss(a, b, 'maximum_absolute_error'), 5.) \
           and _equal(qmml.loss(a, b, 'maximum_absolute_error'), qmml.loss(b, a, 'maximum_absolute_error'))

    def test_loss_1mr(self):
        # random checks
        n = 30
        a = np.random.uniform(-1, 1, n)
        b = np.random.uniform(-1, 1, n)
        assert _equal(qmml.loss(a, b, '1-r'), 1. - qmml.loss(a, b, 'r')) \
           and _equal(qmml.loss(a, b, 'one_minus_correlation'), 1. - qmml.loss(a, b, 'correlation'))

    def test_loss_1mr2(self):
        # random checks
        n = 30
        a = np.random.uniform(-1, 1, n)
        b = np.random.uniform(-1, 1, n)
        assert _equal(qmml.loss(a, b, '1-r2'), 1. - qmml.loss(a, b, 'r2')) \
           and _equal(qmml.loss(a, b, 'one_minus_correlation_squared'), 1. - qmml.loss(a, b, 'correlation_squared'))

    def test_util_r_1(self):
        # dimensionality two
        # Correlation(X,Y) = covar(X,y) / sqrt(var(X)var(Y))
        # for X=(a,b), Y=(x,y) as input, this equals
        # (a-b)(x-y)/(|a-b| |x-y|) (independent of whether biased
        # or unbiased variance estimator is used)
        X, Y = npa( [1.,2.] ), npa( [3.,5.] )
        assert qmml.loss(X, Y, 'r') ==  1.                     \
           and qmml.loss(X, Y, 'r') ==  qmml.loss( Y,  X, 'r') \
           and qmml.loss(X, Y, 'r') == -qmml.loss( X, -Y, 'r') \
           and qmml.loss(X, Y, 'r') ==  qmml.loss(-X, -Y, 'r')

    def test_util_r_2(self):
        # dimensionality three
        # independent of whether biased or unbiased variance estimator is used
        X, Y = npa( [1.,2.,3.] ), npa( [4.,-5.,6.] )
        assert _equal(qmml.loss(X, Y, 'correlation'),  math.sqrt(3./103))                \
           and _equal(qmml.loss(X, Y, 'correlation'),  qmml.loss( Y,  X, 'correlation')) \
           and _equal(qmml.loss(X, Y, 'correlation'), -qmml.loss( X, -Y, 'correlation')) \
           and _equal(qmml.loss(X, Y, 'correlation'),  qmml.loss(-X, -Y, 'correlation'))

    def test_perf_r2_1(self):
        # dimensionality two
        X, Y = npa( [1.,2.] ), npa( [3.,5.] )
        assert qmml.loss(X, Y, 'r2') == 1.                      \
           and qmml.loss(X, Y, 'r2') == qmml.loss( Y,  X, 'r2') \
           and qmml.loss(X, Y, 'r2') == qmml.loss( X, -Y, 'r2') \
           and qmml.loss(X, Y, 'r2') == qmml.loss(-X,  Y, 'r2') \
           and qmml.loss(X, Y, 'r2') == qmml.loss(-X, -Y, 'r2')

    def test_perf_r2_2(self):
        # dimensionality three
        X, Y = npa( [1.,2.,3.] ), npa( [4.,-5.,6.] )
        assert _equal(qmml.loss(X, Y, 'correlation_squared'), 3./103)                                   \
           and _equal(qmml.loss(X, Y, 'correlation_squared'), qmml.loss( Y,  X, 'correlation_squared')) \
           and _equal(qmml.loss(X, Y, 'correlation_squared'), qmml.loss( X, -Y, 'correlation_squared')) \
           and _equal(qmml.loss(X, Y, 'correlation_squared'), qmml.loss(-X, -Y, 'correlation_squared'))

    def test_aux_n(self):
        X, Y, Z = npa( [1.] ), npa( [2.,3.] ), npa( [4.,5.,6.] )
        assert qmml.loss(X, X, 'n') == 1 \
           and qmml.loss(Y, Y, 'n') == 2 \
           and qmml.loss(Z, Z, 'number_of_samples') == 3

    def test_aux_stddev(self):
        X = npa( [2,0,5,1,6,-3] )
        Y = npa( [1,1,3,3,3, 0] )
        assert _equal(qmml.loss(X, Y, 'stddev'), math.sqrt(qmml.loss(X, Y, 'var'))) \
           and _equal(qmml.loss(X, Y, 'standard_deviation'), math.sqrt(qmml.loss(X, Y, 'variance'))) \
           and _equal(qmml.loss(X, Y, 'stddev'), qmml.loss(Y, X, 'stddev'))

    def test_aux_var(self):
        X = npa( [2,0,5,1,6,-3] )
        Y = npa( [1,1,3,3,3, 0] )
        # 1,-1,2,-2,3,-3
        assert _equal(qmml.loss(X, Y, 'var'), 28./6) \
           and _equal(qmml.loss(X, Y, 'variance'), 28./6) \
           and _equal(qmml.loss(X, Y, 'var'), qmml.loss(Y, X, 'var'))

    def test_multiple(self):
        a, b = np.random.uniform(-1, 1, 20), np.random.uniform(-1, 1, 20)
        lossf = ('rmse', 'mae', 'medianae', 'maxae', '1-r', '1-r2', 'r', 'r2', 'n', 'stddev', 'var')
        res = qmml.loss(a, b, lossf)
        for f in lossf: assert _equal(qmml.loss(a, b, f), res[f])
