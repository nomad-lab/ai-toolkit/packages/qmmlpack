# qmmlpack
# (c) Matthias Rupp, 2006-2017.
# See LICENSE.txt for license.

"""Unit tests for Gaussian kernel.

Part of qmmlpack library.
"""

import pytest
import numpy as np
from math import exp, sqrt

import qmmlpack as qmml

def _equal(a, b):
    # Equality test for floating point arrays
    return np.allclose(a, b)

#  ############################
#  #  From distance matrix D  #
#  ############################

class TestKernelGaussianD:
    def test_one_sample(self):
        dd = np.array( [ [ 0 ] ] );
        rr = np.array( [ [ 1 ] ] );
        assert _equal(qmml.kernel_gaussian(dd, theta=[1], distance=True), rr)

    def test_two_samples1(self):
        dd = np.array( [ [ 0, 17 ], [ 17, 0 ] ] );
        rr = np.array( [ [ 1, exp(-17./2) ], [ exp(-17./2), 1 ] ] );
        assert _equal(qmml.kernel_gaussian(dd, theta=[1], distance=True), rr)

    def test_two_samples2(self):
        # check also passing a scalar as parameter
        dd = np.array( [ [ 0, 8 ], [ 8, 0 ] ] );
        rr = np.array( [ [ 1, 1/exp(1) ], [ 1/exp(1), 1 ] ] );
        assert _equal(qmml.kernel_gaussian(dd, theta=2, distance=True), rr)

    def test_distance_diagonal(self):
        dd = np.array( [ [ 0, 17 ], [ 17, 0 ] ] );
        rr = np.array( [ 1, 1 ] );
        assert _equal(qmml.kernel_gaussian(dd, theta=[1], distance=True, diagonal=True), rr)

    def test_symmetry(self):
        kc = qmml.kernel_gaussian(qmml.distance_squared_euclidean(np.random.uniform(-1, 1, (   2,   1))), theta= 1, distance=True); assert ( kc == kc.T ).all()
        kc = qmml.kernel_gaussian(qmml.distance_squared_euclidean(np.random.uniform(-1, 1, (  20,  10))), theta= 2, distance=True); assert ( kc == kc.T ).all()
        kc = qmml.kernel_gaussian(qmml.distance_squared_euclidean(np.random.uniform(-1, 1, ( 100, 500))), theta=13, distance=True); assert ( kc == kc.T ).all()
        kc = qmml.kernel_gaussian(qmml.distance_squared_euclidean(np.random.uniform(-1, 1, (1000, 100))), theta= 6, distance=True); assert ( kc == kc.T ).all()


#  ############################
#  #  From distance matrix E  #
#  ############################

class TestKernelGaussianE:
    def test_one_vs_one(self):
        ee = np.array( [ [ 1 ] ] );
        rr = np.array( [ [ exp(-1) ] ] );
        assert _equal(qmml.kernel_gaussian(ee, theta=[sqrt(0.5)], distance=True), rr)

    def three_vs_two(self):
        ee = np.array( [ [ 4, 45 ], [ 25, 8 ], [ 9, 34 ] ] );
        rr = np.array( [ [ exp(-2), exp(-45/2) ], [ exp(-25/2), exp(-4) ], [ exp(-9/2), exp(-17) ] ] );
        assert _equal(qmml.kernel_gaussian(ee, theta=[1], distance=True), rr)

    def two_vs_three(self):
        # check also passing a scalar as parameter
        ee = np.array( [ [ 10, 2, 4 ], [ 5, 5, 1 ] ] );
        rr = np.array( [ [ exp(-10), exp(-2), exp(-4) ], [ exp(-5), exp(-5), exp(-1) ] ] );
        assert _equal(qmml.kernel_gaussian(ee, theta=sqrt(0.5), distance=True), rr)


#  #####################
#  #  Kernel matrix K  #
#  #####################

class TestKernelGaussianK:
    def test_one_sample(self):
        xx = np.array( [ [ 1 ] ] );
        rr = np.array( [ [ 1 ] ] );
        assert _equal(qmml.kernel_gaussian(xx, theta=[1.]), rr)

    def test_two_samples1(self):
        xx = np.array( [ [ 1, -1 ], [ 2, 3 ] ] );
        rr = np.array( [ [ 1, exp(-17/2) ], [ exp(-17/2), 1 ] ] );
        assert _equal(qmml.kernel_gaussian(xx, theta=[1.]), rr)

    def test_two_samples2(self):
        # check also passing a scalar as parameter
        xx = np.array( [ [ 1, 2 ], [ 3, 4 ] ] );
        rr = np.array( [ [ 1, 1/exp(1) ], [ 1/exp(1), 1 ] ] );
        assert _equal(qmml.kernel_gaussian(xx, theta=2), rr)

    def test_symmetry(self):
        kc = qmml.kernel_gaussian(np.random.uniform(-1, 1, (  2,    1)), theta= 1); assert ( kc == kc.T ).all()
        kc = qmml.kernel_gaussian(np.random.uniform(-1, 1, (  20,  10)), theta= 2); assert ( kc == kc.T ).all()
        kc = qmml.kernel_gaussian(np.random.uniform(-1, 1, ( 100, 500)), theta=13); assert ( kc == kc.T ).all()
        kc = qmml.kernel_gaussian(np.random.uniform(-1, 1, (1000, 100)), theta= 6); assert ( kc == kc.T ).all()


#  #####################
#  #  Kernel matrix L  #
#  #####################

class TestKernelGaussianL:
    def test_one_vs_one1(self):
        xx = np.array( [ [ 1 ] ] );
        zz = np.array( [ [ 2 ] ] );
        rr = np.array( [ [ exp(-1) ] ] );
        assert _equal(qmml.kernel_gaussian(xx, zz, theta=[sqrt(0.5)]), rr)

    def test_one_vs_one2(self):
        xx = np.array( [ [ 1, -1 ] ] );
        zz = np.array( [ [ 2,  3 ] ] );
        rr = np.array( [ [ exp(-17/2) ] ] );
        assert _equal(qmml.kernel_gaussian(xx, zz, theta=[1]), rr)

    def test_three_vs_two1(self):
        xx = np.array( [ [ 1, -1 ], [ 2, 3 ], [ -1, 2 ] ] );
        zz = np.array( [ [ -1, -1 ], [ 4, 5 ] ] );
        rr = np.array( [ [ exp(-2), exp(-45/2) ], [ exp(-25/2), exp(-4) ], [ exp(-9/2), exp(-17) ] ] );
        assert _equal(qmml.kernel_gaussian(xx, zz, theta=[1]), rr)

    def test_three_vs_two2(self):
        # check also passing a scalar as parameter
        xx = np.array( [ [ -2, 1 ], [ 0, 1 ], [ -1, 2 ] ] );
        zz = np.array( [ [ 1, 2 ], [ -1, 3 ] ] );
        rr = np.array( [ [ exp(-10), exp(-5) ], [ exp(-2), exp(-5) ], [ exp(-4), exp(-1) ] ] );
        assert _equal(qmml.kernel_gaussian(xx, zz, theta=sqrt(0.5)), rr)

    def test_two_vs_three(self):
        xx = np.array( [ [ 1, 2 ], [ -1, 3 ] ] );
        zz = np.array( [ [ -2, 1 ], [ 0, 1 ], [ -1, 2 ] ] );
        rr = np.array( [ [ exp(-10), exp(-2), exp(-4) ], [ exp(-5), exp(-5), exp(-1) ] ] );
        assert _equal(qmml.kernel_gaussian(xx, zz, theta=[sqrt(0.5)]), rr)


#  #####################
#  #  Kernel vector m  #
#  #####################

class TestKernelGaussianM:
    def test_one_sample_length_1(self):
        xx = np.array( [ [ 1 ] ] );
        rr = np.array( [ [ 1 ] ] );
        assert _equal(qmml.kernel_gaussian(xx, theta=[10], diagonal=True), rr)

    def test_three_samples(self):
        # check also passing a scalar as parameter
        xx = np.array( [ [ 1, 2 ], [ 3, 4 ], [ 5, 6 ] ] );
        rr = np.array( [ [ 1, 1, 1 ] ] );
        assert _equal(qmml.kernel_gaussian(xx, theta=1, diagonal=True), rr)
