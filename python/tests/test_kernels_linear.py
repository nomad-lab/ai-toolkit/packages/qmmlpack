# qmmlpack
# (c) Matthias Rupp, 2006-2017.
# See LICENSE.txt for license.

"""Unit tests for linear kernel.

Part of qmmlpack library.
"""

import pytest
import numpy as np

import qmmlpack as qmml

def _equal(a, b):
    # Equality test for floating point arrays
    return np.allclose(a, b)

#  #####################
#  #  Kernel matrix K  #
#  #####################

class TestKernelLinearK:
    def test_one_sample(self):
        xx  = np.array([[1.,2.,3.]])
        res = np.array([[14.]])
        assert _equal(qmml.kernel_linear(xx), res)

    def test_two_samples(self):
        xx  = np.array([[1,-1],[ 2, 3]], dtype=np.float)
        res = np.array([[2,-1],[-1,13]], dtype=np.float)
        assert _equal(qmml.kernel_linear(xx), res)

    def test_random_samples(self):
        xx  = np.array([[-3.5578999445797557],[-2.2354209763725947],[6.5842430275922650],[-1.7508029405309670],[3.4757634012503330]])
        res = np.array([
            [ 12.658652015640628,   7.953404167948478 , -23.426077902970164,   6.229181685085201 , -12.366418412680904],
            [  7.953404167948478,   4.997106941606605 , -14.718554977414751,   3.9137816187577443, - 7.76979441606315 ],
            [-23.426077902970164, -14.718554977414751 ,  43.35225624639736 , -11.527712053879055 ,  22.885270940242883],
            [  6.229181685085201,   3.9137816187577443, -11.527712053879055,   3.065310936571881 , - 6.085376783498999],
            [-12.366418412680904, - 7.76979441606315  ,  22.885270940242883, - 6.085376783498999 ,  12.080931221471284]
        ]);
        assert _equal(qmml.kernel_linear(xx), res)

    def test_symmetry(self):
        kc = qmml.kernel_linear(np.random.uniform(-1, 1, (2, 1)))
        assert ( kc == kc.T ).all()

        kc = qmml.kernel_linear(np.random.uniform(-1, 1, (20, 10)))
        assert ( kc == kc.T ).all()

        kc = qmml.kernel_linear(np.random.uniform(-1, 1, (100, 500)))
        assert ( kc == kc.T ).all()

        kc = qmml.kernel_linear(np.random.uniform(-1, 1, (1000, 100)))
        assert ( kc == kc.T ).all()


#  #####################
#  #  Kernel matrix L  #
#  #####################

class TestKernelLinearL:
    def test_one_sample_each1(self):
        xx  = np.array([[1,-1]], dtype=np.float)
        zz  = np.array([[2, 3]], dtype=np.float)
        res = np.array([[-1]],   dtype=np.float)
        assert _equal(qmml.kernel_linear(xx, zz), res)

    def test_one_sample_each2(self):
        xx  = np.array([[1,2, 3]], dtype=np.float)
        zz  = np.array([[2,0,-1]], dtype=np.float)
        res = np.array([[-1]]    , dtype=np.float)
        assert _equal(qmml.kernel_linear(xx, zz), res)

    def test_two_vs_one(self):
        xx  = np.array([[1,2],[3,4]], dtype=np.float)
        zz  = np.array([[-1,-2]]    , dtype=np.float)
        res = np.array([[-5],[-11]] , dtype=np.float)
        assert _equal(qmml.kernel_linear(xx, zz), res)

    def test_one_vs_two(self):
        xx  = np.array([[3,1]]      , dtype=np.float)
        zz  = np.array([[5,6],[0,1]], dtype=np.float)
        res = np.array([[21,1]]     , dtype=np.float)
        assert _equal(qmml.kernel_linear(xx, zz), res)

    def test_two_vs_two(self):
        xx  = np.array([[ 1,  2],[  3,  5]], dtype=np.float)
        zz  = np.array([[-2,- 1],[- 4,- 3]], dtype=np.float)
        res = np.array([[-4,-10],[-11,-27]], dtype=np.float)
        assert _equal(qmml.kernel_linear(xx, zz), res)

    def test_three_vs_two1(self):
        xx  = np.array([[ 1,-1],[ 2, 3],[-1,2]], dtype=np.float)
        zz  = np.array([[-1,-1],[ 4, 5]]       , dtype=np.float)
        res = np.array([[ 0,-1],[-5,23],[-1,6]], dtype=np.float)
        assert _equal(qmml.kernel_linear(xx, zz), res)

    def test_three_vs_two2(self):
        xx  = np.array([[-2,- 1],[- 4,- 3],[- 6,- 5]], dtype=np.float)
        zz  = np.array([[ 1,  2],[  3,  4]]          , dtype=np.float)
        res = np.array([[-4,-10],[-10,-24],[-16,-38]], dtype=np.float)
        assert _equal(qmml.kernel_linear(xx, zz), res)

    def test_two_vs_three(self):
        xx  = np.array([[ 1,  2],[  3, 4]]         , dtype=np.float)
        zz  = np.array([[-2,- 1],[- 4,-3],[-6,-5]] , dtype=np.float)
        res = np.array([[-4,-10,-16],[-10,-24,-38]], dtype=np.float)
        assert _equal(qmml.kernel_linear(xx, zz), res)

#  #####################
#  #  Kernel vector m  #
#  #####################

class TestKernelLinearM:
    def test_one_sample(self):
        xx  = np.array([[1,2,3]], dtype=np.float)
        res = np.array([14]     , dtype=np.float)
        assert _equal(qmml.kernel_linear(xx, diagonal=True), res)

    def test_two_samples(self):
        xx  = np.array([[1,-1],[2,3]], dtype=np.float)
        res = np.array([2,13]        , dtype=np.float)
        assert _equal(qmml.kernel_linear(xx, diagonal=True), res)
