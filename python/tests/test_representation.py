# qmmlpack
# (c) Matthias Rupp, 2006-2016.
# See LICENSE.txt for license.

"""Unit tests for representations.

Part of qmmlpack library.
"""

import pytest
import numpy as np

import qmmlpack as qmml

def _equal(a, b):
    # Equality test for floating point arrays
    return np.allclose(a, b)

#  ##################
#  #  Test systems  #
#  ##################

#  Molecules and crystals for testing purposes

class FiniteTestSystems:
    def __call__(self, systems):
        (z, r) = ([], [])
        for s in systems:
            if   s == 'empty':
                z.append( [] )
                r.append( [] )
            elif s == 'H':
                z.append( [1] )
                r.append( [[0,0,0]] )
            elif s == 'H3':
                z.append( [1, 1, 1] )
                r.append( [ [0,0,0], [0.72,0,0], [1.44,0,0] ] )
            elif s == 'O2':
                z.append( [8, 8] )
                r.append( [ [0,0,0], [1.48,0,0] ] )
            elif s == 'HCl':
                z.append( [1, 17] )
                r.append( [ [0,0,0], [0,1.27,0] ] )
            elif s == 'H2O':
                z.append( [8, 1, 1] )
                r.append( [
                    [ 0.16597615219814216, -0.3140092250490285, -0.07027664259186725],
                    [-0.03622260506424052,  0.5665229602793138,  0.24958159518024825],
                    [-0.63725621934564140, -0.5997033905165634, -0.50814324621155720]
                ] )
            elif s == 'C6H6':
                z.append( [ 6, 6, 6, 6, 6, 6, 1, 1, 1, 1, 1, 1 ] )
                r.append( [
                    [-1.19070, -0.015997, -0.75474],
                    [-0.13956, +0.846770, -1.10680],
                    [-1.05110, -0.862770, +0.35210],
                    [+1.05110, +0.862770, -0.35210],
                    [+0.13956, -0.846770, +1.10680],
                    [+1.19070, +0.015997, +0.75474],
                    [-2.09330, -0.026239, -1.32920],
                    [-0.24311, +1.494900, -1.95190],
                    [-1.84850, -1.522100, +0.62495],
                    [+1.84850, +1.522100, -0.62495],
                    [+0.24311, -1.494900, +1.95190],
                    [+2.09330, +0.026239, +1.32920]
                ] )
            elif s == 'HCN':
                z.append( [ 1, 6, 7 ] )
                r.append( [ [2.5369, -0.095, 0], [3.4030,  0.405, 0], [2.0000, -0.405, 0] ] )
            else:
                raise qmml.QMMLException("Unknown finite test system '" + s + "' requested")
        return ( np.asarray(z), np.asarray(r) )

class PeriodicTestSystems:
    def __call__(self, systems):
        (z, r, b) = ([], [], [])
        for s in systems:
            if   s == 'empty':
                z.append( [] )
                r.append( [] )
                b.append( [ [1,0,0], [0,1,0], [0,0,1] ] )
            elif s == 'NaCl':
                z.append( [ 11, 17 ] )
                r.append( [ [0., 0., 0.], [3.988, 2.302475, 1.628095] ] )
                b.append( [ [3.988, 0, 0],  [1.994, 3.45371, 0],  [1.994, 1.15124, 3.25619] ] )
            elif s == 'Al':
                z.append( [ 13 ] )
                r.append( [ [0, 0, 0] ] )
                b.append( [ [0, 2.02475, 2.02475],  [2.02475, 2.02475, 0],  [2.02475, 0, 2.02475] ] )
            elif s == 'SF6':
                z.append( [ 16, 9, 9, 9, 9, 9, 9 ] )
                r.append( [
                    [ 0.0000,  0.0000, 0.0000],
                    [-0.8070,  4.2386, 1.1293],
                    [ 2.5361, -1.7933, 3.1061],
                    [-2.5361,  1.7933, 3.1061],
                    [-0.9220,  0.6510, 1.1293],
                    [ 0.9220,  1.3040, 0.    ],
                    [ 2.5361,  3.5866, 0.    ]

                ])
                b.append( [ [5.187, 0, 0],  [-1.729, 4.891, 0],  [-1.729, -2.445, 4.235] ] )
            else:
                raise qmml.QMMLException("Unknown periodic test system '" + s + "' requested")
        return ( np.asarray(z), np.asarray(r), np.asarray(b) )

@pytest.fixture(scope="module")
def finiteTestSystems():
    return FiniteTestSystems()

@pytest.fixture(scope="module")
def periodicTestSystems():
    return PeriodicTestSystems()

#  ####################
#  #  Coulomb matrix  #
#  ####################

class TestMoleculeCoulombMatrix:
    def test_defaults(self):
        assert _equal( qmml.coulomb_matrix([[1]], np.asarray([[[0,0,0]]])), [[0.5]] )   # free atom
        assert _equal( qmml.coulomb_matrix([[1,17]], np.asarray([[[0,0,0],[0,1.27,0]]])), [[448.79438598087216,7.0834744715621785],[7.0834744715621785,0.5]] )  # HCl diatomic

    def test_unit(self):
        result = np.asarray( [[0.5,0.7151043390810811],[0.7151043390810811,0.5]] )
        xyz = np.asarray( [[[0,0,-74],[0,0,0]]], dtype=np.float_ )
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz/100), result )  # default is Angstrom
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz/100, unit='Angstroms'), result )  # explicit
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz/100, unit='A'), result )  # alternative spelling
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz, unit='Picometers'), result )
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz, unit='pm'), result )  # alternative spelling
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz*0.0188972612, unit='BohrRadius'), result )
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz*0.0188972612, unit='a0'), result )  # alternative spelling

    def test_padding(self):
        xyz = np.asarray( [[[0,0,0],[0,0,0.74]]], dtype=np.float_ )
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz), [[0.5,0.715104339083375], [0.715104339083375,0.5]] ) # default is no padding
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz, padding=1), [[0.5]] ) # negative padding
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz, padding=2), [[0.5,0.715104339083375], [0.715104339083375,0.5]] ) # no result
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz, padding=3), [[0.5,0.715104339083375,0.], [0.715104339083375,0.5,0.], [0.,0.,0.]] ) # one additional dimension
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz, padding=4), [[0.5,0.715104339083375,0.,0.], [0.715104339083375,0.5,0.,0.], [0.,0.,0.,0.], [0.,0.,0.,0.]] ) # two additional dimensions

    def test_flatten(self):
        xyz = np.asarray( [[[0,0,0],[0.74,0,0]]], dtype=np.float_ )
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz), [[0.5,0.715104339083375], [0.715104339083375,0.5]] )  # default is not to flatten
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz, flatten=True), [0.5,0.715104339083375,0.5] )
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz, flatten=0), [0.5,0.715104339083375,0.5] )  # same result as True
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz, flatten=1), [0.5,0.715104339083375,0.715104339083375,0.5] )
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz, flatten=2), [0.5,0.715104339083375,0.715104339083375,0.5] )
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz, flatten=-1), [0.715104339083375] )
        assert _equal( qmml.coulomb_matrix([[1,1]], xyz, flatten=-2), [] )

    def test_sort(self):
        xyz = np.asarray( [[[1,1,1],[1,1,2.27]]], dtype=np.float_ )
        assert _equal( qmml.coulomb_matrix([[1,17]], xyz), [[448.79438598087216,7.0834744715621785],[7.0834744715621785,0.5]] )  # default is sorting by row norm
        assert _equal( qmml.coulomb_matrix([[1,17]], xyz, sort=True), [[448.79438598087216,7.0834744715621785],[7.0834744715621785,0.5]] )  # same result
        assert _equal( qmml.coulomb_matrix([[1,17]], xyz, sort=False), [[0.5,7.0834744715621785],[7.0834744715621785,448.79438598087216]] )
        assert _equal( qmml.coulomb_matrix([[1,17]], xyz, sort=lambda cm: [0,1]), [[0.5,7.0834744715621785],[7.0834744715621785,448.79438598087216]] )
        assert _equal( qmml.coulomb_matrix([[1,17]], xyz, sort=lambda cm: [1,0]), [[448.79438598087216,7.0834744715621785],[7.0834744715621785,0.5]] )

    def test_postf(self):
        xyz = np.asarray( [[[1,1,1],[1,1,2.27]]], dtype=np.float_ )
        assert _equal( qmml.coulomb_matrix([[1,17]], xyz), [[448.79438598087216,7.0834744715621785],[7.0834744715621785,0.5]] )  # default is identity
        assert _equal( qmml.coulomb_matrix([[1,17]], xyz, post=lambda cmij, zi, zj, dij, i, j: cmij), [[448.79438598087216,7.0834744715621785],[7.0834744715621785,0.5]] )  # identity
        assert _equal( qmml.coulomb_matrix([[1,17]], xyz, post=lambda cmij, zi, zj, dij, i, j: cmij+1), [[449.79438598087216,8.0834744715621785],[8.0834744715621785,1.5]] )  # argument 1
        assert _equal( qmml.coulomb_matrix([[1,17]], xyz, post=lambda cmij, zi, zj, dij, i, j: zi), [[17,17],[1,1]] )  # argument 2
        assert _equal( qmml.coulomb_matrix([[1,17]], xyz, post=lambda cmij, zi, zj, dij, i, j: zj), [[17,1],[17,1]] )  # argument 3
        assert _equal( qmml.coulomb_matrix([[1,17]], xyz, post=lambda cmij, zi, zj, dij, i, j: dij), [[0,2.3999521781899302],[2.3999521781899302,0]] )  # argument 4
        assert _equal( qmml.coulomb_matrix([[1,17]], xyz, post=lambda cmij, zi, zj, dij, i, j: i), [[0,0],[1,1]] )  # argument 5
        assert _equal( qmml.coulomb_matrix([[1,17]], xyz, post=lambda cmij, zi, zj, dij, i, j: j), [[0,1],[0,1]] )  # argument 6

    def test_postf_no_sorting(self):
        assert _equal( \
            qmml.coulomb_matrix([[1,17]], np.asarray([[[1,1,1],[1,1,2.27]]]), padding=3, flatten=False, sort=False, post=lambda cmij, zi, zj, dij, i, j: cmij + zi + zj),
            [[[0.5+2, 7.0834744715621785+18, 0.], [7.0834744715621785+18, 448.79438598087216+34, 0.], [0., 0., 0.]]] )

    def test_example_azanone(self): # nitroxyl
        z = [[ 8, 7, 1 ]]
        r = np.asarray( [ [ [ -0.6175, 0, 0 ], [ 0.6175, 0, 0 ], [ 0.9797, 0.6825, 0.6518 ] ] ], dtype=np.float_ )
        assert _equal( qmml.coulomb_matrix(z, r), [ [ [73.5167, 23.9951, 2.28194], [23.9951, 53.3587, 3.66444], [2.28194, 3.66444, 0.5] ] ] )
        assert _equal( qmml.coulomb_matrix(z, r, padding=2, flatten=True, post=lambda cmij, zi, zj, dij, i, j: 2*cmij), [ 73.5167*2, 23.9951*2, 53.3587*2 ] )
        assert _equal( qmml.coulomb_matrix(z, r, padding=4), [ [73.5167, 23.9951, 2.28194, 0.], [23.9951, 53.3587, 3.66444, 0.], [2.28194, 3.66444, 0.5, 0.], [0., 0., 0., 0.] ] )
        assert _equal( qmml.coulomb_matrix(z, r, padding=4, sort=lambda cm: [2,1,0]), [ [0.5, 3.66444, 2.28194, 0.], [3.66444, 53.3587, 23.9951, 0.], [2.28194, 23.9951, 73.5167, 0.], [0., 0., 0., 0.] ] )
        assert _equal( qmml.coulomb_matrix(z, r, padding=4, flatten=-2, sort=lambda cm: [2,1,0]), [2.28194, 0., 0.] )

    def test_invalid_option_values(self):
        with pytest.raises(qmml.QMMLException): qmml.coulomb_matrix([1], np.asarray([[0,0,0]]), unit='p4Kd')
        with pytest.raises(qmml.QMMLException): qmml.coulomb_matrix([1], np.asarray([[0,0,0]]), padding='abc')


#  #####################################
#  #  Many-body tensor representation  #
#  #####################################

class TestManyBodyTensorFinite:

    # 1-body

    def test_H_unity_unity2(self, finiteTestSystems):
        (z, r) = finiteTestSystems(["H"])
        mbtr = qmml.many_body_tensor( z, r, [0.5, 1., 1], [1, "unity", "unity", ("normal", (1e-10,)), "identity", "full", "full"] )
        assert _equal( mbtr, [[[1]]] ) and mbtr.ndim == 3
        mbtr = qmml.many_body_tensor( z, r, [0.5, 1., 1], [1, "unity", "unity", ("normal", (1e-10,)), "identity", "full", "full"], flatten=True )
        assert _equal( mbtr, [[1]] ) and mbtr.ndim == 2

    def test_H3_count_unity(self, finiteTestSystems):
        (z, r) = finiteTestSystems(["H3"])
        mbtr = qmml.many_body_tensor( z, r, [0.5, 1, 3], [1, "count", "unity", ("normal", (1e-9,)), "identity", "full", "full"] )
        assert _equal( mbtr, [[0,0,3]] )

    def test_H_H3_count_oneoveridentity(self, finiteTestSystems):
        (z, r) = finiteTestSystems(['H', 'H3'])
        mbtr = qmml.many_body_tensor( z, r, [0.5, 1, 3], [1, "count", "1/identity", ("normal", (1e-8,)), "identity", "full", "full"] )
        assert _equal( mbtr, [ [[1,0,0]], [[0,0,1]]] )

    def test_H_H3_unity_unity(self, finiteTestSystems):
        (z, r) = finiteTestSystems(['H', 'H3'])
        mbtr = qmml.many_body_tensor( z, r, [-0.5, 1, 3], [1, "unity", "unity", ("normal", (1e-7,)), "identity", "full", "full"] )
        assert _equal( mbtr, [ [[0,1,0]], [[0,3,0]]] )

    def test_H3_O2_unity_unity(self, finiteTestSystems):
        (z, r) = finiteTestSystems(['H3', 'O2'])
        mbtr = qmml.many_body_tensor( z, r, [-0.5, 1, 3], [1, "unity", "unity", ("normal", (1e-7,)), "identity", "full", "full"] )
        assert _equal( mbtr, [ [[0,3,0], [0,0,0]],  [[0,0,0], [0,2,0]]] )

    def test_H_count_oneoveridentity(self, finiteTestSystems):
        (z, r) = finiteTestSystems(['H'])
        mbtr = qmml.many_body_tensor( z, r, [-4, 1, 10], [1, "count", "1/identity", ("normal", (0.5,)), "identity", "full", "full"], flatten=True )
        assert _equal( mbtr, [ [6.20081e-16, 9.86587e-10, 0.0000316703, 0.0227185, 0.47725, 0.47725, 0.0227185, 0.0000316703, 9.86587e-10, 6.20081e-16] ] )

    # 2-body

    def test_O2_oneoverdistance_unity(self, finiteTestSystems):
        (z, r) = finiteTestSystems(['O2'])
        mbtr = qmml.many_body_tensor( z, r, [-7.5, 0.1, 160], [2, "1/distance", "unity", ("normal", (1e-6,)), "identity", "full", "full"], flatten=True )
        assert _equal( mbtr, [ [
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, \
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, \
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, \
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        ] ] )

        mbtr = qmml.many_body_tensor( z, r, [-7.5, 0.1, 160], [2, "1/distance", "unity", ("normal", (1e-6,)), "identity", "full", "noreversals"], flatten=True )
        assert _equal( mbtr, [ [
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, \
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, \
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, \
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        ]])

    def test_O2_oneoverdistance_unity2(self, finiteTestSystems):
        (z, r) = finiteTestSystems(['O2'])
        mbtr = qmml.many_body_tensor( z, r, [-7.5, 0.1, 160], [2, "1/distance", "unity", ("normal", (1.5,)), "identity", "noreversals", "noreversals"], flatten=True )
        assert _equal( mbtr, [ [
            1.13469e-8  , 1.62441e-8  , 2.31517e-8  , 3.28506e-8  , 4.6406e-8   , 6.52642e-8  , 9.1379e-8   , 1.27376e-7  , 1.76767e-7  , 2.44222e-7  ,
            3.35921e-7  , 4.60004e-7  , 6.27128e-7  , 8.5118e-7   , 1.15016e-6  , 1.54726e-6  , 2.07224e-6  , 2.76304e-6  , 3.66779e-6  , 4.84722e-6  ,
            6.37751e-6  , 8.35373e-6  , 0.0000108938, 0.0000141433, 0.0000182806, 0.0000235235, 0.0000301359, 0.0000384358, 0.0000488044, 0.0000616953,
            0.0000776455, 0.000097286 , 0.000121354 , 0.000150706 , 0.000186327 , 0.000229347 , 0.000281048 , 0.000342876 , 0.000416452 , 0.000503574 ,
            0.000606223 , 0.000726561 , 0.000866926 , 0.00102982  , 0.00121791  , 0.00143395  , 0.00168084  , 0.00196151  , 0.00227889  , 0.00263588  ,
            0.00303529  , 0.00347973  , 0.00397155  , 0.0045128   , 0.00510508  , 0.00574948  , 0.00644653  , 0.00719605  , 0.00799709  , 0.00884791  ,
            0.00974586  , 0.0106874   , 0.0116678   , 0.0126818   , 0.0137228   , 0.0147834   , 0.0158554   , 0.0169297   , 0.0179967   , 0.0190461   ,
            0.0200674   , 0.0210497   , 0.0219822   , 0.0228543   , 0.0236556   , 0.0243765   , 0.025008    , 0.0255421   , 0.025972    , 0.0262921   ,
            0.0264981   , 0.0265873   , 0.0265586   , 0.0264123   , 0.0261504   , 0.0257763   , 0.0252949   , 0.0247125   , 0.0240364   , 0.0232753   ,
            0.0224383   , 0.0215355   , 0.0205774   , 0.0195748   , 0.0185385   , 0.0174792   , 0.0164074   , 0.015333    , 0.0142655   , 0.0132134   ,
            0.0121847   , 0.0111863   , 0.0102242   , 0.00930334  , 0.00842793  , 0.00760105  , 0.0068249   , 0.00610085  , 0.00542943  , 0.00481049  ,
            0.0042432   , 0.00372623  , 0.00325774  , 0.00283552  , 0.00245708  , 0.00211971  , 0.00182056  , 0.0015567   , 0.00132518  , 0.00112309  ,
            0.000947597 , 0.000795985 , 0.000665665 , 0.000554214 , 0.000459378 , 0.000379082 , 0.000311434 , 0.000254724 , 0.000207417 , 0.000168147 ,
            0.000135708 , 0.000109041 , 0.0000872263, 0.0000694664, 0.0000550772, 0.0000434751, 0.0000341648, 0.0000267293, 0.0000208194, 0.0000161442,
            0.0000124634, 9.5792e-6   , 7.32978e-6  , 5.58371e-6  , 4.23473e-6  , 3.19741e-6  , 2.40349e-6  , 1.79869e-6  , 1.34011e-6  , 9.94024e-7  ,
            7.34045e-7  , 5.39658e-7  , 3.9499e-7   , 2.87821e-7  , 2.088e-7    , 1.50803e-7  , 1.08432e-7  , 7.76206e-8  , 5.53181e-8  , 3.92489e-8
        ] ] )

    def test_H2O_oneoverdistance_unity(self, finiteTestSystems):
        (z, r) = finiteTestSystems(['H2O'])
        mbtr = qmml.many_body_tensor( z, r, [0.5, 0.05, 20], [2, "1/distance", "unity", ("normal", (1e-6,)), "identity", "noreversals", "noreversals"] )
        assert _equal( mbtr, [ [
            [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # HH
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # HO
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]   # OO
        ] ] )

    # 3-body

    def test_H2O_angle_unity(self, finiteTestSystems):
        (z, r) = finiteTestSystems(['H2O'])
        mbtr = qmml.many_body_tensor( z, r, [0, 0.15707963267948966, 20], [3, "angle", "unity", ("normal", (1e-6,)), "identity", "noreversals", "noreversals"] )
        assert _equal( mbtr, [ [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # HHH
            [0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # HHO  37.775 degree
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],  # HOH  104.45 degree
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # HOO
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # OHO
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]   # OOO
        ] ] )

    # 4-body

    def test_C6H6_unity_unity(self, finiteTestSystems):
        (z, r) = finiteTestSystems(['C6H6'])
        mbtr = qmml.many_body_tensor( z, r, [0.5, 1, 1], [4, "unity", "unity", ("normal", (1e-6,)), "identity", "noreversals", "noreversals"], flatten=True )
        assert _equal( mbtr, np.asfarray([ [ 180, 720, 720, 900, 900, 450, 720, 450, 720, 180 ] ]) )

class TestManyBodyTensorPeriodic:

    # special cases and error handling

    # 1-body

    def test_NaCl_unity_unity(self, periodicTestSystems):
        (z, r, b) = periodicTestSystems(["NaCl"])
        mbtr = qmml.many_body_tensor( z, r, [-0.5, 1, 3], [1, "unity", "unity", ("normal", (1e-10,)), "identity", "noreversals", "full"], basis=b )
        assert _equal( mbtr, np.asfarray([ [[0, 1, 0],  [0, 1, 0]] ]) )

    # 2-body

    def test_Al_oneoverdistance_delta1(self, periodicTestSystems):
        (z, r, b) = periodicTestSystems(["Al"])
        mbtr = qmml.many_body_tensor( z, r, [-10, 20, 1], [2, "1/distance", ("delta_1/identity", (2.8,)), ("normal", (1e-10,)), "identity", "noreversals", "full"], basis=b )
        assert _equal( mbtr, np.asfarray([[[0]]]) )

    def test_Al_oneoverdistance_delta2(self, periodicTestSystems):
        (z, r, b) = periodicTestSystems(["Al"])
        mbtr = qmml.many_body_tensor( z, r, [0, 0.2, 10], [2, "1/distance", ("delta_1/identity", (2.87,)), ("normal", (1e-10,)), "identity", "noreversals", "full"], basis=b, flatten=True )
        assert _equal( mbtr, np.asfarray([[ 0, 12, 0, 0, 0, 0, 0, 0, 0, 0 ]]) )

    def test_Al_oneoverdistance_delta3(self, periodicTestSystems):
        (z, r, b) = periodicTestSystems(["Al"])
        mbtr = qmml.many_body_tensor( z, r, [0, 0.025, 20], [2, "1/distance", ("delta_1/identity", (5.72687,)), ("normal", (1e-10,)), "identity", "full", "full"], basis=b, flatten=True )
        assert _equal( mbtr, np.asfarray([[ 0, 0, 0, 0, 0, 0, 12, 0, 24, 6, 0, 0, 0, 12, 0, 0, 0, 0, 0, 0 ]]) )
