# qmmlpack
# (c) Matthias Rupp, 2006-2017.
# See LICENSE.txt for license.

"""Unit tests for utility functions.

Part of qmmlpack library.
"""

import pytest
import numpy as np

import qmmlpack as qmml

def _equal(a, b):
    # Equality test for floating point arrays
    return np.allclose(a, b)

#  ##################
#  #  element_data  #
#  ##################

class TestElementData:
    def test_an_abrv(self):
        inds = list(range(1, 119))
        abrv = [qmml.element_data(i, 'abbreviation') for i in inds]
        anum = [qmml.element_data(a, 'atomic number') for a in abrv]
        assert inds == anum
