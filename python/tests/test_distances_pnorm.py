# qmmlpack
# (c) Matthias Rupp, 2006-2016.
# See LICENSE.txt for license.

"""Unit tests for distance functions based on p-norms.

Part of qmmlpack library.
"""

import pytest
import numpy as np
import scipy as sp
import scipy.spatial

import qmmlpack as qmml

def _equal(a, b):
    # Equality test for floating point arrays
    return np.allclose(a, b, atol=1e-07)

#  ##############################
#  #  One-norm distance matrix  #
#  ##############################

class TestDistanceOneNormK:
    def test_vs_scipy_small(self):
        xx = np.random.random((11,5))
        assert _equal(sp.spatial.distance.squareform(sp.spatial.distance.pdist(xx, 'cityblock')), qmml.distance_one_norm(xx))
        assert _equal(qmml.distance_one_norm(xx), qmml.DistanceOneNorm()(xx))

    def test_vs_scipy_large(self):
        xx = np.random.random((299,111))
        assert _equal(sp.spatial.distance.squareform(sp.spatial.distance.pdist(xx, 'cityblock')), qmml.distance_one_norm(xx))
        assert _equal(qmml.distance_one_norm(xx), qmml.DistanceOneNorm()(xx))

class TestDistanceOneNormL:
    def test_vs_scipy_small(self):
        xx = np.random.random((12,6));
        zz = np.random.random((17,6));
        assert _equal(sp.spatial.distance.cdist(xx, zz, 'cityblock'), qmml.distance_one_norm(xx, zz))
        assert _equal(qmml.distance_one_norm(xx, zz), qmml.DistanceOneNorm()(xx, zz))

    def test_vs_scipy_large(self):
        xx = np.random.random((300,129));
        zz = np.random.random((151,129));
        assert _equal(sp.spatial.distance.cdist(xx, zz, 'cityblock'), qmml.distance_one_norm(xx, zz))
        assert _equal(qmml.distance_one_norm(xx, zz), qmml.DistanceOneNorm()(xx, zz))


#  ###############################
#  #  Euclidean distance matrix  #
#  ###############################

class TestDistanceEuclideanK:
    def test_vs_scipy(self):
        xx = np.random.random((50,5))
        assert _equal(sp.spatial.distance.squareform(sp.spatial.distance.pdist(xx, 'euclidean')), qmml.distance_euclidean(xx))
        assert _equal(qmml.distance_euclidean(xx), qmml.DistanceEuclidean()(xx))

class TestDistanceEuclideanL:
    def test_vs_scipy(self):
        xx = np.random.random((50,5));
        zz = np.random.random((100,5));
        assert _equal(sp.spatial.distance.cdist(xx, zz, 'euclidean'), qmml.distance_euclidean(xx, zz))
        assert _equal(qmml.distance_euclidean(xx, zz), qmml.DistanceEuclidean()(xx, zz))


#  #######################################
#  #  Squared Euclidean distance matrix  #
#  #######################################

class TestDistanceSquaredEuclideanK:
    def test_vs_scipy(self):
        xx = np.random.random((50,5))
        assert _equal(sp.spatial.distance.squareform(sp.spatial.distance.pdist(xx, 'sqeuclidean')), qmml.distance_squared_euclidean(xx))
        assert _equal(qmml.distance_squared_euclidean(xx), qmml.DistanceSquaredEuclidean()(xx))

class TestDistanceSquaredEuclideanL:
    def test_vs_scipy(self):
        xx = np.random.random((50,5));
        zz = np.random.random((100,5));
        assert _equal(sp.spatial.distance.cdist(xx, zz, 'sqeuclidean'), qmml.distance_squared_euclidean(xx, zz))
        assert _equal(qmml.distance_squared_euclidean(xx, zz), qmml.DistanceSquaredEuclidean()(xx, zz))
