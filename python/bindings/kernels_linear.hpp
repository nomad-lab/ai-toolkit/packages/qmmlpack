// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Python bindings for linear kernel

#ifndef QMML_PYTHON_KERNELS_LINEAR_HPP_INCLUDED  // include guard
#define QMML_PYTHON_KERNELS_LINEAR_HPP_INCLUDED

#include "python.hpp"

// kernels_linear

void _kernel_matrix_linear_k(py_cdarray_t kk, py_cdarray_t xx);
void _kernel_matrix_linear_l(py_cdarray_t ll, py_cdarray_t xx, py_cdarray_t zz);
void _kernel_matrix_linear_m(py_cdarray_t m, py_cdarray_t xx);

// Python bindings

void init_kernels_linear(py::module &m);

#endif // include guard
