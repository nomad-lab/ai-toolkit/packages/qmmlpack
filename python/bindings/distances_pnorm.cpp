// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Python bindings for p-norm distances

#include "distances_pnorm.hpp"
#include "qmmlpack/distances_pnorm.hpp"

// One-norm distance

void _distance_matrix_one_norm_k(py_cdarray_t dd, py_cdarray_t xx)
{
    auto bufd = dd.request(), bufx = xx.request();

    if(bufd.ndim != 2 || bufx.ndim != 2) throw QMML_ERROR("_distance_matrix_one_norm_k arguments must be matrices");

    const size_t n = bufx.shape[0];  // number of samples
    const size_t d = bufx.shape[1];  // number of dimensions

    if(bufd.shape[0] != n) throw QMML_ERROR("_distance_matrix_one_norm_k: X and D must have same number of rows")
    if(bufd.shape[1] != n) throw QMML_ERROR("_distance_matrix_one_norm_k: D must be quadratic");

    qmml::distance_matrix_one_norm_k(static_cast<double*>(bufd.ptr), static_cast<double*>(bufx.ptr), n, d);
}

void _distance_matrix_one_norm_l(py_cdarray_t dd, py_cdarray_t xx, py_cdarray_t zz)
{
    auto bufd = dd.request(), bufx = xx.request(), bufz = zz.request();

    if(bufd.ndim != 2 || bufx.ndim != 2 || bufz.ndim != 2) throw QMML_ERROR("_distance_matrix_one_norm_l arguments must be matrices");

    const size_t n = bufx.shape[0];  // number of samples in X
    const size_t m = bufz.shape[0];  // number of samples in Z
    const size_t d = bufx.shape[1];  // number of dimensions

    if(bufd.shape[0] != n) throw QMML_ERROR("_distance_matrix_one_norm_l: X and D must have same number of rows");
    if(bufd.shape[1] != m) throw QMML_ERROR("_distance_matrix_one_norm_l: Z and D must have same number of columns");
    if(bufz.shape[1] != d) throw QMML_ERROR("_distance_matrix_one_norm_l: X and Z must have same number of columns");

    qmml::distance_matrix_one_norm_l(static_cast<double*>(bufd.ptr), static_cast<double*>(bufx.ptr), static_cast<double*>(bufz.ptr), n, m, d);
}


// Euclidean distance

void _distance_matrix_euclidean_k(py_cdarray_t dd, py_cdarray_t xx)
{
    auto bufd = dd.request(), bufx = xx.request();

    if(bufd.ndim != 2 || bufx.ndim != 2) throw QMML_ERROR("_distance_matrix_euclidean_k arguments must be matrices");

    const size_t n = bufx.shape[0];  // number of samples
    const size_t d = bufx.shape[1];  // number of dimensions

    if(bufd.shape[0] != n) throw QMML_ERROR("_distance_matrix_euclidean_k: X and D must have same number of rows")
    if(bufd.shape[1] != n) throw QMML_ERROR("_distance_matrix_euclidean_k: D must be quadratic");

    qmml::distance_matrix_euclidean_k(static_cast<double*>(bufd.ptr), static_cast<double*>(bufx.ptr), n, d);
}

void _distance_matrix_euclidean_l(py_cdarray_t dd, py_cdarray_t xx, py_cdarray_t zz)
{
    auto bufd = dd.request(), bufx = xx.request(), bufz = zz.request();

    if(bufd.ndim != 2 || bufx.ndim != 2 || bufz.ndim != 2) throw QMML_ERROR("_distance_matrix_euclidean_l arguments must be matrices");

    const size_t n = bufx.shape[0];  // number of samples in X
    const size_t m = bufz.shape[0];  // number of samples in Z
    const size_t d = bufx.shape[1];  // number of dimensions

    if(bufd.shape[0] != n) throw QMML_ERROR("_distance_matrix_euclidean_l: X and D must have same number of rows");
    if(bufd.shape[1] != m) throw QMML_ERROR("_distance_matrix_euclidean_l: Z and D must have same number of columns");
    if(bufz.shape[1] != d) throw QMML_ERROR("_distance_matrix_euclidean_l: X and Z must have same number of columns");

    qmml::distance_matrix_euclidean_l(static_cast<double*>(bufd.ptr), static_cast<double*>(bufx.ptr), static_cast<double*>(bufz.ptr), n, m, d);
}


// Squared Euclidean distance

void _distance_matrix_squared_euclidean_k(py_cdarray_t dd, py_cdarray_t xx)
{
    auto bufd = dd.request(), bufx = xx.request();

    if(bufd.ndim != 2 || bufx.ndim != 2) throw QMML_ERROR("_distance_matrix_squared_euclidean_k arguments must be matrices");

    const size_t n = bufx.shape[0];  // number of samples
    const size_t d = bufx.shape[1];  // number of dimensions

    if(bufd.shape[0] != n) throw QMML_ERROR("_distance_matrix_squared_euclidean_k: X and D must have same number of rows")
    if(bufd.shape[1] != n) throw QMML_ERROR("_distance_matrix_squared_euclidean_k: D must be quadratic");

    qmml::distance_matrix_squared_euclidean_k(static_cast<double*>(bufd.ptr), static_cast<double*>(bufx.ptr), n, d);
}

void _distance_matrix_squared_euclidean_l(py_cdarray_t dd, py_cdarray_t xx, py_cdarray_t zz)
{
    auto bufd = dd.request(), bufx = xx.request(), bufz = zz.request();

    if(bufd.ndim != 2 || bufx.ndim != 2 || bufz.ndim != 2) throw QMML_ERROR("_distance_matrix_squared_euclidean_l arguments must be matrices");

    const size_t n = bufx.shape[0];  // number of samples in X
    const size_t m = bufz.shape[0];  // number of samples in Z
    const size_t d = bufx.shape[1];  // number of dimensions

    if(bufd.shape[0] != n) throw QMML_ERROR("_distance_matrix_squared_euclidean_l: X and D must have same number of rows");
    if(bufd.shape[1] != m) throw QMML_ERROR("_distance_matrix_squared_euclidean_l: Z and D must have same number of columns");
    if(bufz.shape[1] != d) throw QMML_ERROR("_distance_matrix_squared_euclidean_l: X and Z must have same number of columns");

    qmml::distance_matrix_squared_euclidean_l(static_cast<double*>(bufd.ptr), static_cast<double*>(bufx.ptr), static_cast<double*>(bufz.ptr), n, m, d);
}

// Python bindings

void init_distances_pnorm(py::module &m)
{
    // One-norm distance
    m.def("_distance_matrix_one_norm_k", &_distance_matrix_one_norm_k, "Computes one-norm distance matrix for one set of vectors", py::arg("D"), py::arg("X"));
    m.def("_distance_matrix_one_norm_l", &_distance_matrix_one_norm_l, "Computes one-norm distance matrix for two sets of vectors", py::arg("D"), py::arg("X"), py::arg("Z"));

    // Euclidean distance
    m.def("_distance_matrix_euclidean_k", &_distance_matrix_euclidean_k, "Computes Euclidean distance matrix for one set of vectors", py::arg("D"), py::arg("X"));
    m.def("_distance_matrix_euclidean_l", &_distance_matrix_euclidean_l, "Computes Euclidean distance matrix for two sets of vectors", py::arg("D"), py::arg("X"), py::arg("Z"));

    // Squared Euclidean distance
    m.def("_distance_matrix_squared_euclidean_k", &_distance_matrix_squared_euclidean_k, "Computes squared Euclidean distance matrix for one set of vectors", py::arg("D"), py::arg("X"));
    m.def("_distance_matrix_squared_euclidean_l", &_distance_matrix_squared_euclidean_l, "Computes squared Euclidean distance matrix for two sets of vectors", py::arg("D"), py::arg("X"), py::arg("Z"));
}
