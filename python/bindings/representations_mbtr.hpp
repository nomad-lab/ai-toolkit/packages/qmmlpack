// qmmlpack
// (c) Matthias Rupp, 2006-2017.
// See LICENSE.txt for license.

// Python bindings for many-body tensor representation

#ifndef QMML_PYTHON_REPRESENTATIONS_MANYBODYTENSOR_HPP_INCLUDED  // include guard
#define QMML_PYTHON_REPRESENTATIONS_MANYBODYTENSOR_HPP_INCLUDED

#include "python.hpp"

#include <string>

// representation_mbtr

py::array_t<double> _representation_mbtr(
    long k,
    py_clarray_t ssizes, py_clarray_t sinds, py_cdarray_t bases, py_clarray_t zz, py_cdarray_t rr, py_clarray_t elems,
    double xmin, double deltax, long xdim,
    std::string geomf, py::buffer geomfp, std::string weightf, py::buffer weightfp, std::string distrf, py::buffer distrfp,
    std::string corrf, py::buffer corffp, std::string eindexf, py::buffer eindexfp, std::string aindexf, py::buffer aindexfp,
    double acc, bool flatten
);

// Python bindings

void init_representations_mbtr(py::module &m);

#endif // include guard
