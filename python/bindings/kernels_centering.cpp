// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Python bindings for centering of kernel matrices

#include "kernels_centering.hpp"
#include "qmmlpack/kernels_centering.hpp"

// center_kernel_matrix

void _center_kernel_matrix_k(py_cdarray_t kc, py_cdarray_t kk)
{
    auto bufkc = kc.request(), bufk = kk.request();

    if(bufkc.ndim != 2 || bufk.ndim != 2) throw QMML_ERROR("_center_kernel_matrix_k arguments must be matrices");

    const size_t n = bufk.shape[0];

    if(bufk.shape[1] != n) throw QMML_ERROR("_center_kernel_matrix_k: K must be quadratic");
    if(bufkc.shape[0] != n || bufkc.shape[1] != n) throw QMML_ERROR("_center_kernel_matrix_k: result matrix must have compatible dimensions");

    qmml::center_kernel_matrix_k(static_cast<double*>(bufkc.ptr), static_cast<double*>(bufk.ptr), n);
}

void _center_kernel_matrix_l(py_cdarray_t lc, py_cdarray_t kk, py_cdarray_t ll)
{
    auto buflc = lc.request(), bufkk = kk.request(), bufll = ll.request();

    if(buflc.ndim != 2 || bufkk.ndim != 2 || bufll.ndim != 2) throw QMML_ERROR("_center_kernel_matrix_l arguments must be matrices");

    const size_t n = bufkk.shape[0];
    const size_t m = bufll.shape[1];

    if(bufkk.shape[1] != n) throw QMML_ERROR("_center_kernel_matrix_l: K must be quadratic");
    if(bufll.shape[0] != n) throw QMML_ERROR("_center_kernel_matrix_l: L must have compatible dimensions");
    if(buflc.shape[0] != n || buflc.shape[1] != m) throw QMML_ERROR("_center_kernel_matrix_l: result matrix must have compatible dimensions");

    qmml::center_kernel_matrix_l(static_cast<double*>(buflc.ptr), static_cast<double*>(bufkk.ptr), static_cast<double*>(bufll.ptr), n, m);
}

void _center_kernel_matrix_m(py_cdarray_t mc, py_cdarray_t kk, py_cdarray_t ll, py_cdarray_t mm)
{
    auto bufmc = mc.request(), bufkk = kk.request(), bufll = ll.request(), bufmm = mm.request();

    if(bufmc.ndim != 2 || bufkk.ndim != 2 || bufll.ndim != 2 || bufmm.ndim != 2) throw QMML_ERROR("_center_kernel_matrix_m arguments must be matrices");

    const size_t n = bufkk.shape[0];
    const size_t m = bufll.shape[1];

    if(bufkk.shape[1] != n) throw QMML_ERROR("_center_kernel_matrix_m: K must be quadratic");
    if(bufll.shape[0] != n) throw QMML_ERROR("_center_kernel_matrix_m: L must have compatible dimensions");
    if(bufmm.shape[0] != m || bufmm.shape[1] != m) throw QMML_ERROR("_center_kernel_matrix_m: M must have compatible dimensions");
    if(bufmc.shape[0] != m || bufmc.shape[1] != m) throw QMML_ERROR("_center_kernel_matrix_m: result matrix must have compatible dimensions");

    qmml::center_kernel_matrix_m(static_cast<double*>(bufmc.ptr), static_cast<double*>(bufkk.ptr), static_cast<double*>(bufll.ptr), static_cast<double*>(bufmm.ptr), n, m);
}

void _center_kernel_matrix_mdiag(py_cdarray_t mc, py_cdarray_t kk, py_cdarray_t ll, py_cdarray_t mm)
{
    auto bufmc = mc.request(), bufkk = kk.request(), bufll = ll.request(), bufmm = mm.request();

    if(bufmc.ndim != 1 || bufmm.ndim != 1) throw QMML_ERROR("_center_kernel_matrix_mdiag first and last arguments must be vectors");
    if(bufkk.ndim != 2 || bufll.ndim != 2) throw QMML_ERROR("_center_kernel_matrix_mdiag arguments K and L must be matrices");

    const size_t n = bufkk.shape[0];
    const size_t m = bufll.shape[1];

    if(bufkk.shape[1] != n) throw QMML_ERROR("_center_kernel_matrix_mdiag: K must be quadratic");
    if(bufll.shape[0] != n) throw QMML_ERROR("_center_kernel_matrix_mdiag: L must have compatible dimensions");
    if(bufmm.shape[0] != m) throw QMML_ERROR("_center_kernel_matrix_mdiag: m must have compatible dimensions");
    if(bufmc.shape[0] != m) throw QMML_ERROR("_center_kernel_matrix_mdiag: result vector must have compatible dimensions");

    qmml::center_kernel_matrix_mdiag(static_cast<double*>(bufmc.ptr), static_cast<double*>(bufkk.ptr), static_cast<double*>(bufll.ptr), static_cast<double*>(bufmm.ptr), n, m);
}

// Python bindings

void init_kernels_centering(py::module &m)
{
    m.def("_center_kernel_matrix_k", &_center_kernel_matrix_k, "Centers kernel matrix K in feature space", py::arg("Kc"), py::arg("K"));
    m.def("_center_kernel_matrix_l", &_center_kernel_matrix_l, "Centers kernel matrix L with respect to K", py::arg("Lc"), py::arg("K"), py::arg("L"));
    m.def("_center_kernel_matrix_m", &_center_kernel_matrix_m, "Centers kernel matrix M with respect to K", py::arg("Mc"), py::arg("K"), py::arg("L"), py::arg("M"));
    m.def("_center_kernel_matrix_mdiag", &_center_kernel_matrix_mdiag, "Centers diagonal of M with respect to K", py::arg("mc"), py::arg("K"), py::arg("L"), py::arg("m"));
}
