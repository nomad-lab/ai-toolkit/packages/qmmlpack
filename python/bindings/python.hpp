// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Python bindings

#ifndef QMML_PYTHON_HPP_INCLUDED  // include guard
#define QMML_PYTHON_HPP_INCLUDED

// qmmlpack

#include "qmmlpack/base.hpp"

// pybind11

#include "pybind11/pybind11.h"
#include "pybind11/numpy.h"
namespace py = pybind11;

typedef typename py::array_t<double, py::array::c_style | py::array::forcecast> py_cdarray_t;
typedef typename py::array_t<long  , py::array::c_style | py::array::forcecast> py_clarray_t;

#endif // include guard
