// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Python bindings for Laplacian kernel

#ifndef QMML_PYTHON_KERNELS_LAPLACIAN_HPP_INCLUDED  // include guard
#define QMML_PYTHON_KERNELS_LAPLACIAN_HPP_INCLUDED

#include "python.hpp"

// kernels_laplacian

void _kernel_matrix_laplacian_d(py_cdarray_t ll, py_cdarray_t dd, double sigma);
void _kernel_matrix_laplacian_k(py_cdarray_t kk, py_cdarray_t xx, double sigma);
void _kernel_matrix_laplacian_l(py_cdarray_t ll, py_cdarray_t xx, py_cdarray_t zz, double sigma);
void _kernel_matrix_laplacian_m(py_cdarray_t m, py_cdarray_t xx, double sigma);

// Python bindings

void init_kernels_laplacian(py::module &m);

#endif // include guard
