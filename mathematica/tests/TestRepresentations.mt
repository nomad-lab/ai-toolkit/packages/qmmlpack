(*  qmmlpack                     *)
(*  Matthias Rupp, 2006-2016.    *)
(*  See LICENSE.txt for license. *)

(*  Tests for representations.  *)

Needs["QMMLPack`"];
Needs["ApproximatelyEqual`", "ApproximatelyEqual.m"];
Needs["Developer`"];
On[Assert];

(*  ***********************  *)
(*  *  finiteTestSystems  *  *)
(*  ***********************  *)

finiteTestSystems::error = "Error creating finite test systems: ``";

finiteTestSystems[names_?(VectorQ[#, StringQ] &)] := Module[
    {z, r, i},

    z = {}; r = {};
    For[i = 1, i <= Length[names], ++i, (
        If[names[[i]] == "empty", (
            AppendTo[z, {}];
            AppendTo[r, {}];
            Continue[]
        )];

        If[names[[i]] == "H", (
            AppendTo[z, {1}];
            AppendTo[r, {{0.,0.,0.}}];
            Continue[];
        )];

        If[names[[i]] == "H3", (
            AppendTo[z, {1, 1, 1}];
            AppendTo[r, {{0.,0.,0.}, {0.72,0.,0.}, {1.44,0.,0.}}];
            Continue[];
        )];

        If[names[[i]] == "O2", (
            AppendTo[z, {8, 8}];
            AppendTo[r, {{0, 0, 0}, {1.48, 0, 0}}];
            Continue[];
        )];

        If[names[[i]] == "HCl", (
            AppendTo[z, {1, 17}];
            AppendTo[r, {{0, 0, 0}, {0, 1.27, 0}}];
            Continue[];
        )];

        Message[finiteTestSystems::error, "Unknown finite system name '" <> names[[i]] <> "'"]; Throw[$Failure];
    )];

    {z, N[r]}
];

(*  *************************  *)
(*  *  periodicTestSystems  *  *)
(*  *************************  *)

periodicTestSystems::error = "Error creating periodic test systems: ``";

periodicTestSystems[names_?(VectorQ[#, StringQ] &)] := Module[
    {z, r, b, i},

    z = {}; r = {}; b= {};
    For[i = 1, i <= Length[names], ++i, (
        If[names[[i]] == "empty", (
            AppendTo[z, {}];
            AppendTo[r, {}];
            AppendTo[b, {{1,0,0}, {0,1,0}, {0,0,1}}];
            Continue[]
        )];

        If[names[[i]] == "NaCl", (
            AppendTo[z, {11, 17}];
            AppendTo[r, {{0,0,0}, {3.988, 2.302475, 1.628095}}];
            AppendTo[b, {{3.988, 0, 0},  {1.994, 3.45371, 0},  {1.994, 1.15124, 3.25619}}];
            Continue[];
        )];

        If[names[[i]] == "Al", (
            AppendTo[z, {13}];
            AppendTo[r, {{0,0,0}}];
            AppendTo[b, {{0, 2.02475, 2.02475}, {2.02475, 2.02475, 0},  {2.02475, 0, 2.02475}}];
            Continue[];
        )];

        If[names[[i]] == "SF6", (
            AppendTo[z, {16, 9, 9, 9, 9, 9, 9}];
            AppendTo[r, {
                { 0.0000,  0.0000, 0.0000},
                {-0.8070,  4.2386, 1.1293},
                { 2.5361, -1.7933, 3.1061},
                {-2.5361,  1.7933, 3.1061},
                {-0.9220,  0.6510, 1.1293},
                { 0.9220,  1.3040, 0.    },
                { 2.5361,  3.5866, 0.    }
            }];
            AppendTo[b, {{5.187, 0, 0},  {-1.729, 4.891, 0},  {-1.729, -2.445, 4.235}}];
            Continue[];
        )];

        Message[periodicTestSystems::error, "Unknown periodic system name '" <> names[[i]] <> "'"]; Throw[$Failure];
    )];

    {z, N[r], N[b]}
];

(*  *******************  *)
(*  *  CoulombMatrix  *  *)
(*  *******************  *)

(** Molecule Coulomb matrix **)

(* Special cases and defaults *)
Test[
    {
	    CoulombMatrix[{1}, {{0,0,0}}],  (* free atom *)
	    CoulombMatrix[{1,17}, {{0,0,0},{0,1.27,0}}]  (* HCl diatomic *)
    }
	,
	{
	    {{0.5}},
        {{448.79438598087216,7.0834744715621785},{7.0834744715621785,0.5}}
	}
	,
	TestID->"Representations-20160817-Z3E9X2"
	,
    EquivalenceFunction -> ApproximatelyEqualListableAll
]

(* Unit *)
Test[
	{
	    CoulombMatrix[{1,1}, {{0,0,-0.74},{0,0,0}}], (* Default is Angstrom *)
	    CoulombMatrix[{1,1}, {{0,0,-0.74},{0,0,0}}, Unit->"\[ARing]ngstr\[ODoubleDot]m"],
        CoulombMatrix[{1,1}, {{0,0,-0.74},{0,0,0}}, Unit->"Angstroms"], (* alternative spelling *)
        CoulombMatrix[{1,1}, {{0,0,-74},{0,0,0}}, Unit->"pm"],
        CoulombMatrix[{1,1}, {{0,0,-74},{0,0,0}}, Unit->"Picometers"],  (* alternative spelling *)
	    CoulombMatrix[{1,1}, {{0,0,0},{0,0,1.39839733}}, Unit->"bohr"],
	    CoulombMatrix[{1,1}, {{0,0,0},{0,0,1.39839733}}, Unit->"a.u."]  (* alternative spelling *)
	}
	,
	Evaluate[ConstantArray[CoulombMatrix[{1,1}, {{0,0,-0.74},{0,0,0}}], 7]]
	,
	TestID->"Representations-20160817-e5P2O8"
	,
    EquivalenceFunction -> ApproximatelyEqualListableAll
]

(* Padding *)
Test[
	{
        CoulombMatrix[{1,1}, {{0,0,0},{0,0,0.74}}],  (* Default is no padding *)
        CoulombMatrix[{1,1}, {{0,0,0},{0,0,0.74}}, Padding->1],  (* negative padding *)
	    CoulombMatrix[{1,1}, {{0,0,0},{0,0,0.74}}, Padding->2],  (* no effect *)
	    CoulombMatrix[{1,1}, {{0,0,0},{0,0,0.74}}, Padding->3],  (* one additional dimension *)
	    CoulombMatrix[{1,1}, {{0,0,0},{0,0,0.74}}, Padding->4]   (* two additional dimensions *)
	}
	,
	{
        {{0.5,0.715104339083375}, {0.715104339083375,0.5}},
        {{0.5}},
	    {{0.5,0.715104339083375}, {0.715104339083375,0.5}},
	    {{0.5,0.715104339083375,0}, {0.715104339083375,0.5,0}, {0,0,0}},
	    {{0.5,0.715104339083375,0,0}, {0.715104339083375,0.5,0,0}, {0,0,0,0}, {0,0,0,0}}
	}
	,
	TestID->"Representations-20160817-q5m1L2"
]

(* Flattening *)
Test[
	{
        CoulombMatrix[{1,1}, {{0,0,0},{0.74,0,0}}], (* Default is not to flatten *)
	    CoulombMatrix[{1,1}, {{0,0,0},{0.74,0,0}}, Flatten->True],
        CoulombMatrix[{1,1}, {{0,0,0},{0.74,0,0}}, Flatten-> 0], (* same result as True *)
        CoulombMatrix[{1,1}, {{0,0,0},{0.74,0,0}}, Flatten-> 1],
        CoulombMatrix[{1,1}, {{0,0,0},{0.74,0,0}}, Flatten-> 2],
        CoulombMatrix[{1,1}, {{0,0,0},{0.74,0,0}}, Flatten->-1],
        CoulombMatrix[{1,1}, {{0,0,0},{0.74,0,0}}, Flatten->-2]
	}
	,
	{
        {{0.5,0.715104339083375}, {0.715104339083375,0.5}},
	    {0.5,0.715104339083375,0.5},
        {0.5,0.715104339083375,0.5},
        {0.5,0.715104339083375,0.715104339083375,0.5},
        {0.5,0.715104339083375,0.715104339083375,0.5},
        {0.715104339083375},
        {}
	}
	,
	TestID->"Representations-20160817-j5Q5O8"
]

(* Sort *)
Test[
    {
        CoulombMatrix[{1,17}, {{1,1,1},{1,1,2.27}}], (* Default is sorting by row norm *)
        CoulombMatrix[{1,17}, {{1,1,1},{1,1,2.27}}, Sort->True],
        CoulombMatrix[{1,17}, {{1,1,1},{1,1,2.27}}, Sort->False],
        CoulombMatrix[{1,17}, {{1,1,1},{1,1,2.27}}, Sort->({1,2} &)],
        CoulombMatrix[{1,17}, {{1,1,1},{1,1,2.27}}, Sort->({2,1} &)]
    }
    ,
    {
        {{448.79438598087216,7.0834744715621785},{7.0834744715621785,0.5}},
        {{448.79438598087216,7.0834744715621785},{7.0834744715621785,0.5}},
        {{0.5,7.0834744715621785},{7.0834744715621785,448.79438598087216}},
        {{0.5,7.0834744715621785},{7.0834744715621785,448.79438598087216}},
        {{448.79438598087216,7.0834744715621785},{7.0834744715621785,0.5}}
    }
    ,
    TestID->"Representations-20160817-A493kL"
    ,
    SameTest->ApproximatelyEqualListableAll
]

(* Post *)
Test[
    {
        CoulombMatrix[{1,17}, {{1,1,1},{1,1,2.27}}], (* default is identity *)
        CoulombMatrix[{1,17}, {{1,1,1},{1,1,2.27}}, Post->Function[{cmij, zi, zj, dij, i, j}, cmij]],  (* identity *)
        CoulombMatrix[{1,17}, {{1,1,1},{1,1,2.27}}, Post->Function[{cmij, zi, zj, dij, i, j}, cmij+1]],  (* argument 1 *)
        CoulombMatrix[{1,17}, {{1,1,1},{1,1,2.27}}, Post->Function[{cmij, zi, zj, dij, i, j}, zi]],  (* argument 2 *)
        CoulombMatrix[{1,17}, {{1,1,1},{1,1,2.27}}, Post->Function[{cmij, zi, zj, dij, i, j}, zj]],  (* argument 3 *)
        CoulombMatrix[{1,17}, {{1,1,1},{1,1,2.27}}, Post->Function[{cmij, zi, zj, dij, i, j}, dij]],  (* argument 4 *)
        CoulombMatrix[{1,17}, {{1,1,1},{1,1,2.27}}, Post->Function[{cmij, zi, zj, dij, i, j}, i]],  (* argument 5 *)
        CoulombMatrix[{1,17}, {{1,1,1},{1,1,2.27}}, Post->Function[{cmij, zi, zj, dij, i, j}, j]]  (* argument 6 *)
    }
    ,
    {
        {{448.79438598087216,7.0834744715621785},{7.0834744715621785,0.5}},
        {{448.79438598087216,7.0834744715621785},{7.0834744715621785,0.5}},
        {{448.79438598087216,7.0834744715621785},{7.0834744715621785,0.5}}+1,
        {{17,17},{1,1}},
        {{17,1},{17,1}},
        {{0,2.3999521781899302},{2.3999521781899302,0}},
        {{1,1},{2,2}},
        {{1,2},{1,2}}
    }
    ,
    TestID->"Representations-20160818-kDeo12"
    ,
    SameTest->ApproximatelyEqualListableAll
]

(* Post-processing without sorting *)
Test[
    CoulombMatrix[{1,17}, {{1,1,1},{1,1,2.27}}, Padding->3, Flatten->False, Sort->False, Post->(#1+#2+#3 &)]
    ,
    {{0.5+2,7.0834744715621785+18,0.},{7.0834744715621785+18,448.79438598087216+34,0.},{0.,0.,0.}}
    ,
    TestID->"Representation-20160821-4Io2wE"
    ,
    SameTest->(ApproximatelyEqualListableAll[#1, #2, Accuracy->8] &)
]

(* Example: azanone (nitroxyl) *)
Test[
    Module[{z,r},
        z = { 8, 7, 1 };
        r = { { -0.6175, 0, 0 }, { 0.6175, 0, 0 }, { 0.9797, 0.6825, 0.6518 } };
        {
            CoulombMatrix[z, r],
            CoulombMatrix[z, r, Padding->2, Flatten->True, Post->(2 #1 &)],
            CoulombMatrix[z, r, Padding->4],
            CoulombMatrix[z, r, Padding->4, Sort->({3,2,1} &)],
            CoulombMatrix[z, r, Padding->4, Flatten->-2, Sort->({3,2,1} &)]
        }
    ]
    ,
    {
        { {73.5167, 23.9951, 2.28194}, {23.9951, 53.3587, 3.66444}, {2.28194, 3.66444, 0.5} },
        { 73.5167, 23.9951, 53.3587 } * 2,
        { {73.5167, 23.9951, 2.28194, 0.}, {23.9951, 53.3587, 3.66444, 0.}, {2.28194, 3.66444, 0.5, 0.}, { 0., 0., 0., 0. } },
        { { 0.5, 3.66444, 2.28194, 0.}, { 3.66444, 53.3587, 23.9951, 0.}, { 2.28194, 23.9951, 73.5167, 0. }, { 0., 0., 0., 0. } },
        { 2.28194, 0., 0. }
    }
    ,
    TestID->"Representations-20160818-kEo38D"
    ,
    SameTest->(ApproximatelyEqualListableAll[#1, #2, Accuracy->4] &)
]

(* CoulombMatrixFunction *)
Test[
    CoulombMatrix[Unit->"pm", Padding->3, Flatten->False, Sort->False, Post->(#1+1 &)][{1,9}, {{0,0,0},{0,92,0}}]
    ,
    Evaluate[ CoulombMatrix[{1,9}, {{0,0,0},{0,92,0}}, Unit->"pm", Padding->3, Flatten->False, Sort->False, Post->(#1+1 &)] ]
    ,
    TestID->"Representation-20160821-Ej93K1"
]

(* Invalid option values *)
Test[
    CoulombMatrix[{1}, {{0,0,0}}, Unit->"od8eL"]
    ,
    $Failed
    ,
    {CoulombMatrix::error}
    ,
    TestID->"Representation-20160821-lLed93"
]

Test[
    CoulombMatrix[{1}, {{0,0,0}}, Padding->"abc"]
    ,
    $Failed
    ,
    {CoulombMatrix::error}
    ,
    TestID->"Representation-20160821-lLe0pP"
]


(*  ********************  *)
(*  *  ManyBodyTensor  *  *)
(*  ********************  *)

(** MBTR, finite, dense **)

(* k = 1 *)
Test[
    Module[{h1z,h1r,h3z,h3r,hcz,hcr},
        {h1z,h1r} = finiteTestSystems[{"H"}];
        {h3z,h3r} = finiteTestSystems[{"H3"}];
        {hcz,hcr} = finiteTestSystems[{"H", "H3"}];
        {hoz,hor} = finiteTestSystems[{"H3", "O2"}];
        {
            ManyBodyTensor[h1z, h1r, { 0.5, 1.0, 1}, {1, "unity", "unity", {"normal", {10.^-10}}, "identity", "full", "full"}],
            ManyBodyTensor[h1z, h1r, { 0.5, 1.0, 1}, {1, "unity", "unity", {"normal", {10.^-10}}, "identity", "full", "full"}, Flatten->True],
            ManyBodyTensor[h3z, h3r, { 0.5, 1.0, 3}, {1, "count", "unity", {"normal", {10.^-9}}, "identity", "full", "full"}, Flatten->True ],
            ManyBodyTensor[h3z, h3r, { 0.5, 1.5, 1}, {1, "unity", "unity"   , {"normal", {10.^-7}}, "identity", "full", "full"}, Flatten->True ],
            ManyBodyTensor[h3z, h3r, { 0.5, 1.5, 1}, {1, "unity", "1/count" , {"normal", {10.^-7}}, "identity", "full", "full"}, Flatten->False],
            ManyBodyTensor[h3z, h3r, { 0.5, 1.0, 3}, {1, "count", "identity", {"normal", {10.^-7}}, "identity", "full", "full"}, Flatten->True ],
            ManyBodyTensor[h3z, h3r, { 0.5, 1.0, 3}, {1, "count", "identity^2", {"normal", {10.^-7}}, "identity", "full", "full"}, Flatten->True ],
            ManyBodyTensor[h3z, h3r, { 0.5, 1.0, 3}, {1, "count", "identity_root", {"normal", {10.^-7}}, "identity", "full", "full"}, Flatten->True ],
            ManyBodyTensor[h3z, h3r, { 0.5, 1.0, 3}, {1, "count", "1/identity", {"normal", {10.^-7}}, "identity", "full", "full"}, Flatten->False ],
            ManyBodyTensor[hcz, hcr, { 0.5, 1.0, 3}, {1, "count", "1/identity", {"normal", {10.^-8}}, "identity", "full", "full"}, Flatten->False],
            ManyBodyTensor[hoz, hor, {-0.5, 1.0, 3}, {1, "unity", "unity", {"normal", {10.^-7}}, "identity", "full", "full"}, Flatten->False]
        }
    ]
    ,
    N[{
        {{{1}}},
        {{1}},
        {{0,0,3}},
        {{3}},
        {{{1}}},
        {{0,0,9}},
        {{0,0,27}},
        {{0,0,3*Sqrt[3.]}},
        {{{0,0,1}}},
        {{{1,0,0}}, {{0,0,1}}},
        {{{0,3,0}, {0,0,0}}, {{0,0,0}, {0,2,0}}}
    }]
    ,
    TestID->"Representations-20170719-Ooe34D"
];

Test[
    Module[{z,r},
        {z,r} = finiteTestSystems[{"H"}];
        Round[ManyBodyTensor[z, r, {-4, 1, 10}, {1, "count", "1/identity", {"normal", {0.5}}, "identity", "full", "full"}, Flatten->True], 10^-5]
    ]
    ,
    Round[{{6.20081*10^-16, 9.86587*10^-10, 0.0000316703, 0.0227185, 0.47725, 0.47725, 0.0227185, 0.0000316703, 9.86587*10^-10, 6.20081*10^-16}}, 10^-5]
    ,
    TestID->"Representations-20170720-Ert453"
];

(* Option: Flatten *)
Test[
    Module[{h1z,h1r,hclz,hclr,ho2z,ho2r},
        {h1z,h1r} = finiteTestSystems[{"H"}];
        {hclz,hclr} = finiteTestSystems[{"HCl"}];
        {ho2z,ho2r} = finiteTestSystems[{"H", "O2"}];
        {
            ManyBodyTensor[h1z , h1r , {0.5, 1, 1}, {1, "unity", "unity", {"normal", {10^-7}}, "identity", "full", "full"}, Flatten->False],
            ManyBodyTensor[h1z , h1r , {0.5, 1, 1}, {1, "unity", "unity", {"normal", {10^-7}}, "identity", "full", "full"}, Flatten->True ],
            ManyBodyTensor[hclz, hclr, {0.5, 1, 1}, {1, "unity", "unity", {"normal", {10^-7}}, "identity", "full", "full"}, Flatten->False],
            ManyBodyTensor[hclz, hclr, {0.5, 1, 1}, {1, "unity", "unity", {"normal", {10^-7}}, "identity", "full", "full"}, Flatten->True ],
            ManyBodyTensor[ho2z, ho2r, {0.5, 1, 1}, {1, "unity", "unity", {"normal", {10^-7}}, "identity", "full", "full"}, Flatten->False],
            ManyBodyTensor[ho2z, ho2r, {0.5, 1, 1}, {1, "unity", "unity", {"normal", {10^-7}}, "identity", "full", "full"}, Flatten->True ]
        }
    ]
    ,
    N[{
        {{{1}}},
        {{1}},
        {{{1},{1}}},
        {{1,1}},
        {{{1},{0}}, {{0},{2}}},
        {{1,0}, {0,2}}
    }]
    ,
    TestID->"Representations-20170108-EKd98I"
]

(* Option: "Elements" *)
Test[
    Module[{ho2z,ho2r},
        {ho2z,ho2r} = finiteTestSystems[{"H", "O2"}];
        {
            ManyBodyTensor[ho2z, ho2r, {0.5, 1, 1}, {1, "unity", "unity", {"normal", {10^-7}}, "identity", "full", "full"}, "Elements" -> {"H", "O"}],
            ManyBodyTensor[ho2z, ho2r, {0.5, 1, 1}, {1, "unity", "unity", {"normal", {10^-7}}, "identity", "full", "full"}, "Elements" -> {"H", "He", "O"}]  (* non-present element *)
        }
    ]
    ,
    N[{
        {{{1},{0}}, {{0},{2}}},
        {{{1},{0},{0}}, {{0},{0},{2}}}
    }]
    ,
    TestID->"Representations-20170108-EKopPl"
]

Test[
    Module[{ho2z,ho2r},
        {ho2z,ho2r} = finiteTestSystems[{"H", "O2"}];
        ManyBodyTensor[ho2z, ho2r, {0.5, 1, 1}, {1, "unity", "unity", {"normal", {10^-7}}, "identity", "full", "full"}, "Elements" -> {"H"}]  (* missing element *)
    ]
    ,
    $Failed
    ,
    {ManyBodyTensor::error}
    ,
    TestID->"Representation-20170109-lRed93"
]

(* Discretization *)
Test[
    Module[{hz,hr,ho2z,ho2r},
        {hz,hr} = finiteTestSystems[{"H"}];
        {
            ManyBodyTensor[hz, hr, {-0.5, 1, 3}, {1, "unity", "unity", {"normal", {10^-7}}, "identity", "full", "full"}],
            ManyBodyTensor[hz, hr, {-1.0, 1, 4}, {1, "unity", "unity", {"normal", {10^-7}}, "identity", "full", "full"}]  (* split evenly *)
        }
    ]
    ,
    N[{
        {{{0,1,0}}},
        {{{0,0.5,0.5,0}}}
    }]
    ,
    TestID->"Representation-20170109-ike893"
]

(* Distribution function: normal *)
Test[
    Module[{x,range1,exact1,range2,exact2,rang3,exact3,mbtr1,mbtr2,mbtr3,z,r},
        (*range1 = Range[0.8, 1.2, 0.4/20 ]; exact1 = Table[Integrate[PDF[NormalDistribution[1, 2.^-7], x], {x, range1[[i]], range1[[i + 1]]}], {i, 20 }];
        range2 = Range[0.8, 1.2, 0.4/200]; exact2 = Table[Integrate[PDF[NormalDistribution[1, 2.^-7], x], {x, range2[[i]], range2[[i + 1]]}], {i, 200}];
        range3 = Range[0.8, 1.2, 0.4/200]; exact3 = Table[Integrate[PDF[NormalDistribution[1, 0.1  ], x], {x, range3[[i]], range3[[i + 1]]}], {i, 200}];*)
        range1 = Range[0.8, 1.2, 0.4/20 ]; exact1 = Table[CDF[NormalDistribution[1, 2.^-7], range1[[i+1]]] - CDF[NormalDistribution[1, 2.^-7], range1[[i]]], {i, 20 }];
        range2 = Range[0.8, 1.2, 0.4/200]; exact2 = Table[CDF[NormalDistribution[1, 2.^-7], range2[[i+1]]] - CDF[NormalDistribution[1, 2.^-7], range2[[i]]], {i, 200}];
        range3 = Range[0.8, 1.2, 0.4/200]; exact3 = Table[CDF[NormalDistribution[1, 0.1  ], range3[[i+1]]] - CDF[NormalDistribution[1, 0.1  ], range3[[i]]], {i, 200}];
        z = {{1, 1}}; r = N[{{{0,0,0}, {1,0,0}}}];
        mbtr1 = First[ManyBodyTensor[z, r, {0.8, 0.4/20 ,  20}, {2, "1/distance", "identity^2", {"normal", {2.^-7}}, "identity", "full", "noreversals"}, Flatten->True]];
        mbtr2 = First[ManyBodyTensor[z, r, {0.8, 0.4/200, 200}, {2, "1/distance", "identity^2", {"normal", {2.^-7}}, "identity", "full", "noreversals"}, Flatten->True]];
        mbtr3 = First[ManyBodyTensor[z, r, {0.8, 0.4/200, 200}, {2, "1/distance", "identity^2", {"normal", {0.1  }}, "identity", "full", "noreversals"}, Flatten->True]];
        {
            Chop[Total[Abs[exact1 - mbtr1]], 10^-7],
            Chop[Total[Abs[exact2 - mbtr2]], 10^-7],
            Chop[Total[Abs[exact3 - mbtr3]], 10^-7]
        }
    ]
    ,
    {
        0, 0, 0
    }
    ,
    TestID->"Representation-20170110-395ndk"
]

(* Example: water *)
Test[
    Module[{range2,exact2,exact2b,dist1,dist2,range3,exact3,angle3one,angle3two,mbtr2,mbtr2b,mbtr3},
        (* water molecule. OH 0.9584 angstrom, HOH 104.45 Degree *)
        z = {8, 1, 1};
        r = N[{{0, 0, 0}, {0.9584, 0, 0}, {-0.2391543829355198, 0.9280817534693432, 0}}];

        (* k = 2 *)
        range2 = Range[0., 1/0.7, (1/0.7)/200]; exact2 = ConstantArray[0., {3, 200}];
        dist1 = 1/Norm[r[[2]]-r[[3]]]; dist2 = 1/Norm[r[[2]]-r[[1]]];
        (*exact2[[1]] = Table[1*Integrate[PDF[NormalDistribution[1/Norm[r[[2]]-r[[3]]], 1/8], x], {x, range2[[i]], range2[[i + 1]]}], {i, 200}];
        exact2[[2]] = Table[2*Integrate[PDF[NormalDistribution[1/Norm[r[[2]]-r[[1]]], 1/8], x], {x, range2[[i]], range2[[i + 1]]}], {i, 200}];*)
        exact2[[1]] = Table[1*(CDF[NormalDistribution[dist1, 1/8], range2[[i+1]]] - CDF[NormalDistribution[dist1, 1/8], range2[[i]]]), {i, 200}];
        exact2[[2]] = Table[2*(CDF[NormalDistribution[dist2, 1/8], range2[[i+1]]] - CDF[NormalDistribution[dist2, 1/8], range2[[i]]]), {i, 200}];

        exact2b = exact2;
        exact2b[[1]] = exact2[[1]] * dist1;
        exact2b[[2]] = exact2[[2]] * dist2;

        mbtr2  = First[ManyBodyTensor[{z}, {r}, {0, (1/0.7)/200, 200}, {2, "1/distance", "unity"   , {"normal", {1/8}}, "identity", "noreversals", "noreversals"}, Flatten->False]];
        mbtr2b = First[ManyBodyTensor[{z}, {r}, {0, (1/0.7)/200, 200}, {2, "1/distance", "identity", {"normal", {1/8}}, "identity", "noreversals", "noreversals"}, Flatten->False]];

        (* k = 3 *)
        range3 = Range[0., Pi, Pi/200]; exact3 = ConstantArray[0., {6, 200}];
        angle3one = ArcCos[Dot[r[[2]] - r[[1]], r[[3]] - r[[1]]]/(Norm[r[[2]] - r[[1]]] Norm[r[[3]] - r[[1]]])];
        angle3two = ArcCos[Dot[r[[1]] - r[[2]], r[[3]] - r[[2]]]/(Norm[r[[1]] - r[[2]]] Norm[r[[3]] - r[[2]]])];
        (*exact3[[2]] = Table[2*Integrate[PDF[NormalDistribution[angle3two, 1/8], x], {x, range3[[i]], range3[[i+1]]}], {i, 200}];
        exact3[[3]] = Table[1*Integrate[PDF[NormalDistribution[angle3one, 1/8], x], {x, range3[[i]], range3[[i+1]]}], {i, 200}];*)
        exact3[[2]] = Table[2*(CDF[NormalDistribution[angle3two, 1/8], range3[[i + 1]]] - CDF[NormalDistribution[angle3two, 1/8], range3[[i]]]), {i, 200}];
        exact3[[3]] = Table[1*(CDF[NormalDistribution[angle3one, 1/8], range3[[i + 1]]] - CDF[NormalDistribution[angle3one, 1/8], range3[[i]]]), {i, 200}];

        mbtr3 = First[ManyBodyTensor[{z}, {r}, {0, Pi/200., 200}, {3, "angle", "unity", {"normal", {1/8}}, "identity", "noreversals", "noreversals"}, Flatten->False]];

        {
            Chop[Total[Abs[Flatten[exact2  - mbtr2 ]]], 10^-7],
            Chop[Total[Abs[Flatten[exact2b - mbtr2b]]], 10^-7],
            Chop[Total[Abs[Flatten[exact3  - mbtr3 ]]], 10^-7]
        }
    ]
    ,
    {
        0, 0, 0
    }
    ,
    TestID->"Representation-20170110-34kd1p"
]

(* MBTR, periodic, dense *)

(* special cases and error handling *)

(* 1-body *)

Test[
    Module[{z,r,b},
        {z,r,b} = periodicTestSystems[{"NaCl"}];
        ManyBodyTensor[z, r, b, {-0.5, 1, 3}, {1, "unity", "unity", {"normal", {10.^-10}}, "identity", "noreversals", "full"}]
    ]
    ,
    N[{{{0, 1, 0}, {0, 1, 0}}}]
    ,
    TestID->"Representation-20170721-937ehH"
];

(* 2-body *)

Test[
    Module[{z,r,b},
        {z,r,b} = periodicTestSystems[{"Al"}];
        ManyBodyTensor[z, r, b, {-10, 20, 1}, {2, "1/distance", {"delta_1/identity", {2.8}}, {"normal", {10.^-10}}, "identity", "noreversals", "full"}]
    ]
    ,
    N[{{{0}}}]
    ,
    TestID->"Representation-20170721-oePl43"
];

Test[
    Module[{z,r,b},
        {z,r,b} = periodicTestSystems[{"Al"}];
        ManyBodyTensor[z, r, b, {0, 0.2, 10}, {2, "1/distance", {"delta_1/identity", {2.87}}, {"normal", {10.^-10}}, "identity", "noreversals", "full"}]
    ]
    ,
    N[{{{0, 12, 0, 0, 0, 0, 0, 0, 0, 0}}}]
    ,
    TestID->"Representation-20170721-1lkLol"
];

Test[
    Module[{z,r,b},
        {z,r,b} = periodicTestSystems[{"Al"}];
        ManyBodyTensor[z, r, b, {0, 0.025, 20}, {2, "1/distance", {"delta_1/identity", {5.72687}}, {"normal", {10.^-10}}, "identity", "full", "full"}, Flatten->True]
    ]
    ,
    N[{{0, 0, 0, 0, 0, 0, 12, 0, 24, 6, 0, 0, 0, 12, 0, 0, 0, 0, 0, 0}}]
    ,
    TestID->"Representation-20170721-iuER31"
];
