(* ::Package:: *)

(* Mathematica Package *)

(* The original ApproximatelyEqual[] function was written by Andrew Moylan (andrew.j.moylan@gmail.com, http://andrew.j.moylan.googlepages.com). *)

BeginPackage["ApproximatelyEqual`"]

If[Not[ValueQ[ApproximatelyEqual::usage]], ApproximatelyEqual::usage =
"ApproximatelyEqual[a,b] returns True if a and b are approximately equal.
ApproximatelyEqual[a,b,Precision->pr,Accuracy->ac,NormFunction->f] measures a and b using the norm f, testing precision pr or accuracy ac"];

If[Not[ValueQ[ApproximatelyEqualListable::usage]], ApproximatelyEqualListable::usage =
"ApproximatelyEqualListable[a,b] returns True if a and b are approximately equal.
ApproximatelyEqualListable[a,b,Precision->pr,Accuracy->ac,NormFunction->f] measures a and b using the norm f, testing precision pr or accuracy ac.
ApproximatelyEqualListable threads over input argument lists and has other default options than ApproximatelyEqual."];

If[Not[ValueQ[ApproximatelyEqualListableAll::usage]], ApproximatelyEqualListableAll::usage =
"ApproximatelyEqualListableAll[a,b] returns true if all evaluations of ApproximatelyEqualListable return true,
i.e., it is equivalent to Apply[And,Flatten[ApproximatelyEqualListable[a,b]]]."];

Begin["`Private`"]

Options[ApproximatelyEqual] = {Precision->$MachinePrecision/2, Accuracy->$MachinePrecision/2, NormFunction->Norm};
ApproximatelyEqual::nonnumeric = "The arguments `` and `` to ApproximatelyEqual were not numeric under the specified NormFunction.";

ApproximatelyEqual[a_, b_, OptionsPattern[]] := Module[
    {norm = OptionValue[NormFunction], norma, normb, normab},
    {norma, normb, normab} = {norm[a], norm[b], norm[a-b]};
    If[Not[And[NumericQ[norma], NumericQ[normb], NumericQ[normab]]], (Message[ApproximatelyEqual::nonnumeric, a, b]; Return[$Failed];)];
    normab <= 10^(-OptionValue[Accuracy]) || normab <= Min[norma,normb]*10^(-OptionValue[Precision])
];

Options[ApproximatelyEqualListable] = {Precision->$MachinePrecision-3, Accuracy->$MachinePrecision/2, NormFunction->Abs};
ApproximatelyEqualListable::nonnumeric = "The arguments `` and `` to ApproximatelyEqualListable were not numeric under the specified NormFunction.";

ApproximatelyEqualListable[a_, b_, opts:OptionsPattern[]] := ApproximatelyEqual[a,b,opts];
SetAttributes[ApproximatelyEqualListable, Listable];

ApproximatelyEqualListableAll[a_, b_, opts:OptionsPattern[]] := Apply[And,Flatten[ApproximatelyEqualListable[a,b,opts]]];

End[]

EndPackage[]
