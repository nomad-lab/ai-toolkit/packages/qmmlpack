(*  qmmlpack                     *)
(*  Matthias Rupp, 2006-2016.    *)
(*  See LICENSE.txt for license. *)

(*  Tests for numeric utilities.  *)

Needs["QMMLPack`"];
Needs["ApproximatelyEqual`", "ApproximatelyEqual.m"];
Needs["Developer`"];
On[Assert];

(*  ****************************  *)
(*  *  linearize, delinearize  *  *)
(*  ****************************  *)

(* Example *)
Test[
	Module[{rag, lin, s, subset},
	    rag = { {"a", "b", "c"}, {"d", "e", "f", "g"}, {"h"}, {}, {"i", "j"}, {"k"} };
		lin = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"};
		s = {1, 4, 8, 9, 9, 11, 12};
		subset = {2, 4, 6};
	    {
			linearize[rag] == {lin, s},
			linearize[rag, Subset->subset] == {{"d", "e", "f", "g", "k"}, {1, 5, 5, 6}},
			delinearize[lin, s] == rag,
			delinearize[lin, s, Subset->subset] == rag[[subset]]
	    }
	]
	,
	ConstantArray[True, 4]
	,
	TestID->"Numerics-20180319-q1B1X9"
]

(*  *************************  *)
(*  *  LowerTriangularPart  *  *)
(*  *************************  *)

(* 3 x 3 matrix *)
Test[
	Module[{mm},
	    mm = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
	    {
	    	LowerTriangularPart[mm] == LowerTriangularPart[mm, 0],
	    	LowerTriangularPart[mm, 0] == N[{1,4,5,7,8,9}],
	    	LowerTriangularPart[mm, 1] == N[{1,2,4,5,6,7,8,9}],
            LowerTriangularPart[mm, 2] == N[Range[9]],
            LowerTriangularPart[mm, 3] == Flatten[mm],
            LowerTriangularPart[mm, 4] == Flatten[mm],
            LowerTriangularPart[mm,-1] == N[{4,7,8}],
            LowerTriangularPart[mm,-2] == N[{7}],
            LowerTriangularPart[mm,-3] == {},
            LowerTriangularPart[mm,-4] == {}
	    }
	]
	,
	ConstantArray[True, 10]
	,
	TestID->"Numerics-20160818-q8B1X9"
]

(* 4 x 3 matrix *)
Test[
    Module[{mm},
        mm = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 10, 11, 12 } };
        {
            LowerTriangularPart[mm] == LowerTriangularPart[mm,0],
            LowerTriangularPart[mm, 0] == N[{1,4,5,7,8,9,10,11,12}],
            LowerTriangularPart[mm, 1] == N[{1,2,4,5,6,7,8,9,10,11,12}],
            LowerTriangularPart[mm, 2] == Flatten[mm],
            LowerTriangularPart[mm, 3] == Flatten[mm],
            LowerTriangularPart[mm,-1] == N[{4,7,8,10,11,12}],
            LowerTriangularPart[mm,-2] == N[{7,10,11}],
            LowerTriangularPart[mm,-3] == N[{10}],
            LowerTriangularPart[mm,-4] == {},
            LowerTriangularPart[mm,-5] == {}
        }
    ]
    ,
    ConstantArray[True, 10]
    ,
    TestID->"Numerics-20160818-K1O1S3"
]

(* 5 x 1 matrix *)
Test[
    Module[{m},
        mm = { { 1 }, { 2 }, { 3 }, { 4 }, { 5 } };
        {
            LowerTriangularPart[mm] == LowerTriangularPart[mm, 0],
            LowerTriangularPart[mm, 0] == Flatten[mm],
            LowerTriangularPart[mm, 1] == Flatten[mm],
            LowerTriangularPart[mm,-1] == N[{2,3,4,5}],
            LowerTriangularPart[mm,-2] == N[{3,4,5}],
            LowerTriangularPart[mm,-3] == N[{4,5}],
            LowerTriangularPart[mm,-4] == N[{5}],
            LowerTriangularPart[mm,-5] == {},
            LowerTriangularPart[mm,-6] == {}
        }
    ]
    ,
    ConstantArray[True, 9]
    ,
    TestID->"Numerics-20160818-c7W8O2"
]

(* 3 x 4 matrix *)
Test[
    Module[{mm},
        mm = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 } };
        {
            LowerTriangularPart[mm] == LowerTriangularPart[mm,0],
            LowerTriangularPart[mm, 0] == N[{1,5,6,9,10,11}],
            LowerTriangularPart[mm, 1] == N[{1,2,5,6,7,9,10,11,12}],
            LowerTriangularPart[mm, 2] == N[{1,2,3,5,6,7,8,9,10,11,12}],
            LowerTriangularPart[mm, 3] == Flatten[mm],
            LowerTriangularPart[mm, 4] == Flatten[mm],
            LowerTriangularPart[mm,-1] == N[{5,9,10}],
            LowerTriangularPart[mm,-2] == N[{9}],
            LowerTriangularPart[mm,-3] == {},
            LowerTriangularPart[mm,-4] == {}
        }
    ]
    ,
    ConstantArray[True, 10]
    ,
    TestID->"Numerics-20160818-Y1B5X7"
]

(* 1 x 5 matrix *)
Test[
    Module[{mm},
        mm = { { 1, 2, 3, 4, 5 } };
        {
            LowerTriangularPart[mm] == LowerTriangularPart[mm,0],
            LowerTriangularPart[mm, 0] == N[{1}],
            LowerTriangularPart[mm, 1] == N[{1,2}],
            LowerTriangularPart[mm, 2] == N[{1,2,3}],
            LowerTriangularPart[mm, 3] == N[{1,2,3,4}],
            LowerTriangularPart[mm, 4] == Flatten[mm],
            LowerTriangularPart[mm, 5] == Flatten[mm],
            LowerTriangularPart[mm,-1] == {},
            LowerTriangularPart[mm,-2] == {}
        }
    ]
    ,
    ConstantArray[True, 9]
    ,
    TestID->"Numerics-20160818-G2H7A1"
]


(*  ****************  *)
(*  *  Symmetrize  *  *)
(*  ****************  *)

(* 1 x 1 matrix *)
Test[
		Symmetrize[{ { 1 } }]
		,
		{ { 1. } }
		,
		TestID->"Numerics-20160922-490OeK"
]

(* 2 x 2 matrix, unchanged *)
Test[
		Symmetrize[{ { 1, 3 }, { 3, 5 } }]
		,
		{ { 1., 3. }, { 3., 5. } }
		,
		TestID->"Numerics-20160922-ahet54"
]

(* 2 x 2 matrix, changed *)
Test[
		Symmetrize[{ { 1, 2 }, { 4, 5 } }]
		,
		{ { 1., 3. }, { 3., 5. } }
		,
		TestID->"Numerics-20160922-Etk45I"
]

(* randomized test *)
Test[
		Module[{m,n},
				n = 100;
				m = RandomReal[{-1, 1}, {n, n}];
				m = Symmetrize[m];
				SymmetricMatrixQ[m]
		]
		,
		True
		,
		TestID->"Numerics-20160922-Agde84"
]


(*  *************************  *)
(*  *  ForwardSubstitution  *  *)
(*  *************************  *)

(* vector right hand side *)
Test[
    {
        (* special case n = 0 *)
        ForwardSubstitution[{{}}, {}],
        (* n = 1 *)
        ForwardSubstitution[{ { 1 } }, { 1 }],
        (* n = 3, diagonal *)
        ForwardSubstitution[{ { 1, 0, 0 }, { 0, 2, 0 }, { 0, 0, 3 } }, { 1, 2, 3 }],
        (* n = 3, non-diagonal *)
        ForwardSubstitution[{ { 1, 0, 0 }, { 2, 3, 0 }, { 4, 5, 6 } }, { 1, 8, 32 }]
    }
    ,
    N[{
        { },
        { 1 },
        { 1, 1, 1 },
        { 1, 2, 3 }
    }]
    ,
    TestID->"Regression-20160719-ZeU7X6"
]

(* matrix right hand side *)
Test[
    {
        (* special case n = 0 *)
        ForwardSubstitution[{{}}, {{}}],
        (* n = 1, m = 1 *)
        ForwardSubstitution[{ { 1 } }, { { 1 } }],
        (* n = 3, m = 3, diagonal *)
        ForwardSubstitution[{ { 1, 0, 0 }, { 0, 2, 0 }, { 0, 0, 3 } }, { { 1, 1, 1 }, { 2, 2, 2 }, { 3, 3, 3 } }],
        (* n = 3, m = 3, non-diagonal *)
        ForwardSubstitution[{ { 1,  0, 0 }, { 2,  3,  0 }, {  4,   5,  6 } }, { { 1, -1, 2 }, { 8, -8, 16 }, { 32, -32, 76 } }],
        (* n = 3, m = 2, non-diagonal *)
        ForwardSubstitution[{ { 1,  0, 0 }, { 2,  3,  0 }, {  4,   5,  6 } }, { { 1, -1 }, { 8, -8 }, { 32, -32 } }],
        (* n = 3, m = 4 *)
        ForwardSubstitution[{ { 1,  0, 0 }, { 2,  3,  0 }, {  4,   5,  6 } }, { { 1, -1, 2, -2 }, { 8, -8, 16, -16 }, { 32, -32, 76, -76 } }]
    }
    ,
    N[{
        {{}},
        { { 1 } },
        { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } },
        { { 1, -1, 2 }, { 2, -2,  4 }, {  3, - 3,  8 } },
        { { 1, -1 }, { 2, -2 }, {  3, - 3 } },
        { { 1, -1, 2, -2 }, { 2, -2,  4, - 4 }, {  3, - 3,  8, - 8 } }
    }]
    ,
    TestID->"Regression-20160720-54r291"
]

(*  **************************  *)
(*  *  BackwardSubstitution  *  *)
(*  **************************  *)

(* vector right hand side *)
Test[
    {
        (* special case n = 0 *)
        BackwardSubstitution[{{}}, {}],
        (* n = 1 *)
        BackwardSubstitution[{ { 1 } }, { 1 }],
        (* n = 3, diagonal *)
        BackwardSubstitution[{ { 3, 0, 0 }, { 0, 2, 0 }, { 0, 0, 1 } }, { 3, 2, 1 }],
        (* n = 3, non-diagonal *)
        BackwardSubstitution[{ { 6, 5, 4 }, { 0, 3, 2 }, { 0, 0, 1 } }, { 32, 8, 1 }]
    }
    ,
    N[{
        { },
        { 1 },
        { 1, 1, 1 },
        { 3, 2, 1 }
    }]
    ,
    TestID->"Regression-20160719-oP930x"
]

(* matrix right hand side *)
Test[
    {
        (* special case n = 0 *)
        BackwardSubstitution[{{}}, {{}}],
        (* n = 1, m = 1 *)
        BackwardSubstitution[{ { 1 } }, { { 1 } }],
        (* n = 3, m = 3, diagonal *)
        BackwardSubstitution[{ { 3, 0, 0 }, { 0, 2, 0 }, { 0, 0, 1 } }, { { 3, 3, 3 }, { 2, 2, 2 }, { 1, 1, 1 } }],
        (* n = 3, m = 3, non-diagonal *)
        BackwardSubstitution[{ { 6, 5, 4 }, { 0, 3, 2 }, { 0, 0, 1 } }, { { 32, -32, 76 }, { 8, -8, 16 }, { 1, -1, 2 } }],
        (* n = 3, m = 2, non-diagonal *)
        BackwardSubstitution[{ { 6, 5, 4 }, { 0, 3, 2 }, { 0, 0, 1 } }, { { 32, -32 }, { 8, -8 }, { 1, -1 } }],
        (* n = 3, m = 4 *)
        BackwardSubstitution[{ { 6, 5, 4 }, { 0, 3, 2 }, { 0, 0, 1 } }, { { 32, -32, 76, -76 }, { 8, -8, 16, -16 }, { 1, -1, 2, -2 } }]
    }
    ,
    N[{
        {{}},
        { { 1 } },
        { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } },
        { { 3, -3, 8 }, { 2, -2, 4 }, { 1, -1, 2 } },
        { { 3, -3 }, { 2, -2 }, { 1, -1 } },
        { { 3, -3, 8, -8 }, { 2, -2, 4, -4 }, { 1, -1, 2, -2 } }
    }]
    ,
    TestID->"Regression-20160720-dK393K"
]

(*  *********************************  *)
(*  *  ForwardBackwardSubstitution  *  *)
(*  *********************************  *)

(* vector right hand side *)
Test[
    {
        (* special case n = 0 *)
        ForwardBackwardSubstitution[{{}}, {}],
        (* n = 1 *)
        ForwardBackwardSubstitution[{ { 1 } }, { 1 }],
        (* n = 3, diagonal *)
        ForwardBackwardSubstitution[{ { 1, 0, 0 }, { 0, 2, 0 }, { 0, 0, 3 } }, { 1, 2, 3 }],
        (* n = 3, non-diagonal *)
        ForwardBackwardSubstitution[{ { 1, 0, 0 }, { 2, 3, 0 }, { 4, 5, 6 } }, { 1, 8, 32 }]
    }
    ,
    N[{
        { },
        { 1 },
        { 1., 1./2, 1./3 },
        { -2/3., -1/6., 1/2. }
    }]
    ,
    TestID->"Regression-20160719-iO83ed"
]

(*  *********************************  *)
(*  *  BackwardForwardSubstitution  *  *)
(*  *********************************  *)

(* vector right hand side *)
Test[
    {
        (* special case n = 0 *)
        BackwardForwardSubstitution[{{}}, {}],
        (* n = 1 *)
        BackwardForwardSubstitution[{ { 1 } }, { 1 }],
        (* n = 3, diagonal *)
        BackwardForwardSubstitution[{ { 3, 0, 0 }, { 0, 2, 0 }, { 0, 0, 1 } }, { 3, 2, 1 }],
        (* n = 3, non-diagonal *)
        BackwardForwardSubstitution[{ { 6, 5, 4 }, { 0, 3, 2 }, { 0, 0, 1 } }, { 32, 8, 1 }]
    }
    ,
    N[{
        { },
        { 1 },
        { 1/3, 1/2, 1 },
        { 283/81, 86/27, -71/9 }
    }]
    ,
    TestID->"Regression-20160719-02IuK8"
]
