(*  qmmlpack                     *)
(*  Matthias Rupp, 2006-2017.    *)
(*  See LICENSE.txt for license. *)

(*  Tests for utility functions.  *)

Needs["QMMLPack`"];
On[Assert];


(*  *****************  *)
(*  *  ElementData  *  *)
(*  *****************  *)

Test[
    Module[{ans, abrvs},
        ans = Range[1, 112];  (* for 113, 115, 117, 118 Mathematica uses old abbreviations at time of writing *)
        abrvs = Map[ElementData[#, "Abbreviation"] &, ans];
        {
            Map[elementData[#, "AtomicNumber"] &, ans] == ans,
            Map[elementData[#, "AtomicNumber"] &, abrvs] == ans,
            Map[elementData[#, "Abbreviation"] &, ans] == abrvs,
            Map[elementData[#, "Abbreviation"] &, abrvs] == abrvs
        }
    ]
    ,
    ConstantArray[True, 4]
    ,
    TestID->"Utility-20170810-eiyTg1"
]
