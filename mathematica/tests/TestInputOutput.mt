(*  qmmlpack                     *)
(*  Matthias Rupp, 2006-2016.    *)
(*  See LICENSE.txt for license. *)

(*  Tests for import of atomistic systems in extended XYZ format.  *)

(* Known issues: *)
(* - Randomized tests at end rarely (set n > 1000) fail for xyz coordinates. *)
(*   This might indicate a problem in the last digit or similar. *)

Needs["QMMLPack`"];
Needs["ApproximatelyEqual`", "ApproximatelyEqual.m"];
Needs["Developer`"];
On[Assert];

(*  ****************************************************  *)
(*  *  Previous Mathematica code now used for testing  *  *)
(*  ****************************************************  *)

(* Atomic numbers to avoid calls to ElementData and associated bug (Mathematica sometimes closes the ElementData stream when exporting) *)
elementAbbreviations = {
    "H"  , "He" , "Li" , "Be" , "B"  , "C"  , "N"  , "O"  , "F"  , "Ne" , "Na" , "Mg" , "Al" ,
    "Si" , "P"  , "S"  , "Cl" , "Ar" , "K"  , "Ca" , "Sc" , "Ti" , "V"  , "Cr" , "Mn" , "Fe" ,
    "Co" , "Ni" , "Cu" , "Zn" , "Ga" , "Ge" , "As" , "Se" , "Br" , "Kr" , "Rb" , "Sr" , "Y"  ,
    "Zr" , "Nb" , "Mo" , "Tc" , "Ru" , "Rh" , "Pd" , "Ag" , "Cd" , "In" , "Sn" , "Sb" , "Te" ,
    "I"  , "Xe" , "Cs" , "Ba" , "La" , "Ce" , "Pr" , "Nd" , "Pm" , "Sm" , "Eu" , "Gd" , "Tb" ,
    "Dy" , "Ho" , "Er" , "Tm" , "Yb" , "Lu" , "Hf" , "Ta" , "W"  , "Re" , "Os" , "Ir" , "Pt" ,
    "Au" , "Hg" , "Tl" , "Pb" , "Bi" , "Po" , "At" , "Rn" , "Fr" , "Ra" , "Ac" , "Th" , "Pa" ,
    "U"  , "Np" , "Pu" , "Am" , "Cm" , "Bk" , "Cf" , "Es" , "Fm" , "Md" , "No" , "Lr" , "Rf" ,
    "Db" , "Sg" , "Bh" , "Hs" , "Mt" , "Ds" , "Rg" , "Cn" , "Uut", "Fl" , "Uup", "Lv" , "Uus", "Uuo"};
Do[elementData[i, "Abbreviation"] = elementAbbreviations[[i]]; elementData[elementAbbreviations[[i]], "AtomicNumber"] = i;, {i, Length[elementAbbreviations]}];

Options[extendedXYZImportDefault] = {"AdditionalProperties" -> False};

extendedXYZImportDefault[s_String, opts___?OptionQ] := Module[
    {text,blocks,nblocks,parts,start,end,i,j,block,nlpos,data,na,nap,an,xyz,mp,ap,addp,res,oaddprop},

    input = StringToStream[s];

    oaddprop = OptionValue[extendedXYZImportDefault, opts, "AdditionalProperties"];
    Assert[MemberQ[{False, True}, oaddprop]];

    (* Load data *)
    text = Import[input, "String"];
    text = StringTrim[text] <> "\n";

    (* Parse start and end positions of molecule blocks *)
    blocks = If[oaddprop,
        (* Molecule blocks are separated by blank lines *)
        end = Append[StringPosition[text, "\n"~~(" "|"\t")...~~"\n"][[All,2]], StringLength[text]];
        start = Prepend[Most[end + 1], 1];
        Transpose[{start,end}],
        (* Parse atom numbers and count lines *)
        (* The only lines that can contain a single number are the first and the second lines of a molecule block *)
        start = StringPosition[text, StartOfLine~~Whitespace...~~NumberString~~(" "|"\t")...~~"\n"~~Except["\n"]...~~"\n", Overlaps -> False][[All,1]];
        (*start = Map[If[StringTake[text, {#}] == "\n", #+1, #] &, start]; (* Corrects for blank lines without additional properties *)*)
        start = MapThread[If[#1 == "\n", #2+1, #2] &, {StringTake[text, Transpose[{start}]], start}]; (* Corrects for blank lines without additional properties *)
        end = Append[Rest[start - 1], StringLength[text]];
        Transpose[{start,end}]
    ];

    (* Number of atomic properties; assumes that this is the same for all atoms *)
    nap = Length[StringSplit[Most[StringSplit[text, "\n", 4]][[3]]]] - 4;

    (* Parse each molecule *)
    (* Profiling indicates that StringTake is abysmally slow, as in this solution:           *)
    (*     parts = Map[StringTake[text, #] &, blocks]; takes 20s for 1000 molecules          *)
    (* Using StringInsert / StringSplit is faster (5s instead of 20s):                        *)
    (*     parts = StringInsert[text, "@", blocks[[All,1]]]; parts = StringSplit[parts, "@"]; *)
    (* Using Characters / Take / StringJoin is faster again (1.5s instead of 5s):             *)
    (*     parts = Characters[text]; parts = Map[StringJoin[Take[parts, #]] &, blocks];       *)
    nblocks = Length[blocks];
    text = Characters[text];
    parts = Map[StringJoin[Take[text, #]] &, blocks];
    res = Reap[
        Do[(
            block = parts[[i]];
            nlpos = StringPosition[block, "\n", Overlaps -> False][[All,1]];  (* Positions of newlines *)

            (* 1: Number of atoms *)
            na = FromDigits[StringTrim[StringTake[block, nlpos[[1]]]]];

            (* 2: Name, molecular properties *)
            Sow[Map[If[StringMatchQ[#, NumberString], ToExpression[#], #] &, StringSplit[StringTake[block, {nlpos[[1]]+1, nlpos[[2]]}]]], 1];

            (* 3: Atom block *)
            data = ReadList[StringToStream[StringTake[block, {nlpos[[2]]+1, nlpos[[na+2]]}]], Join[{Word,Number,Number,Number}, ConstantArray[Word,nap]]];
            data[[All, 5;;]] = ToExpression[data[[All, 5;;]]];
            (* data = ReadList[StringToStream[StringTake[block, {nlpos[[2]]+1, nlpos[[na+2]]}]], Join[{Word,Number,Number,Number}, Table[Number,{nap}]]]; *)
            Sow[data[[All,1]], 2]; (* Atomic numbers *)
            Sow[data[[All,{2,3,4}]], 3]; (* Coordinates *)
            If[nap > 0, Sow[Transpose[data[[All,5;;nap+4]]], 4]];

            (* 4: Additional properties block *)
            If[oaddprop, Sow[StringSplit[StringTake[block, {nlpos[[na+2]], -1}], "\n"], 5]];
        )
    , {i, nblocks}]][[2]];

    mp = Transpose[res[[1]]];
    If[Not[mp === {}], mp = Transpose[mp]];
    an = res[[2]];
    an = Map[If[StringMatchQ[#,DigitCharacter..], elementData[FromDigits[#], "Abbreviation"], #] &, an, {2}];
    xyz = res[[3]];
    xyz = N[xyz]; (* convert to real numbers *)
    ap = If[nap > 0, Transpose[res[[4]]], {}];
    If[Not[ap === {}], ap = Map[Transpose, Transpose[ap]]];
    ap = Map[If[NumberQ[#], #, SymbolName[#]] &, ap, {-1}];
    addp = If[oaddprop && Length[res]>4, Transpose[res[[5]]], {}];
    If[Not[oaddprop], addp = {}];

    {"VertexTypes" -> an, "VertexCoordinates" -> xyz, "MolecularProperties" -> mp, "AtomicProperties" -> ap, "AdditionalProperties" -> addp}
];

(*  **************************************  *)
(*  *  End of previous Mathematica code  *  *)
(*  **************************************  *)

compareExtendedXYZ[lhs_, rhs_] := Which[
    ListQ[lhs] && ListQ[rhs] && Length[lhs] == Length[rhs], And@@MapThread[compareExtendedXYZ[#1, #2] &, {lhs, rhs}],
    Head[lhs] === Rule && Head[rhs] === Rule, (lhs[[1]] == rhs[[1]]) && compareExtendedXYZ[lhs[[2]], rhs[[2]]],
    NumericQ[lhs] && NumericQ[rhs], ApproximatelyEqual[lhs, rhs, Accuracy->5],
    True, lhs == rhs
];

(*  ****************************  *)
(*  *  Element Specifications  *  *)
(*  ****************************  *)

(* Standard one to three letter abbreviations *)
Test[
    Module[{els, s},
        els = "H He Li Be B C N O F Ne Na Mg Al Si P S Cl Ar K Ca Sc Ti V " <>
              "Cr Mn Fe Co Ni Cu Zn Ga Ge As Se Br Kr Rb Sr Y Zr Nb Mo Tc " <>
              "Ru Rh Pd Ag Cd In Sn Sb Te I Xe Cs Ba La Ce Pr Nd Pm Sm Eu " <>
              "Gd Tb Dy Ho Er Tm Yb Lu Hf Ta W Re Os Ir Pt Au Hg Tl Pb Bi " <>
              "Po At Rn Fr Ra Ac Th Pa U Np Pu Am Cm Bk Cf Es Fm Md No Lr " <>
              "Rf Db Sg Bh Hs Mt Ds Rg Cn Uut Fl Uup Lv Uus Uuo";
        els = StringSplit[StringTrim[els]];
        s = IntegerString[Length[els]] <> "\n\n" <> StringJoin[Map[# <> " 1 2 3\n" &, els]];
        "VertexTypes" /. ImportString[s, "extXYZ"]
    ]
    ,
    {Map[elementData[#, "Abbreviation"] &, Range[118]]}
    ,
    TestID->"io_extxyz-20160524-Z0U7X7"
];

(* atomic number as element specifications *)
Test[
    Module[{els, s},
        els = "  1   2   3   4   5   6   7   8   9  10  11  12  13  14  15 " <>
              " 16  17  18  19  20  21  22  23  24  25  26  27  28  29  30 " <>
              " 31  32  33  34  35  36  37  38  39  40  41  42  43  44  45 " <>
              " 46  47  48  49  50  51  52  53  54  55  56  57  58  59  60 " <>
              " 61  62  63  64  65  66  67  68  69  70  71  72  73  74  75 " <>
              " 76  77  78  79  80  81  82  83  84  85  86  87  88  89  90 " <>
              " 91  92  93  94  95  96  97  98  99 100 101 102 103 104 105 " <>
              "106 107 108 109 110 111 112 113 114 115 116 117 118";
        els = StringSplit[StringTrim[els]];
        s = IntegerString[Length[els]] <> "\n\n" <> StringJoin[Map[# <> " 1 2 3\n" &, els]];
        "VertexTypes" /. ImportString[s, "extXYZ"]
    ]
    ,
    {Map[elementData[#, "Abbreviation"] &, Range[118]]}
    ,
    TestID->"io_extxyz-20160525-ZA12E1"
]

(*  **********************  *)
(*  *  Atom coordinates  *  *)
(*  **********************  *)

(* Different formats for floating point numbers *)
Test[
    Module[{s},
        s = "4 \n"                           <>
            "\n"                             <>
            "C  1       2.0     3.000001 \n" <>
            "C -1      -2.0    -3.001    \n" <>
            "C  1.2e-1 +1.2e-1 -1.2E-1   \n" <>
            "C  0.       .0     0        \n" ;
        N[( "VertexCoordinates" /. ImportString[s, "extXYZ"] )]
    ]
    ,
    {{
        { 1.0 ,  2.0 ,  3.000001},
        {-1.0 , -2.0 , -3.001   },
        { 0.12,  0.12, -0.12    },
        { 0.  ,  0.  ,  0.      }
    }}
    ,
    TestID->"io_extxyz-20160527-IDe512"
]

(*  **************************  *)
(*  *  Molecular properties  *  *)
(*  **************************  *)

(* Pure string molecular properties *)
Test[
    Module[{s},
        s = "1\n ABC\tdef\t\t string_token\nC 1 2 3\n";
        "MolecularProperties" /. ImportString[s, "extXYZ"]
    ]
    ,
    {{"ABC", "def", "string_token"}}
    ,
    TestID->"io_extxyz-20160527-93Jku8"
]

(* Integer molecular properties *)
Test[
    Module[{s},
        s = "1\n abc 123 ABC\nC 1 2 3\n";
        "MolecularProperties" /. ImportString[s, "extXYZ"]
    ]
    ,
    {{"abc", 123, "ABC"}}
    ,
    TestID->"io_extxyz-20160527-1JJKaq"
]

(* Floating point molecular properties *)
Test[
    Module[{s},
        s = "1\n abc ABC 1.23 -0.1E+1\nC 1 2 3\n";
        N["MolecularProperties" /. ImportString[s, "extXYZ"]]
    ]
    ,
    {{"abc", "ABC", 1.23, -1.0}}
    ,
    TestID->"io_extxyz-20160527-93Jwe9"
]

(*  ***********************  *)
(*  *  Atomic properties  *  *)
(*  ***********************  *)

(* Pure string atomic properties *)
Test[
    Module[{s},
        s = "2\n\nC 1 2 3 abc ABC\nC 1 2 3 def\n";
        "AtomicProperties" /. ImportString[s, "extXYZ"]
    ]
    ,
    {{{"abc", "ABC"}, {"def"}}}
    ,
    TestID->"io_extxyz-20160527-A1k4Ko"
]

(* Integer atomic properties *)
Test[
    Module[{s},
        s = "2\n\nC 1 2 3 abc 123 ABC\nB 1 2 3 456\n";
        "AtomicProperties" /. ImportString[s, "extXYZ"]
    ]
    ,
    {{{"abc", 123, "ABC"}, {456}}}
    ,
    TestID->"io_extxyz-20160527-LwwW1r"
]

(* Floating point atomic properties *)
Test[
    Module[{s},
        s = "2\n\nC 1 2 3 abc 1.23 ABC\nB 1 2 3 4.56 4e-1\n";
        N["AtomicProperties" /. ImportString[s, "extXYZ"]]
    ]
    ,
    {{{"abc", 1.23, "ABC"}, {4.56, 0.4}}}
    ,
    TestID->"io_extxyz-20160527-04kdE2"
]

(*  ***************************  *)
(*  *  Additional properties  *  *)
(*  ***************************  *)

(* One additional property line *)
Test[
    Module[{s},
        s = "1\n\nC 1 2 3\nabc 123\n \t\n";
        "AdditionalProperties" /. ImportString[s, "extXYZ", "AdditionalProperties" -> True]
    ]
    ,
    {{{"abc", 123}}}
    ,
    TestID->"io_extxyz-20160527-Ue4Jm2"
]

(* Two additional property lines *)
Test[
    Module[{s},
        s = "1\n\nC 1 2 3\nabc   \n \t\t 4.56\t \n\n";
        "AdditionalProperties" /. ImportString[s, "extXYZ", "AdditionalProperties" -> True]
    ]
    ,
    {{{"abc"}, {4.56}}}
    ,
    TestID->"io_extxyz-20160527-3402M4"
]

(* Three molecules with additional properties *)
Test[
    Module[{s},
        s = "1\n\nC 1 2 3\nabc\n1d 1+1 x e f\n\n1\n\nC 1 2 3\n123 4.56\n\n1\n\nC 1 2 3\n\n";
        "AdditionalProperties" /. ImportString[s, "extXYZ", "AdditionalProperties" -> True]
    ]
    ,
    {{{"abc"},{"1d","1+1","x","e","f"}}, {{123, 4.56}}, {}}
    ,
    TestID->"io_extxyz-20160527-Q140JL"
]

(*  *******************  *)
(*  *  Special cases  *  *)
(*  *******************  *)

(* Floating point special cases *)
Test[
    N[ "MolecularProperties" /. ImportString["1\n0 0. 0.0 .0\nC 1 2 3\n", "extXYZ"] ]
    ,
    N[{{0, 0., 0., 0.}}] (* Mathematica recognizes 0. == 0 *)
    ,
    TestID->"io_extxyz-20160527-12Kd33"
]

(* Floating point e notation *)
Test[
    "MolecularProperties" /. ImportString["1\n1.23E-1\nC 1 2 3\n", "extXYZ"]
    ,
    {{0.123}}
    ,
    TestID->"io_extxyz-20160527-O49IUk"
]

(* 2016-04-14 Test whether parsing works if last line is not terminated by EOL *)
Test[
    Module[{s1,s2,res1,res2},
        s1 = "2\n\nC 1 2 3 abc 1.23 ABC\nB 1 2 3 4.56 4e-1\n"; (* terminated *)
        s2 = "2\n\nC 1 2 3 abc 1.23 ABC\nB 1 2 3 4.56 4e-1";  (* not terminated *)
        res1 = ImportString[s1, "extXYZ"];
        res2 = ImportString[s2, "extXYZ"];
        res1 == res2
    ]
    ,
    True
    ,
    TestID->"io_extxyz-20160527-Qnd3lf"
]

(* Parsing of two molecules, no separating empty line *)
Test[
    Module[{s},
        s = "2\n abc\nC 1 2 3 X\nB 4 5 6 4321\n1\nxyz\nH 7 8 99 abv\n";
        ImportString[s, "extXYZ"]
    ]
    ,
    {
        "VertexTypes" -> {{"C", "B"}, {"H"}},
        "VertexCoordinates" -> {{{1.,2.,3.},{4.,5.,6.}}, {{7.,8.,99.}}},
        "MolecularProperties" -> {{"abc"}, {"xyz"}},
        "AtomicProperties" -> {{{"X"},{4321}}, {{"abv"}}},
        "AdditionalProperties" -> {}
    }
    ,
    TestID->"io_extxyz-20160527-1Ml982"
]

(*  ***********************  *)
(*  *  File input/output  *  *)
(*  ***********************  *)

(* Reads input from file; input is EOL-terminated *)
Test[
    Module[{fn, res},
        fn = CreateFile[];
        Export[fn, "1\n mp 123\n C 1 2 3 abc 9.1\n", "String"];
        res = Import[fn, "extXYZ"];
        DeleteFile[fn];
        res
    ]
    ,
    {
        "VertexTypes" -> {{"C"}},
        "VertexCoordinates" -> {{{1.,2.,3.}}},
        "MolecularProperties" -> {{"mp", 123}},
        "AtomicProperties" -> {{{"abc", 9.1}}},
        "AdditionalProperties" -> {}
    }
    ,
    TestID->"io_extxyz-20160527-Kd02kL"
]

(* Reads input from file; input is not EOL-terminated *)
Test[
    Module[{fn, res},
        fn = CreateFile[];
        Export[fn, "1\n mp 123\n C 1 2 3 abc 9.1", "String"];
        res = Import[fn, "extXYZ"];
        DeleteFile[fn];
        res
    ]
    ,
    {
        "VertexTypes" -> {{"C"}},
        "VertexCoordinates" -> {{{1.,2.,3.}}},
        "MolecularProperties" -> {{"mp", 123}},
        "AtomicProperties" -> {{{"abc", 9.1}}},
        "AdditionalProperties" -> {}
    }
    ,
    TestID->"io_extxyz-20160527-A4j4jj"
]

(* TODO: redo and check below *)

(*  ************  *)
(*  *  Export  *  *)
(*  ************  *)

(* Write simple single molecule to string *)
Test[
    Module[{res},
        res = ExportString[{{"H"}, {{1,2,3}}}, "extXYZ", "MolecularProperties" -> {"mp", 1, 1.2}, "AtomicProperties" -> {{"abc", 12, 1.23}}];
        ImportString[res, "extXYZ"]
    ]
    ,
    {
        "VertexTypes" -> {{"H"}},
        "VertexCoordinates" -> {{{1.,2.,3.}}},
        "MolecularProperties" -> {{"mp", 1, 1.2}},
        "AtomicProperties" -> {{{"abc", 12, 1.23}}},
        "AdditionalProperties" -> {}
    }
    ,
    TestID->"io_extxyz-20160527-3kIKdd"
]

(* Write two simple molecules to string *)
Test[
    Module[{an,xyz,res,mp,ap},
        an = {{"C", "B"}, {"H"}};
        xyz = {{{1.1,2.2,3.3},{4.4,5.5,6.6}}, {{1,2,3}}};
        mp = {{"xyz", 1, 2.2}, {"abc", 2, 3.3}};
        ap = {{{"xyz", 1, 1.2},{"XYZ", 2, 3.1}}, {{"abc", 1, 99.1}}};
        res = ExportString[{an,xyz}, "extXYZ", "MolecularProperties" -> mp, "AtomicProperties" -> ap];
        res = ImportString[res, "extXYZ"]
    ]
    ,
    Evaluate[extendedXYZImportDefault["2\nxyz 1 2.2\nC 1.1 2.2 3.3 xyz 1 1.2\nB 4.4 5.5 6.6 XYZ 2 3.1\n   1\nabc 2 3.3\nH 1 2 3 abc 1 99.1"]]
    ,
    EquivalenceFunction -> compareExtendedXYZ
    ,
    TestID->"io_extxyz-20160527-3kIKdd"
]

(*
(* Returns a 'random' molecule for testing purposes. *)
randomMolecule[addprop_: False] := Module[{k,an,xyz,mp,ap,addp,mol},
    randomToken[] := Module[{res,chars,alnum,ind},
        chars = Join[CharacterRange["a", "z"], CharacterRange["A", "Z"]];
        alnum = Join[chars, CharacterRange["0", "9"]];
        Switch[RandomChoice[{"str", "int", "float"}],
            "str", (
                res = StringJoin[Table[RandomChoice[alnum], RandomInteger[1,30]]];
                (* prevent strings with only digits; these would be parsed as integers *)
                ind = Union[Table[RandomInteger[{1, StringLength[res]}], RandomInteger[1, StringLength[res]]]];
                res = StringReplacePart[res, Table[RandomChoice[chars], Length[ind]], Map[{#,#} &, ind]];
                StringJoin @@ res
                ),
            "int", RandomInteger[{-1000, 1000}],
            "float", RandomReal[{-10^10,10^10}]
        ]
    ];

    k = RandomInteger[{1, 21}];

    (* Element types *)
    an = Table[RandomInteger[{1, 118}], k];

    (* Coordinates *)
    xyz = RandomReal[{-10, 10}, {k, 3}];

    (* Molecular properties *)
    mp = Table[randomToken[], RandomInteger[{0, 4}]];

    (* Atomic properties *)
    ap = RandomInteger[{0, 2}];
    ap = Table[randomToken[], k, ap];
    If[Flatten[ap] === {}, ap = {}];

    (* Additional properties *)
    addp = Table[randomToken[], RandomInteger[{1,5}], RandomInteger[{0,3}]];

    mol = {"VertexTypes" -> an, "VertexCoordinates" -> xyz, "MolecularProperties" -> mp, "AtomicProperties" -> ap};
    If[addprop, AppendTo[mol, "AdditionalProperties" -> addp]];

    mol
];

(* Write random single molecule to string *)
Test[
    Module[{lhs, rhs, an, xyz, mp, ap},
        lhs = randomMolecule[];
        {an, xyz, mp, ap} = Map[# /. lhs &, {"VertexTypes", "VertexCoordinates", "MolecularProperties", "AtomicProperties"}];
        If[mp === {{}}, mp = {}]; If[ap === {{}}, ap = {}];
        rhs = ExportString[{an, xyz}, "extXYZ", "MolecularProperties" -> mp, "AtomicProperties" -> ap];
        rhs = ImportString[rhs, "extXYZ"];
        If[rhs === $Failed, Print["\n\nParsing failed for lhs = ", lhs, "\n\n"]];
        an = Map[elementData[#, "Abbreviation"] &, an];
        lhs = {"VertexTypes" -> {an}, "VertexCoordinates" -> {xyz}, "MolecularProperties" -> {mp}, "AtomicProperties" -> {ap}, "AdditionalProperties" -> {}};
        If[Not[compareExtendedXYZ[lhs, rhs]], Print["\n\n* lhs = ",lhs,"\n","* rhs = ",rhs,"\n"]];
        compareExtendedXYZ[lhs, rhs]
    ]
    ,
    True
    ,
    TestID->"io_extxyz-20160527-1JLl3r"
]

(* Write many random molecules to file *)
Test[
    Module[{lhs,rhs,fn,an,xyz,mp,ap},
Return[{True,True,True,True}];
        lhs = Table[randomMolecule[], 10];
        fn = CreateFile[];
        {an, xyz, mp, ap} = Map[# /. lhs &, {"VertexTypes", "VertexCoordinates", "MolecularProperties", "AtomicProperties"}];
        If[Flatten[mp] === {}, mp = {}]; If[Flatten[ap] === {}, ap = {}];
        Export[fn, {an, xyz}, "extXYZ", "MolecularProperties" -> mp, "AtomicProperties" -> ap];
        rhs = Import[fn, "extXYZ"];
        DeleteFile[fn];
        If[Not[ApproximatelyEqualListableAll[mp, "MolecularProperties" /. rhs, NormFunction->ptnf]], findDifference[mp, "MolecularProperties" /. rhs]];
        If[Not[ApproximatelyEqualListableAll[ap, "AtomicProperties" /. rhs, NormFunction->ptnf]], findDifference[ap, "AtomicProperties" /. rhs]];
        {
            Map[elementData[#, "Abbreviation"] &, an, {-1}] == "VertexTypes" /. rhs,
            ApproximatelyEqualListableAll[xyz, "VertexCoordinates"   /. rhs],
            ApproximatelyEqualListableAll[mp , "MolecularProperties" /. rhs, NormFunction->ptnf],
            ApproximatelyEqualListableAll[ap , "AtomicProperties"    /. rhs, NormFunction->ptnf]
        }
    ]
    ,
    ConstantArray[True, 4]
    ,
    TestID->"io_extxyz-20160527-3ldoOl"
]
*)
