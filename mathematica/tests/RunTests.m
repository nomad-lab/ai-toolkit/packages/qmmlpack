(*  qmmlpack                     *)
(*  Matthias Rupp, 2006-2016.    *)
(*  See LICENSE.txt for license. *)

(*  Mathematica unit tests  *)

Needs["MUnit`"];

succCount = 0;
failCount = 0;
startTime = AbsoluteTime[];
lastTitle = "unknown";

QMMLLogger[] :=	With[{logger = Unique["MUnit`Loggers`Private`logger"]}, Module[{fatal = False},
    logger /: LogStart[logger, title_] := lastTitle = ToString@title;
    logger /: LogMessage[logger, msg_String] := WriteString[$Output, "\n" <> msg <> "\n"];
    logger /: LogFatal[logger, msg_String] := WriteString[$Output, "\n" <> msg <> "\n"];
    (* logger /: LogSuccess[logger, (*tr*)_?TestResultQ] := *)
    logger /: LogFailure[logger, tr_?TestResultQ] :=
        Module[{msg = TestFailureMessage[tr]},
            WriteString[$Output, "! Test '" <> ToString[TestID[tr], OutputForm] <> "' in '" <> lastTitle <> " failed.\n"];
            WriteString[$Output, "  Expected output: " <> ToString[ExpectedOutput[tr], InputForm] <> "\n"];
            WriteString[$Output, "  Actual output: " <> ToString[ActualOutput[tr], InputForm] <> "\n"];
            If[msg != "", WriteString[$Output, "\n** " <> ToString[msg] <> " **\n"]];
        ];
    logger /: LogMessagesFailure[logger, (*tr*)_?TestResultQ] := WriteString[$Output, "*"];
    logger /: LogError[logger, tr_] :=
        Module[{msg = ErrorMessage[tr]},
            If[msg != "", WriteString[$Output, "\n** " <> msg <> " **\n"]]
        ];
    logger /: LogFatal[logger, msg_String] :=
        (fatal = True;
        WriteString[$Output, "\n" <> msg <> "\n"]);
    logger /: LogEnd[logger, testCnt_, successCnt_, failCnt_, msgFailCnt_, skippedTestCnt_, errorCnt_, (*abort*)_] :=
        (
            succCount += successCnt; failCount += failCnt;
        );
    logger
    ]
]

(* When running unit tests, ignore other installations of qmmlpack and use the tested one *)
PrependTo[$Path, ParentDirectory[]]; (* Relies on running tests from current directory *)

exitCode = 0;

exitCode = If[TestRun["TestInit.mt"           , Loggers :> {QMMLLogger[]}], exitCode, 1];
exitCode = If[TestRun["TestUtility.mt"        , Loggers :> {QMMLLogger[]}], exitCode, 1];
exitCode = If[TestRun["TestInputOutput.mt"    , Loggers :> {QMMLLogger[]}], exitCode, 1];
exitCode = If[TestRun["TestKernels.mt"        , Loggers :> {QMMLLogger[]}], exitCode, 1];
exitCode = If[TestRun["TestDistances.mt"      , Loggers :> {QMMLLogger[]}], exitCode, 1];
exitCode = If[TestRun["TestNumerics.mt"       , Loggers :> {QMMLLogger[]}], exitCode, 1];
exitCode = If[TestRun["TestValidation.mt"     , Loggers :> {QMMLLogger[]}], exitCode, 1];
exitCode = If[TestRun["TestRegression.mt"     , Loggers :> {QMMLLogger[]}], exitCode, 1];
(*exitCode = If[TestRun["TestRepresentations.mt", Loggers :> {QMMLLogger[]}], exitCode, 1];*)

endTime = AbsoluteTime[];
Print[succCount, " tests passed in ", Round[endTime-startTime], "s"];
If[failCount > 0, Print["! ", failCount, " test", If[failCount > 1, "s", ""], " failed."]];

Exit[exitCode];
