// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Mathematica bindings for extended XYZ format.

#include "mathematica.hpp"

#include <memory>
#include <string>
#include <cstring>
#include <cstdio>
#include <iomanip>

#include "qmmlpack/io_extxyz.hpp"

//  ////////////////////////////
//  //  Import extXYZ format  //
//  ////////////////////////////

//  Implementation helpers

void _format_float(std::ostringstream & s, const double f)
{
    char buf[32];
    const int n = std::sprintf(buf, "%f", f);
    if(n < 1) throw QMML_ERROR("Error formatting floating point number.");
    s << buf;
}

void _format_parsing_token(std::ostringstream & s, qmml::parsing_token const& token)
{
         if(token.is_int   ()) s << token.intv();
    else if(token.is_float ()) _format_float(s, token.floatv());
    else if(token.is_string()) s << "\"" << token.stringv() << "\"";
    else throw QMML_ERROR("Unknown token type.");
}

void _format_parsing_tokens(std::ostringstream & s, std::vector<qmml::parsing_token> const& v)
{
    s << "{";
    for(auto j = v.cbegin(); j != v.cend(); ++j)
    {
        _format_parsing_token(s, *j);
        s << ",";
    }
    if(!v.empty()) s.seekp(-1, s.cur);
    s << "}";
}

// - If there are no molecular properties, atomic properties, or additional properties,
//   a rule is still returned because Import[...] expects it.
void _format_extxyz_data(std::vector<qmml::extxyz_data> const& res, std::string & s, const bool addp)
{
    static char const*const els [] = {  // indexing starts from 0
        "H"  , "He" , "Li" , "Be" , "B"  , "C"  , "N" , "O"  , "F"  , "Ne" , "Na" , "Mg" , "Al" , "Si",
        "P"  , "S"  , "Cl" , "Ar",  "K"  , "Ca" , "Sc", "Ti" , "V"  , "Cr" , "Mn",  "Fe" , "Co" , "Ni",
        "Cu" , "Zn" , "Ga" , "Ge",  "As" , "Se" , "Br", "Kr" , "Rb" , "Sr" , "Y" ,  "Zr" , "Nb" , "Mo",
        "Tc" , "Ru" , "Rh" , "Pd",  "Ag" , "Cd" , "In", "Sn" , "Sb" , "Te" , "I" ,  "Xe" , "Cs" , "Ba",
        "La" , "Ce" , "Pr" , "Nd",  "Pm" , "Sm" , "Eu", "Gd" , "Tb" , "Dy" , "Ho",  "Er" , "Tm" , "Yb",
        "Lu" , "Hf" , "Ta" , "W" ,  "Re" , "Os" , "Ir", "Pt" , "Au" , "Hg" , "Tl",  "Pb" , "Bi" , "Po",
        "At" , "Rn" , "Fr" , "Ra",  "Ac" , "Th" , "Pa", "U"  , "Np" , "Pu" , "Am",  "Cm" , "Bk" , "Cf",
        "Es" , "Fm" , "Md" , "No",  "Lr" , "Rf" , "Db", "Sg" , "Bh" , "Hs" , "Mt",  "Ds" , "Rg" , "Cn",
        "Uut", "Fl" , "Uup", "Lv",  "Uus", "Uuo"
    };

    std::ostringstream ss;
    ss.precision(17); // see Theorem 15 in D Goldberg, "What Every Computer Scientist Should Know About Floating Point Arithmetic"

    ss << "{";

    // atomic numbers
    ss << "\"VertexTypes\"->{";
    for(auto i = res.cbegin(); i != res.cend(); ++i)
    {
        ss << "{";
        for(auto j = i->an.cbegin(); j != i->an.cend(); ++j) ss << "\"" << els[*j-1] << "\",";
        ss.seekp(-1, ss.cur);  // pop_back trailing command
        ss << "},";
    }
    ss.seekp(-1, ss.cur);  // pop_back trailing command
    ss << "}";

    // atom coordinates
    ss << ", \"VertexCoordinates\"->{";
    for(auto i = res.cbegin(); i != res.cend(); ++i)
    {
        ss << "{";
        for(auto j = i->xyz.cbegin(); j != i->xyz.cend(); ++j)
        {
            ss << "{";
            _format_float(ss, (*j)[0]); ss << ",";
            _format_float(ss, (*j)[1]); ss << ",";
            _format_float(ss, (*j)[2]);
            ss << "},";
        }
        ss.seekp(-1, ss.cur);  // pop_back trailing command
        ss << "},";
    }
    ss.seekp(-1, ss.cur);  // pop_back trailing command
    ss << "}";

    // molecular properties
    ss << ", \"MolecularProperties\"->{";
    bool emptymp = true; for(auto i = res.cbegin(); i != res.cend(); ++i) if(!(i->mp.empty())) { emptymp = false; break; }
    if(!emptymp)
    {
        for(auto i = res.cbegin(); i != res.cend(); ++i) { _format_parsing_tokens(ss, i->mp); ss << ","; }
        if(!res.empty()) ss.seekp(-1, ss.cur);  // pop_back trailing command
    }
    ss << "}";

    // atomic properties
    ss << ", \"AtomicProperties\"->{";
    bool emptyap = true; for(auto i = res.cbegin(); i != res.cend(); ++i) for(auto j = i->ap.cbegin(); j != i->ap.cend(); ++j) if(!(j->empty())) { emptyap = false; break; }
    if(!emptyap)
    {
        for(auto i = res.cbegin(); i != res.cend(); ++i)
        {
            ss << "{";
            for(auto j = i->ap.cbegin(); j != i->ap.cend(); ++j) { _format_parsing_tokens(ss, *j); ss << ","; }
            if(!i->ap.empty()) ss.seekp(-1, ss.cur);
            ss << "},";
        }
        if(!res.empty()) ss.seekp(-1, ss.cur);
    }
    ss << "}";

    // additional properties
    ss << ", \"AdditionalProperties\"->{";
    bool emptyaddp = true; for(auto i = res.cbegin(); i != res.cend(); ++i) if(!(i->addp.empty())) { emptyaddp = false; break; }
    if(addp && !emptyaddp)
    {
        for(auto i = res.cbegin(); i != res.cend(); ++i)
        {
            ss << "{";
            for(auto j = i->addp.cbegin(); j != i->addp.cend(); ++j) { _format_parsing_tokens(ss, *j); ss << ","; }
            if(!(i->addp.empty())) ss.seekp(-1, ss.cur);
            ss << "},";
        }
        if(!(res.empty())) ss.seekp(-1, ss.cur);
    }
    ss << "}";

    ss << "}";
    s.append(ss.str());
}

//
//  Imports molecules in extended XYZ format from file
//
//  Input:
//    [0] UTF8String  Filename
//    [1] Boolean  Whether molecules have additional properties block
//  Output:
//    [0] String  Mathematica expression of results data structure
//

EXTERN_C DLLEXPORT int qmml_import_extxyz1(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    static std::string s;  // Lifetime beyond function call is necessary because Mathematica needs to access the result string.

    // Arguments processing
    if( argc != 2 ) return LIBRARY_FUNCTION_ERROR;

    auto del = [&](char * s) { libdata->UTF8String_disown(s); };
    std::unique_ptr<char, decltype(del)> filename(MArgument_getUTF8String(args[0]), del);
    if(!filename) { return LIBRARY_FUNCTION_ERROR; }

    const mbool addp = MArgument_getBoolean(args[1]);

    // Calculation
    try
    {
        auto beg = qmml::parsing_file_iterator(std::string(filename.get())); auto end = qmml::parsing_file_iterator();
        std::vector<qmml::extxyz_data> res = qmml::import_extxyz(beg, end, addp);

        s.clear();  // previous invocations
        _format_extxyz_data(res, s, addp);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setUTF8String(res, const_cast<char*>(s.c_str()));
    return LIBRARY_NO_ERROR;
}

//
//  Imports molecules in extended XYZ format from string
//
//  Input:
//    [0] UTF8String  Input to parse
//    [1] Boolean  Whether molecules have additional properties block
//  Output:
//    [0] String  Mathematica expression of results data structure
//

EXTERN_C DLLEXPORT int qmml_import_extxyz2(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    static std::string s;  // Lifetime beyond function call is necessary because Mathematica needs to access the result string.

    // Arguments processing
    if( argc != 2 ) return LIBRARY_FUNCTION_ERROR;

    auto del = [&](char * s) { libdata->UTF8String_disown(s); };
    std::unique_ptr<char, decltype(del)> input(MArgument_getUTF8String(args[0]), del);
    if(!input) { return LIBRARY_FUNCTION_ERROR; }
    const size_t input_len = std::strlen(input.get());

    const mbool addp = MArgument_getBoolean(args[1]);

    // Calculation
    try
    {
        std::vector<qmml::extxyz_data> res = qmml::import_extxyz(input.get(), input.get()+input_len, addp);

        s.clear();  // previous invocations
        _format_extxyz_data(res, s, addp);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setUTF8String(res, const_cast<char*>(s.c_str()));
    return LIBRARY_NO_ERROR;
}
