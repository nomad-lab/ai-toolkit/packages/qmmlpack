// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Mathematica bindings

#include "mathematica.hpp"

DLLEXPORT mint WolframLibrary_getVersion() { return WolframLibraryVersion; }

DLLEXPORT int WolframLibrary_initialize(WolframLibraryData libData) { return 0; }
