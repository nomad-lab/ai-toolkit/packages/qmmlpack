// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Mathematica bindings for regression.

#include "mathematica.hpp"
#include "qmmlpack/numerics.hpp"

//  //////////////////
//  //  Symmetrize  //
//  //////////////////

//
//  Symmetrizes quadratic matrix
//
//  Input:
//    [0] {Real,2} n x n matrix M
//  Output:
//    [0] {Real,2} symmetrized matrix M
//

EXTERN_C DLLEXPORT int qmml_symmetrize(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 1 ) { libdata->Message("qmml_symmetrize: wrong number of arguments"); return LIBRARY_FUNCTION_ERROR; }

    MTensor M = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(M) != 2 ) { libdata->Message("qmml_symmetrize: input matrix has wrong rank"); return LIBRARY_RANK_ERROR; }
    mint const* Mdims = libdata->MTensor_getDimensions(M);
    const mint n = Mdims[0];
    if( Mdims[1] != n ) { libdata->Message("qmml_symmetrize: input matrix has wrong dimensions"); return LIBRARY_DIMENSION_ERROR; }

    MTensor S;
    const mint Sdims[2] = {n, n};
    int err; if( (err = libdata->MTensor_new(MType_Real, 2, Sdims, &S)) ) { libdata->Message("qmml_symmetrize: creation of result matrix failed"); return err; }

    try
    {
        mreal * m_data = libdata->MTensor_getRealData(M);
        mreal * s_data = libdata->MTensor_getRealData(S);
        std::memcpy(s_data, m_data, n*n*sizeof(mreal));
        qmml::symmetrize(s_data, n);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, S);
    return LIBRARY_NO_ERROR;
}


//  ///////////////////////////
//  //  LowerTriangularPart  //
//  ///////////////////////////

//
//  Returns vector containing the lower triangular part of a matrix
//
//  m_{i,j} for j <= i + k
//
//  Input:
//    [0] {Real,2,"Constant"} n x m matrix M
//    [1] {Integer,0} Diagonal of M that acts as upper right border
//  Output:
//    [0] {Real,1} result vector
//

EXTERN_C DLLEXPORT int qmml_lower_triangular_part(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 2 ) { libdata->Message("qmml_lower_triangular_part: wrong number of arguments"); return LIBRARY_FUNCTION_ERROR; }

    MTensor M = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(M) != 2 ) { libdata->Message("qmml_lower_triangular_part: input matrix has wrong rank"); return LIBRARY_RANK_ERROR; }
    mint const* Mdims = libdata->MTensor_getDimensions(M);
    const mint n = Mdims[0];
    const mint m = Mdims[1];

    const int k = MArgument_getInteger(args[1]);

    MTensor v;
    const mint vdims[1] = {static_cast<mint>(qmml::lower_triangular_part_nelems(n, m, k))};  // potentially narrows
    int err; if( (err = libdata->MTensor_new(MType_Real, 1, vdims, &v)) ) { libdata->Message("qmml_lower_triangular_part: creation of result vector failed"); return err; }

    try
    {
        qmml::lower_triangular_part(libdata->MTensor_getRealData(v), libdata->MTensor_getRealData(M), n, m, k);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, v);
    return LIBRARY_NO_ERROR;
}


//  /////////////////////////////////////////
//  //  Forward and backward substitution  //
//  /////////////////////////////////////////

//
// Solves linear system of equations given by lower triangular matrix and right hand side vector.
//
// L . x = b
//
// Input:
//   [0] {Real,2,"Constant"} n x n lower triangular matrix L.
//   [1] {Real,1,"Constant"} n right hand side vector b.
// Output:
//   [0] {Real,1} n result vector x.
//
// Remarks:
// - Main diagonal of L must not contain 0 (or values close to 0).
//

EXTERN_C DLLEXPORT int qmml_forward_substitution_v(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 2 ) return LIBRARY_FUNCTION_ERROR;

    MTensor L = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(L) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Ldims = libdata->MTensor_getDimensions(L);
    const mint n = Ldims[0];
    if( Ldims[1] != n ) return LIBRARY_DIMENSION_ERROR;

    MTensor b = MArgument_getMTensor(args[1]);
    if( libdata->MTensor_getRank(b) != 1 ) return LIBRARY_RANK_ERROR;
    mint const* bdims = libdata->MTensor_getDimensions(b);
    if( bdims[0] != n ) return LIBRARY_DIMENSION_ERROR;

    MTensor x;
    const mint xdims[1] { n };
    int err; if( ( err = libdata->MTensor_new(MType_Real, 1, xdims, &x) ) ) return err;

    // Calculation
    try
    {
        qmml::forward_substitution_v(libdata->MTensor_getRealData(x), libdata->MTensor_getRealData(L), libdata->MTensor_getRealData(b), n);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, x);
    return LIBRARY_NO_ERROR;
}

//
// Solves linear system of equations given by lower triangular matrix and right hand side matrix.
//
// L . X = B
//
// Input:
//   [0] {Real,2,"Constant"} n x n lower triangular matrix L.
//   [1] {Real,2,"Constant"} n x m right hand side matrix B.
// Output:
//   [0] {Real,2} n x m result matrix X.
//
// Remarks:
// - Main diagonal of L must not contain 0 (or values close to 0).
//

EXTERN_C DLLEXPORT int qmml_forward_substitution_m(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 2 ) return LIBRARY_FUNCTION_ERROR;

    MTensor L = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(L) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Ldims = libdata->MTensor_getDimensions(L);
    const mint n = Ldims[0];
    if( Ldims[1] != n ) return LIBRARY_DIMENSION_ERROR;

    MTensor B = MArgument_getMTensor(args[1]);
    if( libdata->MTensor_getRank(B) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Bdims = libdata->MTensor_getDimensions(B);
    const mint m = Bdims[1];
    if( Bdims[0] != n ) return LIBRARY_DIMENSION_ERROR;

    MTensor X;
    const mint Xdims[2] { n, m };
    int err; if( ( err = libdata->MTensor_new(MType_Real, 2, Xdims, &X) ) ) return err;

    // Calculation
    try
    {
        qmml::forward_substitution_m(libdata->MTensor_getRealData(X), libdata->MTensor_getRealData(L), libdata->MTensor_getRealData(B), n, m);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, X);
    return LIBRARY_NO_ERROR;
}

//
// Solves linear system of equations given by upper triangular matrix and right hand side vector.
//
// U . x = b
//
// Input:
//   [0] {Real,2,"Constant"} n x n upper triangular matrix U.
//   [1] {Real,1,"Constant"} n right hand side vector b.
// Output:
//   [0] {Real,1} n result vector x.
//
// Remarks:
// - Main diagonal of U must not contain 0 (or values close to 0).
//

EXTERN_C DLLEXPORT int qmml_backward_substitution_v(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 2 ) return LIBRARY_FUNCTION_ERROR;

    MTensor U = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(U) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Udims = libdata->MTensor_getDimensions(U);
    const mint n = Udims[0];
    if( Udims[1] != n ) return LIBRARY_DIMENSION_ERROR;

    MTensor b = MArgument_getMTensor(args[1]);
    if( libdata->MTensor_getRank(b) != 1 ) return LIBRARY_RANK_ERROR;
    mint const* bdims = libdata->MTensor_getDimensions(b);
    if( bdims[0] != n ) return LIBRARY_DIMENSION_ERROR;

    MTensor x;
    const mint xdims[1] { n };
    int err; if( ( err = libdata->MTensor_new(MType_Real, 1, xdims, &x) ) ) return err;

    // Calculation
    try
    {
        qmml::backward_substitution_v(libdata->MTensor_getRealData(x), libdata->MTensor_getRealData(U), libdata->MTensor_getRealData(b), n);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, x);
    return LIBRARY_NO_ERROR;
}

//
// Solves linear system of equations given by upper triangular matrix and right hand side matrix.
//
// U . X = B
//
// Input:
//   [0] {Real,2,"Constant"} n x n upper triangular matrix U.
//   [1] {Real,2,"Constant"} n x m right hand side matrix B.
// Output:
//   [0] {Real,2} n x m result matrix X.
//
// Remarks:
// - Main diagonal of U must not contain 0 (or values close to 0).
//

EXTERN_C DLLEXPORT int qmml_backward_substitution_m(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 2 ) return LIBRARY_FUNCTION_ERROR;

    MTensor U = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(U) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Udims = libdata->MTensor_getDimensions(U);
    const mint n = Udims[0];
    if( Udims[1] != n ) return LIBRARY_DIMENSION_ERROR;

    MTensor B = MArgument_getMTensor(args[1]);
    if( libdata->MTensor_getRank(B) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Bdims = libdata->MTensor_getDimensions(B);
    const mint m = Bdims[1];
    if( Bdims[0] != n ) return LIBRARY_DIMENSION_ERROR;

    MTensor X;
    const mint Xdims[2] { n, m };
    int err; if( ( err = libdata->MTensor_new(MType_Real, 2, Xdims, &X) ) ) return err;

    // Calculation
    try
    {
        qmml::backward_substitution_m(libdata->MTensor_getRealData(X), libdata->MTensor_getRealData(U), libdata->MTensor_getRealData(B), n, m);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, X);
    return LIBRARY_NO_ERROR;
}

//
// Does forward and backward substitution in one pass to solve linear system of equations given by lower triangular matrix and right hand side vector.
//
// L . L^T . x = b
//
// Input:
//   [0] {Real,2,"Constant"} n x n lower triangular matrix L.
//   [1] {Real,1,"Constant"} n right hand side vector b.
// Output:
//   [0] {Real,1} n result vector x.
//
// Remarks:
// - Main diagonal of L must not contain 0 (or values close to 0).
//

EXTERN_C DLLEXPORT int qmml_forward_backward_substitution_v(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 2 ) return LIBRARY_FUNCTION_ERROR;

    MTensor L = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(L) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Ldims = libdata->MTensor_getDimensions(L);
    const mint n = Ldims[0];
    if( Ldims[1] != n ) return LIBRARY_DIMENSION_ERROR;

    MTensor b = MArgument_getMTensor(args[1]);
    if( libdata->MTensor_getRank(b) != 1 ) return LIBRARY_RANK_ERROR;
    mint const* bdims = libdata->MTensor_getDimensions(b);
    if( bdims[0] != n ) return LIBRARY_DIMENSION_ERROR;

    MTensor x;
    const mint xdims[1] { n };
    int err; if( ( err = libdata->MTensor_new(MType_Real, 1, xdims, &x) ) ) return err;

    // Calculation
    try
    {
        qmml::forward_backward_substitution_v(libdata->MTensor_getRealData(x), libdata->MTensor_getRealData(L), libdata->MTensor_getRealData(b), n);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, x);
    return LIBRARY_NO_ERROR;
}

//
// Does backward and forward substitution in one pass to solve linear system of equations given by upper triangular matrix and right hand side vector.
//
// U^T . U . x = b
//
// Input:
//   [0] {Real,2,"Constant"} n x n upper triangular matrix U.
//   [1] {Real,1,"Constant"} n right hand side vector b.
// Output:
//   [0] {Real,1} n result vector x.
//
// Remarks:
// - Main diagonal of U must not contain 0 (or values close to 0).
//

EXTERN_C DLLEXPORT int qmml_backward_forward_substitution_v(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 2 ) return LIBRARY_FUNCTION_ERROR;

    MTensor U = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(U) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Udims = libdata->MTensor_getDimensions(U);
    const mint n = Udims[0];
    if( Udims[1] != n ) return LIBRARY_DIMENSION_ERROR;

    MTensor b = MArgument_getMTensor(args[1]);
    if( libdata->MTensor_getRank(b) != 1 ) return LIBRARY_RANK_ERROR;
    mint const* bdims = libdata->MTensor_getDimensions(b);
    if( bdims[0] != n ) return LIBRARY_DIMENSION_ERROR;

    MTensor x;
    const mint xdims[1] { n };
    int err; if( ( err = libdata->MTensor_new(MType_Real, 1, xdims, &x) ) ) return err;

    // Calculation
    try
    {
        qmml::backward_forward_substitution_v(libdata->MTensor_getRealData(x), libdata->MTensor_getRealData(U), libdata->MTensor_getRealData(b), n);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, x);
    return LIBRARY_NO_ERROR;
}
