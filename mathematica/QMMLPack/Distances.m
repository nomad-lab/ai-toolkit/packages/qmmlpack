(* ::Package:: *)

(*  qmmlpack                     *)
(*  Matthias Rupp, 2006-2016.    *)
(*  See LICENSE.txt for license. *)

(* Numerical routines *)

BeginPackage["QMMLPack`", {"LibraryLink`"}]


(*  ******************************  *)
(*  *  One-norm distance matrix  *  *)
(*  ******************************  *)

If[Not[ValueQ[DistanceOneNorm::usage]], DistanceOneNorm::usage =
"DistanceOneNorm[X] returns matrix of 1-norm distances between rows of X.
DistanceOneNorm[X, Z] returns 1-norm distances between rows of X and Z."];

Begin["`Private`"]

distanceOneNormK = LibraryFunctionLoad[libQMMLPack, "qmml_distance_matrix_one_norm_k", {{Real, 2, "Constant"}}, {Real, 2}];
distanceOneNormL = LibraryFunctionLoad[libQMMLPack, "qmml_distance_matrix_one_norm_l", {{Real, 2, "Constant"}, {Real, 2, "Constant"}}, {Real, 2}];

DistanceOneNorm[X_?(MatrixQ[#, NumericQ] &)] := distanceOneNormK[X];
DistanceOneNorm[X_?(MatrixQ[#, NumericQ] &), Z_?(MatrixQ[#, NumericQ] &)] := distanceOneNormL[X, Z];

End[]


(*  *******************************  *)
(*  *  Euclidean distance matrix  *  *)
(*  *******************************  *)

If[Not[ValueQ[DistanceEuclidean::usage]], DistanceEuclidean::usage =
"DistanceEuclidean[X] returns matrix of Euclidean distances between rows of X.
DistanceEuclidean[X, Z] returns Euclidean distances between rows of X and Z."];

Begin["`Private`"]

distanceEuclideanK = LibraryFunctionLoad[libQMMLPack, "qmml_distance_matrix_euclidean_k", {{Real, 2, "Constant"}}, {Real, 2}];
distanceEuclideanL = LibraryFunctionLoad[libQMMLPack, "qmml_distance_matrix_euclidean_l", {{Real, 2, "Constant"}, {Real, 2, "Constant"}}, {Real, 2}];

DistanceEuclidean[X_?(MatrixQ[#, NumericQ] &)] := distanceEuclideanK[X];
DistanceEuclidean[X_?(MatrixQ[#, NumericQ] &), Z_?(MatrixQ[#, NumericQ] &)] := distanceEuclideanL[X, Z];

End[]


(*  ***************************************  *)
(*  *  Squared Euclidean distance matrix  *  *)
(*  ***************************************  *)

If[Not[ValueQ[DistanceSquaredEuclidean::usage]], DistanceSquaredEuclidean::usage =
"DistanceSquaredEuclidean[X] returns matrix of squared Euclidean distances between rows of X.
DistanceSquaredEuclidean[X, Z] returns squared Euclidean distances between rows of X and Z."];

Begin["`Private`"]

distanceSquaredEuclideanK = LibraryFunctionLoad[libQMMLPack, "qmml_distance_matrix_squared_euclidean_k", {{Real, 2, "Constant"}}, {Real, 2}];
distanceSquaredEuclideanL = LibraryFunctionLoad[libQMMLPack, "qmml_distance_matrix_squared_euclidean_l", {{Real, 2, "Constant"}, {Real, 2, "Constant"}}, {Real, 2}];

DistanceSquaredEuclidean[X_?(MatrixQ[#, NumericQ] &)] := distanceSquaredEuclideanK[X];
DistanceSquaredEuclidean[X_?(MatrixQ[#, NumericQ] &), Z_?(MatrixQ[#, NumericQ] &)] := distanceSquaredEuclideanL[X, Z];

End[]


EndPackage[]
