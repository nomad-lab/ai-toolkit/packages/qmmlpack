// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Unit tests for distances

#include "catch/catch.hpp"
#include "qmmlpack/distances_pnorm.hpp"
#include "util.hpp"

#include <cmath>

using namespace qmml;

TEST_CASE( "One-norm distance matrix", "[distances]" )
{
    SECTION( "simple test cases" )
    {
        // n = 1, d = 1
        {
            const double xx[1][1] = { { 0 } };
            const double zz[1][1] = { { 1 } };
            double dd[1][1];

            const double rk[1][1] = { { 0 } };
            distance_matrix_one_norm_k(&dd[0][0], &xx[0][0], 1, 1);
            CHECK( equal_abs(&dd[0][0], &rk[0][0], 1) );

            const double rl[1][1] = { { 1 } };
            distance_matrix_one_norm_l(&dd[0][0], &xx[0][0], &zz[0][0], 1, 1, 1);
            CHECK( equal_abs(&dd[0][0], &rl[0][0], 1) );
        }

        // n = 1, d = 2
        {
            const double xx[1][2] = { { 0,  1 } };
            const double zz[1][2] = { { 1, -3 } };
            double dd[1][1];

            const double rk[1][1] = { { 0 } };
            distance_matrix_one_norm_k(&dd[0][0], &xx[0][0], 1, 2);
            CHECK( equal_abs(&dd[0][0], &rk[0][0], 1) );

            const double rl[1][1] = { { 5 } };
            distance_matrix_one_norm_l(&dd[0][0], &xx[0][0], &zz[0][0], 1, 1, 2);
            CHECK( equal_abs(&dd[0][0], &rl[0][0], 1) );
        }

        // n = 2, m = 3, d = 2
        {
            const double xx[2][2] = { {  0, 1 }, { 1, -3} };
            const double zz[3][2] = { { -1, 2 }, { 4,  5}, { -2, -3 } };

            const double rk[2][2] = { { 0, 5 }, { 5, 0} };
            double dk[2][2];
            distance_matrix_one_norm_k(&dk[0][0], &xx[0][0], 2, 2);
            CHECK( equal_abs(&dk[0][0], &rk[0][0], 2*2) );

            const double rl[2][3] = { { 2, 8, 6 }, { 7, 11, 3 } };
            double dl[2][3];
            distance_matrix_one_norm_l(&dl[0][0], &xx[0][0], &zz[0][0], 2, 3, 2);
            CHECK( equal_abs(&dl[0][0], &rl[0][0], 2*3) );
        }

        // n = 51, m = 52, d = 101; triggers code path for larger inputs
        {
            const size_t n = 51; const size_t m = 52; const size_t d = 101;
            double *xx = new double[n*d], *zz = new double[m*d], *rk = new double[n*n], *rl = new double[n*m], *dk = new double[n*n], *dl = new double[n*m];
            for(size_t i = 0; i < n; ++i) for(size_t k = 0; k < d; ++k) xx[i*d+k] = i+k;
            for(size_t i = 0; i < m; ++i) for(size_t k = 0; k < d; ++k) zz[i*d+k] = i+k-1.; // need double 1 as i+k is unsigned int
            for(size_t i = 0; i < n; ++i) for(size_t j = 0; j < n; ++j) rk[i*n+j] = (i >= j ? i-j : j-i)*d;
            for(size_t i = 0; i < n; ++i) for(size_t j = 0; j < m; ++j) rl[i*m+j] = (i + 1 >= j ? (i+1)-j : j-i-1)*d;

            distance_matrix_one_norm_k(dk, xx, n, d);
            CHECK( equal_abs(dk, rk, n*n) );

            distance_matrix_one_norm_l(dl, xx, zz, n, m, d);
            CHECK( equal_abs(dl, rl, n*m) );

            delete[] xx; delete[] zz; delete[] rk; delete[] rl; delete[] dk; delete[] dl;
        }
    }
}

TEST_CASE( "Euclidean distance matrix", "[distances]")
{
    SECTION( "simple test cases" )
    {
        // n = 1, d = 1
        {
            const double x[1][1] = { { 1 } };
            const double z[1][1] = { { 3 } };
            double d[1][1], r[1][1];

            distance_matrix_euclidean_k(&d[0][0], &x[0][0], 1, 1); r[0][0] = 0;
            CHECK( equal_abs(&d[0][0], &r[0][0], 1) );

            distance_matrix_euclidean_l(&d[0][0], &x[0][0], &z[0][0], 1, 1, 1); r[0][0] = 2;
            CHECK( equal_abs(&d[0][0], &r[0][0], 1) );
        }

        // n = 1, d = 2
        {
            const double x[1][2] = { { 0, 1 } };
            const double z[1][2] = { { 1,-3 } };
            double d[1][1], r[1][1];

            distance_matrix_euclidean_k(&d[0][0], &x[0][0], 1, 1); r[0][0] = 0;
            CHECK( equal_abs(&d[0][0], &r[0][0], 1) );

            distance_matrix_euclidean_l(&d[0][0], &x[0][0], &z[0][0], 1, 1, 2); r[0][0] = std::sqrt(17);
            CHECK( equal_abs(&d[0][0], &r[0][0], 1) );
        }

        // n = 2, m = 3, d = 2
        {
            const double x[2][2] = { {  0, 1 }, { 1,-3} };
            const double z[3][2] = { { -1, 2 }, { 4, 5}, {-2,-3} };
            const double r1[2][2] = { { 0, std::sqrt(17) }, { std::sqrt(17), 0} };
            const double r2[2][3] = { { std::sqrt(2), std::sqrt(32), std::sqrt(20) }, { std::sqrt(29), std::sqrt(73), std::sqrt(9) } };
            double d1[2][2], d2[2][3];

            distance_matrix_euclidean_k(&d1[0][0], &x[0][0], 2, 2);
            CHECK( equal_abs(&d1[0][0], &r1[0][0], 2*2) );

            distance_matrix_euclidean_l(&d2[0][0], &x[0][0], &z[0][0], 2, 3, 2);
            CHECK( equal_abs(&d2[0][0], &r2[0][0], 2*3) );
        }
    }
}

TEST_CASE( "Squared Euclidean distance matrix", "[distances]" )
{
    SECTION( "simple test cases" )
    {
        // n = 1, d = 1
        {
            const double x[1][1] = { { 0 } };
            const double z[1][1] = { { 1 } };
            double d[1][1], r[1][1];

            distance_matrix_squared_euclidean_k(&d[0][0], &x[0][0], 1, 1); r[0][0] = 0;
            CHECK( equal_abs(&d[0][0], &r[0][0], 1) );

            distance_matrix_squared_euclidean_l(&d[0][0], &x[0][0], &z[0][0], 1, 1, 1); r[0][0] = 1;
            CHECK( equal_abs(&d[0][0], &r[0][0], 1) );
        }

        // n = 1, d = 2
        {
            const double x[1][2] = { { 0, 1 } };
            const double z[1][2] = { { 1,-3 } };
            double d[1][1], r[1][1];

            distance_matrix_squared_euclidean_k(&d[0][0], &x[0][0], 1, 1); r[0][0] = 0;
            CHECK( equal_abs(&d[0][0], &r[0][0], 1) );

            distance_matrix_squared_euclidean_l(&d[0][0], &x[0][0], &z[0][0], 1, 1, 2); r[0][0] = 17;
            CHECK( equal_abs(&d[0][0], &r[0][0], 1) );
        }

        // n = 2, m = 3, d = 2
        {
            const double x[2][2] = { {  0, 1 }, { 1,-3} };
            const double z[3][2] = { { -1, 2 }, { 4, 5}, {-2,-3} };
            const double r1[2][2] = { { 0, 17 }, { 17, 0} };
            const double r2[2][3] = { { 2, 32, 20 }, { 29, 73, 9 } };
            double d1[2][2], d2[2][3];

            distance_matrix_squared_euclidean_k(&d1[0][0], &x[0][0], 2, 2);
            CHECK( equal_abs(&d1[0][0], &r1[0][0], 2*2) );

            distance_matrix_squared_euclidean_l(&d2[0][0], &x[0][0], &z[0][0], 2, 3, 2);
            CHECK( equal_abs(&d2[0][0], &r2[0][0], 2*3) );
        }
    }
}
