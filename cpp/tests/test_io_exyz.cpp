// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Unit tests for import of atomistic systems in extended XYZ format

#include <cstdio>

#include "catch/catch.hpp"
#include "qmmlpack/io_extxyz.hpp"
#include "util.hpp"

using namespace qmml;

TEST_CASE("Parsing extended XYZ format: atomic coordinates", "[parsing][extxyz]")
{
    SECTION("example 1")
    {
        const std::string s =
        "4                                      \n\
                                                \n\
         C  1       2.0     3.00000000001       \n\
         C -1      -2.0    -3.001               \n\
         C  1.2e-1 +1.2e-1 -1.2E-1              \n\
         C  0.       .0     0                   \n\
        ";

        const std::vector<std::array<double, 3>> res = {
            { 1.0 ,  2.0 ,  3.00000000001},
            {-1.0 , -2.0 , -3.001        },
            { 0.12,  0.12, -0.12         },
            { 0.  ,  0.  ,  0.           }
        };

        auto i = s.cbegin(); const extxyz_data exyz = parse_extxyz_entry(i, false);

        CHECK( exyz.xyz == res );
    }
}

TEST_CASE("Parsing extended XYZ format: molecular properties", "[parsing][extxyz]")
{
    SECTION("integer properties")
    {
        const std::string s = "1\n 123 1 2\t\t3\nC 1 2 3\n";
        auto i = s.cbegin(); const extxyz_data exyz = parse_extxyz_entry(i, false);

        CHECK( exyz.mp.size() == 4 );
        CHECK( exyz.mp[0].intv() == 123 );
        CHECK( exyz.mp[1].intv() ==   1 );
        CHECK( exyz.mp[2].intv() ==   2 );
        CHECK( exyz.mp[3].intv() ==   3 );
    }

    SECTION("floating point properties")
    {
        const std::string s = "1\n 1.23 -0.1E+1\nC 1 2 3\n";
        auto i = s.cbegin(); const extxyz_data exyz = parse_extxyz_entry(i, false);

        CHECK( exyz.mp.size() == 2 );
        CHECK( exyz.mp[0].floatv() == 1.23 );
        CHECK( exyz.mp[1].floatv() == -1.0 );
    }

    SECTION("string properties")
    {
        const std::string s = "1\n ABC\tdef\t\t string_token\nC 1 2 3\n";
        auto i = s.cbegin(); const extxyz_data exyz = parse_extxyz_entry(i, false);

        CHECK( exyz.mp.size() == 3 );
        CHECK( exyz.mp[0].stringv() == "ABC" );
        CHECK( exyz.mp[1].stringv() == "def" );
        CHECK( exyz.mp[2].stringv() == "string_token" );
    }

    SECTION("mixed properties")
    {
        const std::string s = "1\n ABC\t123\t\t 1.23\nC 1 2 3\n";
        auto i = s.cbegin(); const extxyz_data exyz = parse_extxyz_entry(i, false);

        CHECK( exyz.mp.size() == 3 );
        CHECK( exyz.mp[0].stringv() == "ABC" );
        CHECK( exyz.mp[1].intv()    ==   123 );
        CHECK( exyz.mp[2].floatv()  ==  1.23 );
    }
}

TEST_CASE("Parsing extended XYZ format: atomic properties", "[parsing][extxyz]")
{
    SECTION("string properties")
    {
        const std::string s = "2\n\nC 1 2 3 abc ABC\nC 1 2 3 def\n";
        auto i = s.cbegin(); const extxyz_data exyz = parse_extxyz_entry(i, false);

        CHECK( exyz.ap.size() == 2 );

        CHECK( exyz.ap[0].size() == 2 );
        CHECK( exyz.ap[0][0].stringv() == "abc" );
        CHECK( exyz.ap[0][1].stringv() == "ABC" );

        CHECK( exyz.ap[1].size() == 1 );
        CHECK( exyz.ap[1][0].stringv() == "def" );
    }

    SECTION("integer properties")
    {
        const std::string s = "2\n\nC 1 2 3 321\nB 1 2 3 123 -456\n";
        auto i = s.cbegin(); const extxyz_data exyz = parse_extxyz_entry(i, false);

        CHECK( exyz.ap.size() == 2 );

        CHECK( exyz.ap[0].size() == 1 );
        CHECK( exyz.ap[0][0].intv() ==  321 );

        CHECK( exyz.ap[1].size() == 2 );
        CHECK( exyz.ap[1][0].intv() ==  123 );
        CHECK( exyz.ap[1][1].intv() == -456 );
    }

    SECTION("floating point properties")
    {
        const std::string s = "2\n\nC 1 2 3 1.23 -1E-1 \nB 1 2 3 4.56 4e-1\n";
        auto i = s.cbegin(); const extxyz_data exyz = parse_extxyz_entry(i, false);

        CHECK( exyz.ap.size() == 2 );

        CHECK( exyz.ap[0].size() == 2 );
        CHECK( exyz.ap[0][0].floatv() == 1.23 );
        CHECK( exyz.ap[0][1].floatv() == -0.1 );

        CHECK( exyz.ap[1].size() == 2 );
        CHECK( exyz.ap[1][0].floatv() == 4.56 );
        CHECK( exyz.ap[1][1].floatv() ==  0.4 );
    }

    SECTION("mixed properties")
    {
        const std::string s = "2\n\nC 1 2 3 abc 1.23 \nB 1 2 3 456 ABC\n";
        auto i = s.cbegin(); const extxyz_data exyz = parse_extxyz_entry(i, false);

        CHECK( exyz.ap.size() == 2 );

        CHECK( exyz.ap[0].size() == 2 );
        CHECK( exyz.ap[0][0].stringv() == "abc" );
        CHECK( exyz.ap[0][1].floatv()  ==  1.23 );

        CHECK( exyz.ap[1].size() == 2 );
        CHECK( exyz.ap[1][0].intv()    ==   456 );
        CHECK( exyz.ap[1][1].stringv() == "ABC" );
    }
}

TEST_CASE("Parsing extended XYZ format: additional properties", "[parsing][extxyz]")
{
    SECTION("single line")
    {
        const std::string s = "1\n\nC 1 2 3\nabc 123\n \t\n";
        auto i = s.cbegin(); const extxyz_data exyz = parse_extxyz_entry(i, true);

        CHECK( exyz.addp.size() == 1 );

        CHECK( exyz.addp[0].size() == 2 );

        CHECK( exyz.addp[0][0].stringv() == "abc" );
        CHECK( exyz.addp[0][1].intv()    ==   123 );
    }

    SECTION("two lines")
    {
        const std::string s = "1\n\nC 1 2 3\nabc   \n \t\t 4.56\t \n\n";
        auto i = s.cbegin(); const extxyz_data exyz = parse_extxyz_entry(i, true);

        CHECK( exyz.addp.size() == 2 );

        CHECK( exyz.addp[0].size() == 1 );
        CHECK( exyz.addp[0][0].stringv() == "abc" );

        CHECK( exyz.addp[1].size() == 1 );
        CHECK( exyz.addp[1][0].floatv() == 4.56 );
    }

}

TEST_CASE("Parsing extended XYZ format: parsing_iterator", "[parsing][extxyz]")
{
    SECTION("std::string::const_iterator")
    {
        std::string s = "1234567890";
        auto beg = parsing_iterator<std::string::const_iterator>(s.cbegin(), s.cend());
        auto end = beg; ++end; ++end; ++end;
        std::string res = std::string(beg, end);
        CHECK( res == "123" );
    }

    SECTION("std::string::const_iterator, additional end-of-line")
    {
        std::string s = "123";
        auto beg = parsing_iterator<std::string::const_iterator>(s.cbegin(), s.cend());
        ++beg; ++beg; ++beg;
        CHECK( *beg == '\n' );
    }

    SECTION("char const*const")
    {
        char const*const s = "1234567890";
        auto beg = parsing_iterator<char const*>(s, s+10);
        auto end = beg; ++end; ++end; ++end;
        std::string res = std::string(beg, end);
        CHECK( res == "123" );
    }
}

TEST_CASE("Parsing extended XYZ format: multiple molecules", "[parsing][extxyz]")
{
    SECTION("single molecule, no additional properties")
    {
        const std::string s = "1\nA 1 1.1\nC 1 2 3 a\n";
        const std::vector<extxyz_data> res = import_extxyz(s.cbegin(), s.cend(), false);

        CHECK( res.size() == 1 );

        CHECK( res[0].an.size() == 1 );
        CHECK( res[0].an[0] == 6 );

        CHECK( res[0].xyz.size() == 1 );
        CHECK( res[0].xyz[0][0] == 1. ); CHECK( res[0].xyz[0][1] == 2. ); CHECK( res[0].xyz[0][2] == 3. );

        CHECK( res[0].mp.size() == 3 );
        CHECK( res[0].mp[0].stringv() ==  "A" );
        CHECK( res[0].mp[1].intv()    ==    1 );
        CHECK( res[0].mp[2].floatv()  ==  1.1 );

        CHECK( res[0].ap.size() == 1 );
        CHECK( res[0].ap[0].size() == 1 );
        CHECK( res[0].ap[0][0].stringv() == "a" );
    }

    SECTION("two molecules, missing end-of-line")
    {
        const std::string s = "1\nA 1 1.1\nC 1 2 3 a\n1 \nB 2 -2.2 \n7 0 0 0.";
        const std::vector<extxyz_data> res = import_extxyz(s.cbegin(), s.cend(), false);

        REQUIRE( res.size() == 2 );

        CHECK( res[0].an.size() == 1 );
        CHECK( res[0].an[0] == 6 );

        CHECK( res[0].xyz.size() == 1 );
        CHECK( res[0].xyz[0][0] == 1. ); CHECK( res[0].xyz[0][1] == 2. ); CHECK( res[0].xyz[0][2] == 3. );

        CHECK( res[0].mp.size() == 3 );
        CHECK( res[0].mp[0].stringv() ==  "A" );
        CHECK( res[0].mp[1].intv()    ==    1 );
        CHECK( res[0].mp[2].floatv()  ==  1.1 );

        CHECK( res[0].ap.size() == 1 );
        CHECK( res[0].ap[0].size() == 1 );
        CHECK( res[0].ap[0][0].stringv() == "a" );

        CHECK( res[1].an.size() == 1 );
        CHECK( res[1].an[0] == 7 );

        CHECK( res[1].xyz.size() == 1 );
        CHECK( res[1].xyz[0][0] == 0. ); CHECK( res[1].xyz[0][1] == 0. ); CHECK( res[1].xyz[0][2] == 0. );

        CHECK( res[1].mp.size() == 3 );
        CHECK( res[1].mp[0].stringv() ==  "B" );
        CHECK( res[1].mp[1].intv()    ==    2 );
        CHECK( res[1].mp[2].floatv()  == -2.2 );

        CHECK( res[1].ap.size() == 1 );
        CHECK( res[1].ap[0].size() == 0 );
    }

    SECTION("three molecules with additional properties")
    {
        const std::string s = "1\n\nC 1 2 3\nabc\n\n1\n\nC 1 2 3\n123 4.56\n\n1\n\nC 1 2 3\n\n";
        const std::vector<extxyz_data> res = import_extxyz(s.cbegin(), s.cend(), true);

        REQUIRE( res.size() == 3 );

        // molecule 1
        CHECK( res[0].an.size() == 1 );
        CHECK( res[0].an[0] == 6 );

        CHECK( res[0].xyz.size() == 1 );
        CHECK( res[0].xyz[0][0] == 1. ); CHECK( res[0].xyz[0][1] == 2. ); CHECK( res[0].xyz[0][2] == 3. );

        CHECK( res[0].mp.size() == 0 );
        CHECK( res[0].ap.size() == 1 );
        CHECK( res[0].ap[0].size() == 0 );

        CHECK( res[0].addp.size() == 1 );
        CHECK( res[0].addp[0].size() == 1 );
        CHECK( res[0].addp[0][0].stringv() == "abc" );

        // molecule 2
        CHECK( res[1].an.size() == 1 );
        CHECK( res[1].an[0] == 6 );

        CHECK( res[1].xyz.size() == 1 );
        CHECK( res[1].xyz[0][0] == 1. ); CHECK( res[1].xyz[0][1] == 2. ); CHECK( res[1].xyz[0][2] == 3. );

        CHECK( res[1].mp.size() == 0 );
        CHECK( res[1].ap.size() == 1 );
        CHECK( res[1].ap[0].size() == 0 );

        CHECK( res[1].addp.size() == 1 );
        CHECK( res[1].addp[0].size() == 2 );
        CHECK( res[1].addp[0][0].intv() == 123 );
        CHECK( res[1].addp[0][1].floatv() == 4.56 );

        // molecule 3
        CHECK( res[2].an.size() == 1 );
        CHECK( res[2].an[0] == 6 );

        CHECK( res[2].xyz.size() == 1 );
        CHECK( res[2].xyz[0][0] == 1. ); CHECK( res[2].xyz[0][1] == 2. ); CHECK( res[2].xyz[0][2] == 3. );

        CHECK( res[2].mp.size() == 0 );
        CHECK( res[2].ap.size() == 1 );
        CHECK( res[2].ap[0].size() == 0 );
        CHECK( res[2].addp.size() == 0 );
    }
}

TEST_CASE("Parsing extended XYZ format: file input", "[parsing][extxyz]")
{
    SECTION("two molecules, no additional properties")
    {
        const std::string s ("2                                  \n\
         ABC 123 1.23                           \n\
         C  1   2.0   3.0001  abc 123 1.23      \n\
         1 -1  -2.0  -3.001   xyz 321 32.1      \n\
         1                                      \n\
         \n\
         H 0 0 0");  // no missing eol
        temporary_file f(s);  // block scope
        const std::vector<extxyz_data> res = import_extxyz(parsing_file_iterator(f.filename(), 7, 6), parsing_file_iterator(), false);

        REQUIRE( res.size() == 2 );

        // molecule 1
        CHECK( res[0].an.size() == 2 );
        CHECK( res[0].an[0] == 6 ); CHECK( res[0].an[1] == 1 );

        CHECK( res[0].xyz.size() == 2 );
        CHECK( res[0].xyz[0][0] ==  1. ); CHECK( res[0].xyz[0][1] ==  2. ); CHECK( res[0].xyz[0][2] ==  3.0001 );
        CHECK( res[0].xyz[1][0] == -1. ); CHECK( res[0].xyz[1][1] == -2. ); CHECK( res[0].xyz[1][2] == -3.001 );

        CHECK( res[0].mp.size() == 3 );
        CHECK( res[0].mp[0].stringv() == "ABC" ); CHECK( res[0].mp[1].intv() == 123 ); CHECK( res[0].mp[2].floatv() == 1.23 );

        CHECK( res[0].ap.size() == 2 );
        CHECK( res[0].ap[0].size() == 3 );
        CHECK( res[0].ap[0][0].stringv() == "abc" ); CHECK( res[0].ap[0][1].intv() == 123 ); CHECK( res[0].ap[0][2].floatv() == 1.23 );
        CHECK( res[0].ap[1][0].stringv() == "xyz" ); CHECK( res[0].ap[1][1].intv() == 321 ); CHECK( res[0].ap[1][2].floatv() == 32.1 );

        // molecule 2
        CHECK( res[1].an.size() == 1 );
        CHECK( res[1].an[0] == 1 );

        CHECK( res[1].xyz.size() == 1 );
        CHECK( res[1].xyz[0][0] ==  0. ); CHECK( res[1].xyz[0][1] == 0. ); CHECK( res[1].xyz[0][2] == 0. );

        CHECK( res[1].mp.size() == 0 );

        CHECK( res[1].ap.size() == 1 );
        CHECK( res[1].ap[0].size() == 0 );
    }

    SECTION("two molecules, no additional properties, large buffer size")
    {
        const std::string s ("2                                  \n\
         ABC 123 1.23                           \n\
         C  1   2.0   3.0001  abc 123 1.23      \n\
         1 -1  -2.0  -3.001   xyz 321 32.1      \n\
         1                                      \n\
         \n\
         H 0 0 0");  // no missing eol
        temporary_file f(s);  // block scope
        const std::vector<extxyz_data> res = import_extxyz(parsing_file_iterator(f.filename()), parsing_file_iterator(), false);

        REQUIRE( res.size() == 2 );

        // molecule 1
        CHECK( res[0].an.size() == 2 );
        CHECK( res[0].an[0] == 6 ); CHECK( res[0].an[1] == 1 );

        CHECK( res[0].xyz.size() == 2 );
        CHECK( res[0].xyz[0][0] ==  1. ); CHECK( res[0].xyz[0][1] ==  2. ); CHECK( res[0].xyz[0][2] ==  3.0001 );
        CHECK( res[0].xyz[1][0] == -1. ); CHECK( res[0].xyz[1][1] == -2. ); CHECK( res[0].xyz[1][2] == -3.001 );

        CHECK( res[0].mp.size() == 3 );
        CHECK( res[0].mp[0].stringv() == "ABC" ); CHECK( res[0].mp[1].intv() == 123 ); CHECK( res[0].mp[2].floatv() == 1.23 );

        CHECK( res[0].ap.size() == 2 );
        CHECK( res[0].ap[0].size() == 3 );
        CHECK( res[0].ap[0][0].stringv() == "abc" ); CHECK( res[0].ap[0][1].intv() == 123 ); CHECK( res[0].ap[0][2].floatv() == 1.23 );
        CHECK( res[0].ap[1][0].stringv() == "xyz" ); CHECK( res[0].ap[1][1].intv() == 321 ); CHECK( res[0].ap[1][2].floatv() == 32.1 );

        // molecule 2
        CHECK( res[1].an.size() == 1 );
        CHECK( res[1].an[0] == 1 );

        CHECK( res[1].xyz.size() == 1 );
        CHECK( res[1].xyz[0][0] ==  0. ); CHECK( res[1].xyz[0][1] == 0. ); CHECK( res[1].xyz[0][2] == 0. );

        CHECK( res[1].mp.size() == 0 );

        CHECK( res[1].ap.size() == 1 );
        CHECK( res[1].ap[0].size() == 0 );
    }
}

TEST_CASE("Parsing extended XYZ format: real examples", "[parsing][extxyz]")
{
    SECTION("GDB, 20 atoms, SMILES name, only molecular properties")
    {
        const std::string s {
            "20\n"
            "BrC1=C2C(NS(=O)(=O)C=O)=NC=C2N=CS1 -113.36526027 -110815.79710819  1.62920700\n"
            "Br     8.28727192 -1.87818170 -1.03527287\n"
            "C      7.84447149 -0.52267466  0.16918073\n"
            "C      6.60366591 -0.01061410  0.27137627\n"
            "C      5.37225676 -0.25921457 -0.40306449\n"
            "N      5.08109704 -1.11070010 -1.41941117\n"
            "S      3.49722337 -1.04418756 -1.99267430\n"
            "O      3.34983754  0.11392758 -2.82190143\n"
            "O      3.25139252 -2.36616711 -2.49128243\n"
            "C      2.54903665 -0.69621812 -0.40876360\n"
            "O      2.42159884 -1.53910720  0.41972944\n"
            "N      4.41625235  0.51049901  0.08247665\n"
            "C      4.93276207  1.30202660  1.07080308\n"
            "C      6.26472972  1.04218016  1.24823242\n"
            "N      7.13671808  1.58087335  2.11334178\n"
            "C      8.35782690  1.22160102  2.14896446\n"
            "S      9.14596598 -0.00003131  1.13854089\n"
            "H      5.58578128 -1.99048572 -1.54429250\n"
            "H      1.99118254  0.25861298 -0.52514930\n"
            "H      4.31548467  2.01531007  1.61401907\n"
            "H      9.06678291  1.66660195  2.85643576"
        };

        const std::vector<extxyz_data> res = import_extxyz(s.cbegin(), s.cend(), false);

        REQUIRE( res.size() == 1 );

        CHECK( res[0].mp[1].floatv() == -113.36526027 );
    }
}
