// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Unit tests for parsing

#include <cstdio>

#include "catch/catch.hpp"
#include "qmmlpack/io_parse.hpp"
#include "util.hpp"

using namespace qmml;

TEST_CASE("Parsing element species", "[parsing]")
{
    SECTION("Element abbreviations")
    {
        char const* c;

        c = "H   "; CHECK( parse_element(c) ==   1 );
        c = "He  "; CHECK( parse_element(c) ==   2 );
        c = "Li  "; CHECK( parse_element(c) ==   3 );
        c = "Be  "; CHECK( parse_element(c) ==   4 );
        c = "B   "; CHECK( parse_element(c) ==   5 );
        c = "C   "; CHECK( parse_element(c) ==   6 );
        c = "N   "; CHECK( parse_element(c) ==   7 );
        c = "O   "; CHECK( parse_element(c) ==   8 );
        c = "F   "; CHECK( parse_element(c) ==   9 );
        c = "Ne  "; CHECK( parse_element(c) ==  10 );
        c = "Na  "; CHECK( parse_element(c) ==  11 );
        c = "Mg  "; CHECK( parse_element(c) ==  12 );
        c = "Al  "; CHECK( parse_element(c) ==  13 );
        c = "Si  "; CHECK( parse_element(c) ==  14 );
        c = "P   "; CHECK( parse_element(c) ==  15 );
        c = "S   "; CHECK( parse_element(c) ==  16 );
        c = "Cl  "; CHECK( parse_element(c) ==  17 );
        c = "Ar  "; CHECK( parse_element(c) ==  18 );
        c = "K   "; CHECK( parse_element(c) ==  19 );
        c = "Ca  "; CHECK( parse_element(c) ==  20 );
        c = "Sc  "; CHECK( parse_element(c) ==  21 );
        c = "Ti  "; CHECK( parse_element(c) ==  22 );
        c = "V   "; CHECK( parse_element(c) ==  23 );
        c = "Cr  "; CHECK( parse_element(c) ==  24 );
        c = "Mn  "; CHECK( parse_element(c) ==  25 );
        c = "Fe  "; CHECK( parse_element(c) ==  26 );
        c = "Co  "; CHECK( parse_element(c) ==  27 );
        c = "Ni  "; CHECK( parse_element(c) ==  28 );
        c = "Cu  "; CHECK( parse_element(c) ==  29 );
        c = "Zn  "; CHECK( parse_element(c) ==  30 );
        c = "Ga  "; CHECK( parse_element(c) ==  31 );
        c = "Ge  "; CHECK( parse_element(c) ==  32 );
        c = "As  "; CHECK( parse_element(c) ==  33 );
        c = "Se  "; CHECK( parse_element(c) ==  34 );
        c = "Br  "; CHECK( parse_element(c) ==  35 );
        c = "Kr  "; CHECK( parse_element(c) ==  36 );
        c = "Rb  "; CHECK( parse_element(c) ==  37 );
        c = "Sr  "; CHECK( parse_element(c) ==  38 );
        c = "Y   "; CHECK( parse_element(c) ==  39 );
        c = "Zr  "; CHECK( parse_element(c) ==  40 );
        c = "Nb  "; CHECK( parse_element(c) ==  41 );
        c = "Mo  "; CHECK( parse_element(c) ==  42 );
        c = "Tc  "; CHECK( parse_element(c) ==  43 );
        c = "Ru  "; CHECK( parse_element(c) ==  44 );
        c = "Rh  "; CHECK( parse_element(c) ==  45 );
        c = "Pd  "; CHECK( parse_element(c) ==  46 );
        c = "Ag  "; CHECK( parse_element(c) ==  47 );
        c = "Cd  "; CHECK( parse_element(c) ==  48 );
        c = "In  "; CHECK( parse_element(c) ==  49 );
        c = "Sn  "; CHECK( parse_element(c) ==  50 );
        c = "Sb  "; CHECK( parse_element(c) ==  51 );
        c = "Te  "; CHECK( parse_element(c) ==  52 );
        c = "I   "; CHECK( parse_element(c) ==  53 );
        c = "Xe  "; CHECK( parse_element(c) ==  54 );
        c = "Cs  "; CHECK( parse_element(c) ==  55 );
        c = "Ba  "; CHECK( parse_element(c) ==  56 );
        c = "La  "; CHECK( parse_element(c) ==  57 );
        c = "Ce  "; CHECK( parse_element(c) ==  58 );
        c = "Pr  "; CHECK( parse_element(c) ==  59 );
        c = "Nd  "; CHECK( parse_element(c) ==  60 );
        c = "Pm  "; CHECK( parse_element(c) ==  61 );
        c = "Sm  "; CHECK( parse_element(c) ==  62 );
        c = "Eu  "; CHECK( parse_element(c) ==  63 );
        c = "Gd  "; CHECK( parse_element(c) ==  64 );
        c = "Tb  "; CHECK( parse_element(c) ==  65 );
        c = "Dy  "; CHECK( parse_element(c) ==  66 );
        c = "Ho  "; CHECK( parse_element(c) ==  67 );
        c = "Er  "; CHECK( parse_element(c) ==  68 );
        c = "Tm  "; CHECK( parse_element(c) ==  69 );
        c = "Yb  "; CHECK( parse_element(c) ==  70 );
        c = "Lu  "; CHECK( parse_element(c) ==  71 );
        c = "Hf  "; CHECK( parse_element(c) ==  72 );
        c = "Ta  "; CHECK( parse_element(c) ==  73 );
        c = "W   "; CHECK( parse_element(c) ==  74 );
        c = "Re  "; CHECK( parse_element(c) ==  75 );
        c = "Os  "; CHECK( parse_element(c) ==  76 );
        c = "Ir  "; CHECK( parse_element(c) ==  77 );
        c = "Pt  "; CHECK( parse_element(c) ==  78 );
        c = "Au  "; CHECK( parse_element(c) ==  79 );
        c = "Hg  "; CHECK( parse_element(c) ==  80 );
        c = "Tl  "; CHECK( parse_element(c) ==  81 );
        c = "Pb  "; CHECK( parse_element(c) ==  82 );
        c = "Bi  "; CHECK( parse_element(c) ==  83 );
        c = "Po  "; CHECK( parse_element(c) ==  84 );
        c = "At  "; CHECK( parse_element(c) ==  85 );
        c = "Rn  "; CHECK( parse_element(c) ==  86 );
        c = "Fr  "; CHECK( parse_element(c) ==  87 );
        c = "Ra  "; CHECK( parse_element(c) ==  88 );
        c = "Ac  "; CHECK( parse_element(c) ==  89 );
        c = "Th  "; CHECK( parse_element(c) ==  90 );
        c = "Pa  "; CHECK( parse_element(c) ==  91 );
        c = "U   "; CHECK( parse_element(c) ==  92 );
        c = "Np  "; CHECK( parse_element(c) ==  93 );
        c = "Pu  "; CHECK( parse_element(c) ==  94 );
        c = "Am  "; CHECK( parse_element(c) ==  95 );
        c = "Cm  "; CHECK( parse_element(c) ==  96 );
        c = "Bk  "; CHECK( parse_element(c) ==  97 );
        c = "Cf  "; CHECK( parse_element(c) ==  98 );
        c = "Es  "; CHECK( parse_element(c) ==  99 );
        c = "Fm  "; CHECK( parse_element(c) == 100 );
        c = "Md  "; CHECK( parse_element(c) == 101 );
        c = "No  "; CHECK( parse_element(c) == 102 );
        c = "Lr  "; CHECK( parse_element(c) == 103 );
        c = "Rf  "; CHECK( parse_element(c) == 104 );
        c = "Db  "; CHECK( parse_element(c) == 105 );
        c = "Sg  "; CHECK( parse_element(c) == 106 );
        c = "Bh  "; CHECK( parse_element(c) == 107 );
        c = "Hs  "; CHECK( parse_element(c) == 108 );
        c = "Mt  "; CHECK( parse_element(c) == 109 );
        c = "Ds  "; CHECK( parse_element(c) == 110 );
        c = "Rg  "; CHECK( parse_element(c) == 111 );
        c = "Cn  "; CHECK( parse_element(c) == 112 );
        c = "Uut "; CHECK( parse_element(c) == 113 );
        c = "Fl  "; CHECK( parse_element(c) == 114 );
        c = "Uup "; CHECK( parse_element(c) == 115 );
        c = "Lv  "; CHECK( parse_element(c) == 116 );
        c = "Uus "; CHECK( parse_element(c) == 117 );
        c = "Uuo "; CHECK( parse_element(c) == 118 );
    }

    SECTION("Atomic numbers")
    {
        char const* c;

        c = "1   "; CHECK( parse_element(c) ==   1 );
        c = "2   "; CHECK( parse_element(c) ==   2 );
        c = "3   "; CHECK( parse_element(c) ==   3 );
        c = "4   "; CHECK( parse_element(c) ==   4 );
        c = "5   "; CHECK( parse_element(c) ==   5 );
        c = "6   "; CHECK( parse_element(c) ==   6 );
        c = "7   "; CHECK( parse_element(c) ==   7 );
        c = "8   "; CHECK( parse_element(c) ==   8 );
        c = "9   "; CHECK( parse_element(c) ==   9 );
        c = "10  "; CHECK( parse_element(c) ==  10 );
        c = "11  "; CHECK( parse_element(c) ==  11 );
        c = "12  "; CHECK( parse_element(c) ==  12 );
        c = "13  "; CHECK( parse_element(c) ==  13 );
        c = "14  "; CHECK( parse_element(c) ==  14 );
        c = "15  "; CHECK( parse_element(c) ==  15 );
        c = "16  "; CHECK( parse_element(c) ==  16 );
        c = "17  "; CHECK( parse_element(c) ==  17 );
        c = "18  "; CHECK( parse_element(c) ==  18 );
        c = "19  "; CHECK( parse_element(c) ==  19 );
        c = "20  "; CHECK( parse_element(c) ==  20 );
        c = "21  "; CHECK( parse_element(c) ==  21 );
        c = "22  "; CHECK( parse_element(c) ==  22 );
        c = "23  "; CHECK( parse_element(c) ==  23 );
        c = "24  "; CHECK( parse_element(c) ==  24 );
        c = "25  "; CHECK( parse_element(c) ==  25 );
        c = "26  "; CHECK( parse_element(c) ==  26 );
        c = "27  "; CHECK( parse_element(c) ==  27 );
        c = "28  "; CHECK( parse_element(c) ==  28 );
        c = "29  "; CHECK( parse_element(c) ==  29 );
        c = "30  "; CHECK( parse_element(c) ==  30 );
        c = "31  "; CHECK( parse_element(c) ==  31 );
        c = "32  "; CHECK( parse_element(c) ==  32 );
        c = "33  "; CHECK( parse_element(c) ==  33 );
        c = "34  "; CHECK( parse_element(c) ==  34 );
        c = "35  "; CHECK( parse_element(c) ==  35 );
        c = "36  "; CHECK( parse_element(c) ==  36 );
        c = "37  "; CHECK( parse_element(c) ==  37 );
        c = "38  "; CHECK( parse_element(c) ==  38 );
        c = "39  "; CHECK( parse_element(c) ==  39 );
        c = "40  "; CHECK( parse_element(c) ==  40 );
        c = "41  "; CHECK( parse_element(c) ==  41 );
        c = "42  "; CHECK( parse_element(c) ==  42 );
        c = "43  "; CHECK( parse_element(c) ==  43 );
        c = "44  "; CHECK( parse_element(c) ==  44 );
        c = "45  "; CHECK( parse_element(c) ==  45 );
        c = "46  "; CHECK( parse_element(c) ==  46 );
        c = "47  "; CHECK( parse_element(c) ==  47 );
        c = "48  "; CHECK( parse_element(c) ==  48 );
        c = "49  "; CHECK( parse_element(c) ==  49 );
        c = "50  "; CHECK( parse_element(c) ==  50 );
        c = "51  "; CHECK( parse_element(c) ==  51 );
        c = "52  "; CHECK( parse_element(c) ==  52 );
        c = "53  "; CHECK( parse_element(c) ==  53 );
        c = "54  "; CHECK( parse_element(c) ==  54 );
        c = "55  "; CHECK( parse_element(c) ==  55 );
        c = "56  "; CHECK( parse_element(c) ==  56 );
        c = "57  "; CHECK( parse_element(c) ==  57 );
        c = "58  "; CHECK( parse_element(c) ==  58 );
        c = "59  "; CHECK( parse_element(c) ==  59 );
        c = "60  "; CHECK( parse_element(c) ==  60 );
        c = "61  "; CHECK( parse_element(c) ==  61 );
        c = "62  "; CHECK( parse_element(c) ==  62 );
        c = "63  "; CHECK( parse_element(c) ==  63 );
        c = "64  "; CHECK( parse_element(c) ==  64 );
        c = "65  "; CHECK( parse_element(c) ==  65 );
        c = "66  "; CHECK( parse_element(c) ==  66 );
        c = "67  "; CHECK( parse_element(c) ==  67 );
        c = "68  "; CHECK( parse_element(c) ==  68 );
        c = "69  "; CHECK( parse_element(c) ==  69 );
        c = "70  "; CHECK( parse_element(c) ==  70 );
        c = "71  "; CHECK( parse_element(c) ==  71 );
        c = "72  "; CHECK( parse_element(c) ==  72 );
        c = "73  "; CHECK( parse_element(c) ==  73 );
        c = "74  "; CHECK( parse_element(c) ==  74 );
        c = "75  "; CHECK( parse_element(c) ==  75 );
        c = "76  "; CHECK( parse_element(c) ==  76 );
        c = "77  "; CHECK( parse_element(c) ==  77 );
        c = "78  "; CHECK( parse_element(c) ==  78 );
        c = "79  "; CHECK( parse_element(c) ==  79 );
        c = "80  "; CHECK( parse_element(c) ==  80 );
        c = "81  "; CHECK( parse_element(c) ==  81 );
        c = "82  "; CHECK( parse_element(c) ==  82 );
        c = "83  "; CHECK( parse_element(c) ==  83 );
        c = "84  "; CHECK( parse_element(c) ==  84 );
        c = "85  "; CHECK( parse_element(c) ==  85 );
        c = "86  "; CHECK( parse_element(c) ==  86 );
        c = "87  "; CHECK( parse_element(c) ==  87 );
        c = "88  "; CHECK( parse_element(c) ==  88 );
        c = "89  "; CHECK( parse_element(c) ==  89 );
        c = "90  "; CHECK( parse_element(c) ==  90 );
        c = "91  "; CHECK( parse_element(c) ==  91 );
        c = "92  "; CHECK( parse_element(c) ==  92 );
        c = "93  "; CHECK( parse_element(c) ==  93 );
        c = "94  "; CHECK( parse_element(c) ==  94 );
        c = "95  "; CHECK( parse_element(c) ==  95 );
        c = "96  "; CHECK( parse_element(c) ==  96 );
        c = "97  "; CHECK( parse_element(c) ==  97 );
        c = "98  "; CHECK( parse_element(c) ==  98 );
        c = "99  "; CHECK( parse_element(c) ==  99 );
        c = "100 "; CHECK( parse_element(c) == 100 );
        c = "101 "; CHECK( parse_element(c) == 101 );
        c = "102 "; CHECK( parse_element(c) == 102 );
        c = "103 "; CHECK( parse_element(c) == 103 );
        c = "104 "; CHECK( parse_element(c) == 104 );
        c = "105 "; CHECK( parse_element(c) == 105 );
        c = "106 "; CHECK( parse_element(c) == 106 );
        c = "107 "; CHECK( parse_element(c) == 107 );
        c = "108 "; CHECK( parse_element(c) == 108 );
        c = "109 "; CHECK( parse_element(c) == 109 );
        c = "110 "; CHECK( parse_element(c) == 110 );
        c = "111 "; CHECK( parse_element(c) == 111 );
        c = "112 "; CHECK( parse_element(c) == 112 );
        c = "113 "; CHECK( parse_element(c) == 113 );
        c = "114 "; CHECK( parse_element(c) == 114 );
        c = "115 "; CHECK( parse_element(c) == 115 );
        c = "116 "; CHECK( parse_element(c) == 116 );
        c = "117 "; CHECK( parse_element(c) == 117 );
        c = "118 "; CHECK( parse_element(c) == 118 );
    }
}

TEST_CASE("Parsing integers", "[parsing]")
{
    char const* c;

    SECTION("integers")
    {
        c =  "1234567890 "; CHECK( parse_int<char const*>(c) ==   1234567890 );
        c = "+1234567890 "; CHECK( parse_int<char const*>(c) ==  +1234567890 );
        c = "-1234567890 "; CHECK( parse_int<char const*>(c) ==  -1234567890 );
        c =  "0987654321 "; CHECK( parse_int<char const*>(c) ==    987654321 );
        c = "+0987654321 "; CHECK( parse_int<char const*>(c) ==   +987654321 );
        c = "-0987654321 "; CHECK( parse_int<char const*>(c) ==   -987654321 );
    }
}

TEST_CASE("Parsing floating point numbers", "[parsing]")
{
    char const* c;

    SECTION("whole numbers")
    {
        c =  "1234567890 "; CHECK( parse_float<char const*>(c) ==  1234567890. );
        c = "+1234567890 "; CHECK( parse_float<char const*>(c) == +1234567890. );
        c = "-1234567890 "; CHECK( parse_float<char const*>(c) == -1234567890. );
        c =  "0123456789 "; CHECK( parse_float<char const*>(c) ==   123456789. );
    }

    SECTION("fractional numbers")
    {
        c =  "123.456 "; CHECK( parse_float<char const*>(c) ==  123.456 );
        c = "+123.456 "; CHECK( parse_float<char const*>(c) == +123.456 );
        c = "-123.456 "; CHECK( parse_float<char const*>(c) == -123.456 );
    }

    SECTION("exponential notation")
    {
        c = "123E-1 "; CHECK( parse_float<char const*>(c) ==  12.3 );
        c = "-7.1e2 "; CHECK( parse_float<char const*>(c) == -710. );
    }

    SECTION("special cases")
    {
        c = ".0 " ; CHECK( parse_float<char const*>(c) == 0. );
        c = "0 "  ; CHECK( parse_float<char const*>(c) == 0. );
        c = "0. " ; CHECK( parse_float<char const*>(c) == 0. );
        c = "0.0 "; CHECK( parse_float<char const*>(c) == 0. );
    }
}

TEST_CASE("Parsing parsing tokens", "[parsing]")
{
    char const* c;

    SECTION("integer parse tokens")
    {
        c = "1234 "; CHECK( parse_token<char const*>(c, true).intv() == 1234 );
    }

    SECTION("floating point number parse tokens")
    {
        c = "1.234 "; CHECK( parse_token<char const*>(c, true).floatv() == 1.234 );
    }

    SECTION("string parse tokens")
    {
        c = "abc "; CHECK( parse_token<char const*>(c, false).stringv() == std::string("abc") );
        c = "1bc "; CHECK( parse_token<char const*>(c, true ).stringv() == std::string("1bc") );
    }

    SECTION("special cases")
    {
        c = "1+ "; CHECK( parse_token<char const*>(c, false).stringv() == std::string("1+") );
        c = "1+ "; CHECK( parse_token<char const*>(c, true ).stringv() == std::string("1+") );
        c = "+9.1E1E2 "; CHECK( parse_token<char const*>(c, false).stringv() == std::string("+9.1E1E2"));
        c = "+9.1E1E2 "; CHECK( parse_token<char const*>(c, true ).stringv() == std::string("+9.1E1E2"));
        c = "1+1\n"; CHECK( parse_token<char const*>(c, true).stringv() == std::string("1+1") );
        c = "e "; CHECK( parse_token<char const*>(c, true).stringv() == std::string("e") );
    }
}

TEST_CASE("Parsing from file", "[parsing]")
{
    SECTION("some integers")
    {
        const std::string s = "1 2 3\n 123 456";
        temporary_file f(s);  // block scope
        parsing_file_iterator beg = parsing_file_iterator(f.filename(), 2, 1);  // no look-back for integers
        parsing_file_iterator end = parsing_file_iterator();

        CHECK( parse_int(beg) == 1 ); skip_ws(beg);
        CHECK( parse_int(beg) == 2 ); skip_ws(beg);
        CHECK( parse_int(beg) == 3 ); skip_to_eol(beg);
        skip_ws(beg);
        CHECK( parse_int(beg) == 123 ); skip_ws(beg);
        CHECK( parse_int(beg) == 456 );
    }

    SECTION("many integers")
    {
        const int n = 10000;
        std::string s; for(int i = 0; i < n; ++i) s += "123 ";
        temporary_file f(s);
        parsing_file_iterator beg = parsing_file_iterator(f.filename());

        int res[n];
        for(int i = 0; i < n; ++i) { res[i] = parse_int(beg); skip_ws(beg); }
        bool b = true; for(int i = 0; i < n; ++i) b = b && (res[i] == 123);
        CHECK( b );
    }
}
