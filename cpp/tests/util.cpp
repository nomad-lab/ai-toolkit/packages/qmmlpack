// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Utility routines for testing

#include "util.hpp"

#include <random>

//  //////////////////////
//  //  Random numbers  //
//  //////////////////////

static std::mt19937 _rnd_engine;  // uses default seed

// Returns random number in the interval [lower,upper)
double random_uniform_real(double lower, double upper)
{
    return std::uniform_real_distribution<double>(lower, upper)(_rnd_engine);
}
