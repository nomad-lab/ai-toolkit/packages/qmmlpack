// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Import of atomistic systems in extended XYZ format.

#ifndef QMMLPACK_IO_EXYZ_HPP_INCLUDED  // Include guard
#define QMMLPACK_IO_EXYZ_HPP_INCLUDED

#include <array>
#include <vector>

#include "qmmlpack/base.hpp"
#include "qmmlpack/io_parse.hpp"

namespace qmml {

//  /////////////////
//  //  Interface  //
//  /////////////////

// Data container for finite atomistic system
// This is not a molecule class, merely the data from extended XYZ format.
struct QMMLAPI extxyz_data
{
    template<typename T> using vector = std::vector<T>;
    template<typename T, int N> using array  = std::array<T, N>;

    vector<uint8_t> an;                  // atomic numbers (atoms)
    vector<array<double, 3>> xyz;        // atom coordinates (atoms x 3)
    vector<parsing_token> mp;            // molecular properties (tokens)
    vector<vector<parsing_token>> ap;    // atomic properties (atoms x tokens)
    vector<vector<parsing_token>> addp;  // additional properties (lines x tokens)

    extxyz_data(size_t n) : an(n), xyz(n), ap(n) {}
};

// Parses finite atomistic systems in extended XYZ format and returns them.
// * Parses whole sequence; to parse subsequences, pass corresponding iterators
template<typename Iter> QMMLAPI std::vector<extxyz_data> import_extxyz(Iter beg, Iter end, bool addp = false);

//  //////////////////////
//  //  Implementation  //
//  //////////////////////

// Parses data of one molecule in extended XYZ format
template<typename Iter> extxyz_data parse_extxyz_entry(Iter & i, bool addp)
{
    // header line: ws* int any* \n
    skip_ws(i);
    int k = parse_int(i);
    skip_to_eol(i);

    extxyz_data res(k);

    // molecular properties line: (ws* token)* ws* \n
    skip_ws(i);
    while(*i != 0x0a)
    {
        res.mp.push_back(parse_token(i, true));
        skip_ws(i);
    }
    ++i; // eol

    // atom block lines: ws* el ws+ x ws+ y ws+ z ws+ (ws* token)* \n
    for(int j = 0; j < k; ++j)
    {
        // atomic number
        skip_ws(i);
        res.an[j] = parse_element(i);

        // coordinates
        skip_ws(i);
        res.xyz[j][0] = parse_float(i); skip_ws(i);
        res.xyz[j][1] = parse_float(i); skip_ws(i);
        res.xyz[j][2] = parse_float(i); skip_ws(i);

        // atomic properties
        while(*i != 0x0a)
        {
            res.ap[j].push_back(parse_token(i, true));
            skip_ws(i);
        }
        if(j < k-1) ++i;  // eol; save last one
    }

    // additional properties
    if(addp)
    {
        ++i;  // eol from atom block

        skip_ws(i);
        while(*i != 0x0a)
        {
            std::vector<parsing_token> line;
            while(*i != 0x0a)
            {
                line.push_back(parse_token(i, true));
                skip_ws(i);
            }
            res.addp.push_back(line);
            ++i;  // eol
            skip_ws(i);
        }
    }

    // Skip trailing ws
    skip_to_eol(i);  // Correct format guarantees termination by end-of-line.

    return res;
}

template<typename Iter> QMMLAPI std::vector<extxyz_data> import_extxyz(Iter beg, Iter end, bool addp)
{
    std::vector<extxyz_data> res;
    parsing_iterator<Iter> i(beg, end);
    while(i != end)
    {
        res.push_back(parse_extxyz_entry(i, addp));
        if(i != end) skip_ws(i);
    }
    return res;
}

}  // namespace qmml

#endif  // Include guard
