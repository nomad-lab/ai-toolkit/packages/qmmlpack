// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Numerics utilities

#ifndef QMMLPACK_NUMERICS_HPP_INCLUDED  // include guard
#define QMMLPACK_NUMERICS_HPP_INCLUDED

#include <cstddef>

namespace qmml {

//  /////////////////////////////
//  //  Lower triangular part  //
//  /////////////////////////////

// Number of elements in the lower triangular part of an n x m matrix starting from k-th diagonal
size_t lower_triangular_part_nelems(size_t n, size_t m, ptrdiff_t k = 0);

// Vector containing lower triangular part of n x m matrix starting from k-th diagonal
// v contiguous block of memory of size lower_triangular_part_nelems(n, m, k) * size(double)
// M contiguous block of memory of size n * m * size(double)
void lower_triangular_part(double *const v, double const*const M, const size_t n, const size_t m, ptrdiff_t k = 0);


//  //////////////////
//  //  Symmetrize  //
//  //////////////////

// Symmetrizes quadratic matrix by averaging.
void symmetrize(double *const M, const size_t n);


//  /////////////////////////////////////////
//  //  Forward and backward substitution  //
//  /////////////////////////////////////////

// Solves linear system of equations given by a lower triangular matrix and a vector, L x = b.
// x contiguous block of memory of size n      * size(double)
// L contiguous block of memory of size n * n  * size(double)
// b contiguous block of memory of size n      * size(double)
// x and b can point to the same address
void forward_substitution_v(double *const x, double const*const L, double const*const b, const size_t n);

// Solves linear system of equations given by a lower triangular matrix and a matrix, L X = B.
// X contiguous block of memory of size n * m  * size(double)
// L contiguous block of memory of size n * n  * size(double)
// B contiguous block of memory of size n * m  * size(double)
// X and B can point to the same address
void forward_substitution_m(double *const X, double const*const L, double const*const B, const size_t n, const size_t m);

// Solves linear system of equations given by an upper triangular matrix and a vector, U x = b.
// x contiguous block of memory of size n      * size(double)
// U contiguous block of memory of size n * n  * size(double)
// b contiguous block of memory of size n      * size(double)
// x and b can point to the same address
void backward_substitution_v(double *const x, double const*const U, double const*const b, const size_t n);

// Solves linear system of equations given by an upper triangular matrix and a matrix, U X = B.
// X contiguous block of memory of size n * m  * size(double)
// U contiguous block of memory of size n * n  * size(double)
// B contiguous block of memory of size n * m  * size(double)
// X and B can point to the same address
void backward_substitution_m(double *const X, double const*const U, double const*const B, const size_t n, const size_t m);

// One-pass forward and backward substitution, solving L L^T x = b.
// x contiguous block of memory of size n      * size(double)
// L contiguous block of memory of size n * n  * size(double)
// b contiguous block of memory of size n      * size(double)
// x and b can point to the same address
void forward_backward_substitution_v(double *const x, double const*const L, double const*const b, const size_t n);

// One-pass forward and backward substitution, solving U^T U x = b.
// x contiguous block of memory of size n      * size(double)
// U contiguous block of memory of size n * n  * size(double)
// b contiguous block of memory of size n      * size(double)
// x and b can point to the same address
void backward_forward_substitution_v(double *const x, double const*const U, double const*const b, const size_t n);

//  /////////////
//  //  clamp  //
//  /////////////

//  Switch to std::clamp once requiring C++/17.

template<class T> constexpr const T& clamp(const T& v, const T& lo, const T& hi)
{
    return (v < lo) ? lo : (hi < v) ? hi : v;
}

} // namespace qmml

#endif  // include guard
