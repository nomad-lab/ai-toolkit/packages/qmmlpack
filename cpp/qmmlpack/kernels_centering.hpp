// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Centering of kernel matrices

#ifndef QMMLPACK_KERNELS_CENTERING_HPP_INCLUDED  // include guard
#define QMMLPACK_KERNELS_CENTERING_HPP_INCLUDED

namespace qmml {

//  /////////////////////////////////
//  //  Centering a kernel matrix  //
//  /////////////////////////////////

// - Potential speed-up possible by using a passed temporary buffer?

// Centers a kernel matrix K of samples versus themselves
// Kc  contiguous memory block of size n * n  * size(T)
// K   contiguous memory block of size n * n  * size(T)
void center_kernel_matrix_k(double *const Kc, double const*const K, const size_t n);

// Centers a kernel matrix L with respect to another kernel matrix K
// Lc  contiguous memory block of size n * m  * size(T)
// K   contiguous memory block of size n * n  * size(T)
// L   contiguous memory block of size n * m  * size(T)
void center_kernel_matrix_l(double *const Lc, double const*const K, double const*const L, const size_t n, const size_t m);

// Centers a test versus test samples kernel matrix M with respect to another kernel matrix K
// Mc  contiguous memory block of size m * m  * size(T)
// K   contiguous memory block of size n * n  * size(T)
// L   contiguous memory block of size n * m  * size(T)
// M   contiguous memory block of size m * m  * size(T)
void center_kernel_matrix_m(double *const Mc, double const*const K, double const*const L, double const*const M, const size_t n, const size_t m);

// Centers a kernel matrix M of test samples versus themselves with respect to a training samples kernel matrix K, returning only the diagonal of M.
// mc  contiguous memory block of size m      * size(T)
// K   contiguous memory block of size n * n  * size(T)
// L   contiguous memory block of size n * m  * size(T)
// mv  contiguous memory block of size m      * size(T)
void center_kernel_matrix_mdiag(double *const mc, double const*const K, double const*const L, double const*const mv, const size_t n, const size_t m);

} // namespace qmml

#endif  // include guard
