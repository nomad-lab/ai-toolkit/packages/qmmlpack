// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Parsing routines.

#ifndef QMMLPACK_IO_PARSE_HPP_INCLUDED  // Include guard
#define QMMLPACK_IO_PARSE_HPP_INCLUDED

#include <stdexcept>
#include <map>
#include <iterator>
#include <cstdio>
#include <cstring>
#include <limits>
#include <cstdint>
#include <memory>   // unique_ptr

#include "qmmlpack/base.hpp"

// Notes:
// * Type Iter must satisfy ForwardIterator (multi-pass guarantee)

namespace qmml {

//  ////////////////////////
//  //  Parsing routines  //
//  ////////////////////////

// True if character is a whitespace character, excluding newline
inline bool is_ws(char const c)
{
    switch(c)
    {
        case 0x20:  // space
        case 0x09:  // tabulator
        case 0x0b:  // vertical tabulator
        case 0x0c:  // form feed
        case 0x0d:  // carriage return
            return true;
        default:
            return false;
    }
}

// True if character is a whitespace character, including newline
inline bool is_ws_eol(char const c)
{
    return is_ws(c) || c == 0x0a;
}

// True if character is a digit (0,1,2,3,4,5,6,7,8,9)
inline bool is_digit(char const c)
{
    return c >= 0x30 && c <= 0x39;
}

// True if character is an alphabet character (a-z, A-Z)
inline bool is_alpha(char const c)
{
    return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
}

// True if character is alphanumeric (alphabetic or digit)
inline bool is_alnum(char const c)
{
    return is_alpha(c) || is_digit(c);
}

// Skips zero or more whitespace characters except newline
template<typename Iter> void skip_ws(Iter & i)
{
    while(is_ws(*i)) ++i;
}

// Skips zero or more whitespace characters, including newline
template<typename Iter> void skip_ws_eol(Iter & i)
{
    while(is_ws_eol(*i)) ++i;
}

// Skips zero or more arbitrary characters until and including first newline
template<typename Iter> void skip_to_eol(Iter & i)
{
    while(*i != 0x0a) ++i;
    ++i;  // eol
}

// Parses an integer value
template<typename Iter> int parse_int(Iter & i)
{
    Iter j(i);
    int res = 0, sign = +1;
    if(*i == '+') ++i; else if(*i == '-') { sign = -1; ++i; }  // sign
    while(is_digit(*i)) { res = 10*res + *i - '0'; ++i; }  // digits 0,1,2,3,4,5,6,7,8,9

    if(j == i) throw QMML_ERROR("Expected integer, got nothing.");
    if(!is_ws_eol(*i)) throw QMML_ERROR("Expected integer, got non-digit.");

    return res*sign;
}

// Parses a floating point value
template<typename Iter> double parse_float(Iter & i)
{
    Iter j(i);

    if(*i == '+' || *i == '-') ++i;  // sign
    while(is_digit(*i)) ++i;         // digits before comma
    if(*i == '.') ++i;               // comma
    while(is_digit(*i)) ++i;         // digits after comma
    if(*i == 'E' || *i == 'e') parse_int(++i);  // exponential notation

    if(i == j) throw QMML_ERROR("parsing error, expected float, but found nothing")

    try
    {
        std::size_t n;
        std::string s { j, i };
        const double d = stod(s, &n);
        if(n != s.size()) throw QMML_ERROR("Parsing of floating point failed.");
        return d;
    }
    catch(std::invalid_argument const& e) { throw QMML_ERROR(e.what()); }
    catch(std::out_of_range const& e) { throw QMML_ERROR(e.what()); }
}

// Parsing token holding integer, floating point value or string.
class parsing_token
{
public:
    explicit parsing_token() : type_(type::uninitialized_) {}
    explicit parsing_token(int i) : type_(type::int_), intv_(i) {}
    explicit parsing_token(double const f) : type_(type::float_), floatv_(f) {}
    explicit parsing_token(std::string const& s) : type_(type::string_), stringv_(new std::string(s)) {}

    parsing_token(parsing_token const& arg) { copy(arg); }

    void copy(parsing_token const& arg)
    // precondition: this is in pristine state (e.g., after clear())
    {
        type_ = arg.type_;
        switch(type_)
        {
            case type::uninitialized_: break;
            case type::int_          : intv_ = arg.intv_; break;
            case type::float_        : floatv_ = arg.floatv_; break;
            case type::string_       : stringv_ = new std::string(*arg.stringv_); break;
        }
    }

    ~parsing_token() { clear(); }

    void clear()
    {
        if(is_string())
        {
            delete stringv_; stringv_ = nullptr;
            type_ = type::uninitialized_;
        }
    }

    parsing_token & operator=(parsing_token const& arg)
    {
        if(this != &arg)  // self-assignment would destroy object
        {
            clear(); copy(arg);
        }
        return *this;
    }

    bool is_int   () const { return type_ == type::int_   ; }
    bool is_float () const { return type_ == type::float_ ; }
    bool is_string() const { return type_ == type::string_; }

    int & intv() { if(is_int()) return intv_; else throw QMML_ERROR("Parsing token does not hold an integer value."); }
    int const& intv() const { if(is_int()) return intv_; else throw QMML_ERROR("Parsing token does not hold an integer value."); }

    double & floatv() { if(is_float()) return floatv_; else throw QMML_ERROR("Parsing token does not hold a float value."); }
    double const& floatv() const { if(is_float()) return floatv_; else throw QMML_ERROR("Parsing token does not hold a float value."); }

    std::string & stringv() { if(is_string()) return *stringv_; else throw QMML_ERROR("Parsing token does not hold a string value."); }
    std::string const& stringv() const { if(is_string()) return *stringv_; else throw QMML_ERROR("Parsing token does not hold a string value."); }

private:
    enum type { uninitialized_, int_, float_, string_ } type_;

    union
    {
        int intv_;               // token has integer value
        double floatv_;          // token has floating point value
        std::string * stringv_;  // token has string value
    };
};

// Parses a contiguous sequence of non-whitespace characters, interpreting it as int or float if desired
template<typename Iter> parsing_token parse_token(Iter & i, double interpret = false)
{
    Iter j(i);
    bool pint = interpret, pfloat = interpret;

    while(!is_ws_eol(*i))
    {
        if(pint && !(is_digit(*i) || *i == '+' || *i == '-')) pint = false;
        if(pfloat && !(is_digit(*i) || *i == '.' || *i == '+' || *i == '-' || *i == 'E' || *i == 'e')) pfloat = false;
        ++i;
    }

    if(pint && pfloat) pfloat = false;

    // if token could be an integer, try to parse it
    if(pint)
    {
        Iter k(j);  // j needs to be copied, otherwise token is consumed in case of failure
        try { return parsing_token(parse_int(k)); }
        catch(qmml::error const&) {}
    }

    // if token could be a float, try to parse it
    if(pfloat)
    {
        Iter k(j); // j needs to be copied, otherwise token is consumed
        try { return parsing_token(parse_float(k)); }
        catch(qmml::error const&) {}
    }

    return parsing_token(std::string(j, i));
}

// Parses element abbreviation or atomic number
template<typename Iter> unsigned char parse_element(Iter & i)
{
    static const std::map<const std::string, const unsigned char> element_data = {
        {"H"  ,   1}, {"He",   2}, {"Li" ,   3}, {"Be",   4}, {"B"  ,   5}, {"C"  ,   6},  {"N",   7},
        {"O"  ,   8}, {"F" ,   9}, {"Ne" ,  10}, {"Na",  11}, {"Mg" ,  12}, {"Al" ,  13}, {"Si",  14},
        {"P"  ,  15}, {"S" ,  16}, {"Cl" ,  17}, {"Ar",  18}, {"K"  ,  19}, {"Ca" ,  20}, {"Sc",  21},
        {"Ti" ,  22}, {"V" ,  23}, {"Cr" ,  24}, {"Mn",  25}, {"Fe" ,  26}, {"Co" ,  27}, {"Ni",  28},
        {"Cu" ,  29}, {"Zn",  30}, {"Ga" ,  31}, {"Ge",  32}, {"As" ,  33}, {"Se" ,  34}, {"Br",  35},
        {"Kr" ,  36}, {"Rb",  37}, {"Sr" ,  38}, {"Y" ,  39}, {"Zr" ,  40}, {"Nb" ,  41}, {"Mo",  42},
        {"Tc" ,  43}, {"Ru",  44}, {"Rh" ,  45}, {"Pd",  46}, {"Ag" ,  47}, {"Cd" ,  48}, {"In",  49},
        {"Sn" ,  50}, {"Sb",  51}, {"Te" ,  52}, {"I" ,  53}, {"Xe" ,  54}, {"Cs" ,  55}, {"Ba",  56},
        {"La" ,  57}, {"Ce",  58}, {"Pr" ,  59}, {"Nd",  60}, {"Pm" ,  61}, {"Sm" ,  62}, {"Eu",  63},
        {"Gd" ,  64}, {"Tb",  65}, {"Dy" ,  66}, {"Ho",  67}, {"Er" ,  68}, {"Tm" ,  69}, {"Yb",  70},
        {"Lu" ,  71}, {"Hf",  72}, {"Ta" ,  73}, {"W" ,  74}, {"Re" ,  75}, {"Os" ,  76}, {"Ir",  77},
        {"Pt" ,  78}, {"Au",  79}, {"Hg" ,  80}, {"Tl",  81}, {"Pb" ,  82}, {"Bi" ,  83}, {"Po",  84},
        {"At" ,  85}, {"Rn",  86}, {"Fr" ,  87}, {"Ra",  88}, {"Ac" ,  89}, {"Th" ,  90}, {"Pa",  91},
        {"U"  ,  92}, {"Np",  93}, {"Pu" ,  94}, {"Am",  95}, {"Cm" ,  96}, {"Bk" ,  97}, {"Cf",  98},
        {"Es" ,  99}, {"Fm", 100}, {"Md" , 101}, {"No", 102}, {"Lr" , 103}, {"Rf" , 104}, {"Db", 105},
        {"Sg" , 106}, {"Bh", 107}, {"Hs" , 108}, {"Mt", 109}, {"Ds" , 110}, {"Rg" , 111}, {"Cn", 112},
        {"Uut", 113}, {"Fl", 114}, {"Uup", 115}, {"Lv", 116}, {"Uus", 117}, {"Uuo", 118},

        {"1"  ,   1}, {"2"  ,   2}, {"3"  ,   3}, {"4"  ,   4}, {"5"  ,   5}, {"6"  ,   6}, {"7"  ,   7},
        {"8"  ,   8}, {"9"  ,   9}, {"10" ,  10}, {"11" ,  11}, {"12" ,  12}, {"13" ,  13}, {"14" ,  14},
        {"15" ,  15}, {"16" ,  16}, {"17" ,  17}, {"18" ,  18}, {"19" ,  19}, {"20" ,  20}, {"21" ,  21},
        {"22" ,  22}, {"23" ,  23}, {"24" ,  24}, {"25" ,  25}, {"26" ,  26}, {"27" ,  27}, {"28" ,  28},
        {"29" ,  29}, {"30" ,  30}, {"31" ,  31}, {"32" ,  32}, {"33" ,  33}, {"34" ,  34}, {"35" ,  35},
        {"36" ,  36}, {"37" ,  37}, {"38" ,  38}, {"39" ,  39}, {"40" ,  40}, {"41" ,  41}, {"42" ,  42},
        {"43" ,  43}, {"44" ,  44}, {"45" ,  45}, {"46" ,  46}, {"47" ,  47}, {"48" ,  48}, {"49" ,  49},
        {"50" ,  50}, {"51" ,  51}, {"52" ,  52}, {"53" ,  53}, {"54" ,  54}, {"55" ,  55}, {"56" ,  56},
        {"57" ,  57}, {"58" ,  58}, {"59" ,  59}, {"60" ,  60}, {"61" ,  61}, {"62" ,  62}, {"63" ,  63},
        {"64" ,  64}, {"65" ,  65}, {"66" ,  66}, {"67" ,  67}, {"68" ,  68}, {"69" ,  69}, {"70" ,  70},
        {"71" ,  71}, {"72" ,  72}, {"73" ,  73}, {"74" ,  74}, {"75" ,  75}, {"76" ,  76}, {"77" ,  77},
        {"78" ,  78}, {"79" ,  79}, {"80" ,  80}, {"81" ,  81}, {"82" ,  82}, {"83" ,  83}, {"84" ,  84},
        {"85" ,  85}, {"86" ,  86}, {"87" ,  87}, {"88" ,  88}, {"89" ,  89}, {"90" ,  90}, {"91" ,  91},
        {"92" ,  92}, {"93" ,  93}, {"94" ,  94}, {"95" ,  95}, {"96" ,  96}, {"97" ,  97}, {"98" ,  98},
        {"99" ,  99}, {"100", 100}, {"101", 101}, {"102", 102}, {"103", 103}, {"104", 104}, {"105", 105},
        {"106", 106}, {"107", 107}, {"108", 108}, {"109", 109}, {"110", 110}, {"111", 111}, {"112", 112},
        {"113", 113}, {"114", 114}, {"115", 115}, {"116", 116}, {"117", 117}, {"118", 118}, {"119", 119}
    };

    std::string const s(parse_token(i, false).stringv());
    return element_data.at(s);
}

//  /////////////////
//  //  Iterators  //
//  /////////////////

// Wrapped iterator. Returns additional end-of-line at end-of-input
// * Behaves like member cur_, end_ is ignored everywhere except when dereferencing.
// * Behaviour beyond end depends on underlying iterator.
// * For use with const_iterators.
// * Not derived from Iter to avoid vtable lookup and to work with pointers.
template<typename Iter> class parsing_iterator
{
public:
    typedef typename std::iterator_traits<Iter>::difference_type difference_type;
    typedef typename std::iterator_traits<Iter>::value_type value_type;
    typedef typename std::iterator_traits<Iter>::reference reference;
    typedef typename std::iterator_traits<Iter>::pointer pointer;
    // typedef typename std::iterator_traits<Iter>::iterator_category iterator_category
    // For std::string::const_iterator, above fails for string construction string(beg,end).
    // Apparently, the STL calls another constructor if iterators are random_access_iterators.
    typedef typename std::forward_iterator_tag iterator_category;

    parsing_iterator() {} // requirement for forward iterators
    parsing_iterator(Iter beg, Iter end) : cur_(beg), end_(end) {}
    ~parsing_iterator() {}

    parsing_iterator(parsing_iterator const& arg) : cur_(arg.cur_), end_(arg.end_) {}

    parsing_iterator& operator=(parsing_iterator const& arg) { cur_ = arg.cur_; end_ = arg.end_; return *this; }

    parsing_iterator& operator++() // prefix increment
    {
        if(cur_ != end_) ++cur_;
        return *this;
    }

    parsing_iterator operator++(int) { parsing_iterator tmp(*this); ++cur_; return *tmp; }; // postfix increment

    value_type operator*() const
    {
        return cur_ == end_ ? '\n' : *cur_;
    };

    pointer operator->() const = delete;  // character has no members

    bool operator==(parsing_iterator const& rhs) const { return cur_ == rhs.cur_; };  // end_ is allowed to differ.
    bool operator!=(parsing_iterator const& rhs) const { return !(*this == rhs); };

    // comparison with underlying pointer for ease of use
    bool operator==(Iter const& rhs) const { return cur_ == rhs; };
    bool operator!=(Iter const& rhs) const { return cur_ != rhs; };

protected:
    Iter cur_;
    Iter end_;
};

// "Lagged" file iterator for sequential read
// * buffered; allows limited look-back
// * potential optimization: use os-dependent hints (for sequential reading)
// * buffered reading scheme should be competitive in principle, see
//   http://stackoverflow.com/questions/17925051/fast-textfile-reading-in-c
class parsing_file_iterator
{
public:
    typedef std::ptrdiff_t difference_type;
    typedef unsigned char value_type;
    typedef unsigned char const& reference;
    typedef unsigned char *const pointer;
    typedef typename std::forward_iterator_tag iterator_category;

    parsing_file_iterator() : fd(nullptr), fpos(0) {}  // end-of-file iterator

    parsing_file_iterator(std::string const & filename, size_t buffer_size = 16*1024, size_t buffer_lookback = 128)
    : fd(nullptr), fpos (0)
    {
        fd = std::shared_ptr<file_data>(new file_data(filename.c_str(), buffer_size, buffer_lookback)) ;
    }

    ~parsing_file_iterator() {}

    parsing_file_iterator(parsing_file_iterator const& arg) : fd(arg.fd), fpos(arg.fpos) {}

    parsing_file_iterator & operator=(parsing_file_iterator const& arg) { fd = arg.fd; fpos = arg.fpos; return *this; }

    // Needs to check whether end of file has been reached, otherwise while(i != eof) ++i; loops forever
    parsing_file_iterator & operator++()  // prefix increment
    {
        if(fd != nullptr) fd->update(++fpos, fd);
        return *this;
    }

    parsing_file_iterator operator++(int) { parsing_file_iterator tmp(*this); ++(*this); return tmp; }; // postfix increment

    value_type operator*() const
    {
        return fd == nullptr ? '\n' : fd->value(fpos);
    }

    pointer operator->() const = delete;  // character has no members

    bool operator==(parsing_file_iterator const& rhs) const
    {
        return fd != nullptr && rhs.fd != nullptr ? fd->file == rhs.fd->file && fpos == rhs.fpos : fd == rhs.fd;
    }

    bool operator!=(parsing_file_iterator const& rhs) const { return !(*this == rhs); };

private:
    // "unique" internal data; iterators referring to the same file all point to the same file_data
    struct file_data
    {
        // static const size_t size_t_max = std::numeric_limits<size_t>::max();  // maximum possible value for size_t; fails for Intel compiler v17.0b
        static const size_t size_t_max = SIZE_MAX;

        std::FILE *  file;             // underlying file
        const size_t buffer_size;      // total size of buffer in bytes
        const size_t buffer_lookback;  // size of look-back part of buffer in bytes
        std::unique_ptr<value_type[]> buffer;  // buffer itself
        size_t       bpos;             // position in file of beginning of buffer
        size_t       maxpos;           // first non-valid position in file (i.e. file size), or

        file_data(char const*const filename, size_t buffer_size, size_t buffer_lookback)
        : file(nullptr), buffer_size(buffer_size), buffer_lookback(buffer_lookback), buffer(nullptr), bpos(0), maxpos(size_t_max)
        {
            if(buffer_size < 2) throw QMML_ERROR("Invalid value for buffer size.");
            if(buffer_lookback >= buffer_size) throw QMML_ERROR("Invalid value for buffer lookback size.");

            file = std::fopen(filename, "rb");
            if(!file) throw QMML_ERROR("Failed to open file.");

            buffer = std::unique_ptr<value_type[]>(new value_type[buffer_size]);
            fill_buffer(false);
        }

        ~file_data()
        {
            if(file)
            {
                const int retv = std::fclose(file); file = nullptr;
                if(retv != 0) throw QMML_ERROR("Error closing file.");

                // buffer is deleted by unique_ptr
            }
        }

        void fill_buffer(bool shift)
        {
            // if shifting the buffer, have overlap of size buffer_lookback
            // currently, only sequential buffer fills are supported
            if(shift)
            {
                std::memmove(&buffer[0], &buffer[buffer_size - buffer_lookback], buffer_lookback);
                bpos += buffer_size - buffer_lookback;

                // read non-lookback part of buffer from file
                const std::size_t br = std::fread(&buffer[buffer_lookback], 1, buffer_size-buffer_lookback, file);
                if(br < buffer_size-buffer_lookback)
                {
                    for(std::size_t i = buffer_lookback + br; i < buffer_size; ++i) buffer[i] = '\n';
                    maxpos = bpos + buffer_lookback + br;  // eof
                }
            }
            else
            {
                // fill whole buffer
                const std::size_t br = std::fread(&buffer[0], 1, buffer_size, file);
                if(br < buffer_size)
                {
                    for(std::size_t i = br; i < buffer_size; ++i) buffer[i] = '\n';
                    maxpos = bpos + br;  // eof
                }
            }
        }

        void update(size_t pos, std::shared_ptr<file_data> & fd)
        {
            if(pos < bpos) throw QMML_ERROR("Look-back behind maximum look-back distance.");
            while(pos >= bpos + buffer_size && pos < maxpos) fill_buffer(true);
            if(pos >= maxpos) fd = nullptr;
        }

        // incrementing the iterator ensures valid buffer
        value_type value(size_t pos) const { return buffer[pos-bpos]; }
    };

    std::shared_ptr<file_data> fd;  // data unique to underlying file and buffer
    size_t fpos;  // position in file (not buffer) to which iterator points
};

} // namespace qmml

#endif  // Include guard
