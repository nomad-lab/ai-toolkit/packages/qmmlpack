// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Distance functions based on p-norms

#include "qmmlpack/base.hpp"
#include "qmmlpack/distances_pnorm.hpp"

#include <cmath>

// benchmarking indicates that for small d, the BLAS overhead outweights its advantages over the naive loop solution.
// this threshold is system-dependent, but optimizing it is not worth the effort, and qmmlpack is not ATLAS.

const size_t threshold_d = 25;  // use naive loop for d <= threshold_d, BLAS otherwise

namespace qmml {

//  /////////////////////////
//  //  One-norm distance  //
//  /////////////////////////

void distance_matrix_one_norm_k (double * const D, double const*const X, const size_t n, const size_t d)
{
    if( d <= threshold_d )
    {
        // naive implementation is faster for small d
        for(size_t i = 0; i < n; ++i) for(size_t j = 0; j < n; ++j)
        {
            double res = 0;
            for(size_t k = 0; k < d; ++k) res += std::abs(X[i*d+k] - X[j*d+k]);
            D[i*n+j] = res;
        }
    }
    else
    {
        // BLAS implementation is faster for large d
        double diff[d];  // difference of input vectors
        for(size_t i = 0; i < n; ++i)
        {
            D[i*n+i] = 0;
            for(size_t j = i+1; j < n; ++j)
            {
                cblas_dcopy(d, &X[j*d], 1, &diff[0], 1);  // diff = x_j
                cblas_daxpy(d, -1, &X[i*d], 1, &diff[0], 1);  // diff = -1*x_i + diff
                D[i*n+j] = D[j*n+i] = cblas_dasum(d, &diff[0], 1);  // sum_i |diff_i|
            }
        }
    }

    // BLAS version without exploiting symmetry was slower
    // double diff[d];  // difference of input vectors
    // for(size_t i = 0; i < n; ++i) for(size_t j = 0; j < n; ++j)
    // {
    //     cblas_dcopy(d, &X[j*d], 1, &diff[0], 1);  // diff = x_j
    //     cblas_daxpy(d, -1, &X[i*d], 1, &diff[0], 1);  // diff = -1*x_i + diff
    //     D[i*n+j] = cblas_dasum(d, &diff[0], 1);  // sum_i |diff_i|
    // }
}

void distance_matrix_one_norm_l (double * const D, double const*const X, double const*const Z, const size_t n, const size_t m, const size_t d)
{
    if( d <= threshold_d )
    {
        // naive implementation
        for(size_t i = 0; i < n; ++i) for(size_t j = 0; j < m; ++j)
        {
            double res = 0;
            for(size_t k = 0; k < d; ++k) res += std::abs(X[i*d+k] - Z[j*d+k]);
            D[i*m+j] = res;
        }
    }
    else
    {
        // BLAS implementation
        double diff[d];  // difference of input vectors
        for(size_t i = 0; i < n; ++i) for(size_t j = 0; j < m; ++j)
        {
            cblas_dcopy(d, &Z[j*d], 1, &diff[0], 1);  // diff = z_j
            cblas_daxpy(d, -1, &X[i*d], 1, &diff[0], 1);  // diff = -1*x_i + diff
            D[i*m+j] = cblas_dasum(d, &diff[0], 1);  // sum_i |diff_i|
        }
    }
}


//  //////////////////////////
//  //  Euclidean distance  //
//  //////////////////////////

void distance_matrix_euclidean_k (double * const D, double const*const X, const size_t n, const size_t d)
{
    // naive implementation
    distance_matrix_squared_euclidean_k(D, X, n, d);
    for(size_t i = 0; i < n*n; ++i) D[i] = std::sqrt(std::abs(D[i]));  // abs is necessary because values can be negative due to small numeric errors
}

void distance_matrix_euclidean_l (double * const D, double const*const X, double const*const Z, const size_t n, const size_t m, const size_t d)
{
    // naive implementation
    distance_matrix_squared_euclidean_l(D, X, Z, n, m, d);
    for(size_t i = 0; i < n; ++i) for(size_t j = 0; j < m; ++j) D[i*m+j] = std::sqrt(std::abs(D[i*m+j]));  // abs is necessary because values can be negative due to small numeric errors
}


//  //////////////////////////////////
//  //  Squared Euclidean distance  //
//  //////////////////////////////////

void distance_matrix_squared_euclidean_k (double * const D, double const*const X, const size_t n, const size_t d)
{
    double * xdots = nullptr;  // squared norms of input vectors

    try
    {
        xdots = new double[n];

        for(size_t i = 0; i < n; ++i) xdots[i] = cblas_ddot(d, &X[i*d], 1, &X[i*d], 1);

        //cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, n, n, d, -2, X, d, X, d, 0, D, n);
        cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, n, n, d, -2, X, d, X, d, 0, D, n);

        for(size_t i = 0; i < n; ++i) for(size_t j = 0; j < n; ++j) D[i*n+j] += xdots[i] + xdots[j];

        delete[] xdots; xdots = nullptr;
    }
    catch(std::bad_alloc const&)
    {
        if( xdots != nullptr ) delete[] xdots;
    }

    // Resorting to the general L case is slower.
    // distance_matrix_squared_euclidean_l(D, X, X, n, n, d);
}

void distance_matrix_squared_euclidean_l (double * const D, double const*const X, double const*const Z, const size_t n, const size_t m, const size_t d)
{
    double * xdots = nullptr;  // squared norms of input vectors
    double * zdots = nullptr;

    try
    {
        xdots = new double[n];
        zdots = new double[m];

        for(size_t i = 0; i < n; ++i) xdots[i] = cblas_ddot(d, &X[i*d], 1, &X[i*d], 1);
        for(size_t j = 0; j < m; ++j) zdots[j] = cblas_ddot(d, &Z[j*d], 1, &Z[j*d], 1);

        //cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, n, m, d, -2, X, d, Z, d, 0, D, m);
        cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, m, n, d, -2, Z, d, X, d, 0, D, m);

        for(size_t i = 0; i < n; ++i) for(size_t j = 0; j < m; ++j)
            D[i*m+j] += xdots[i] + zdots[j];

        delete[] xdots; xdots = nullptr;
        delete[] zdots; zdots = nullptr;
    }
    catch(std::bad_alloc const&)
    {
        if( xdots != nullptr ) delete[] xdots;
        if( zdots != nullptr ) delete[] zdots;
    }

    // Naive implementation is only better for tiny d (<10)
    // for(size_t i = 0; i < n; ++i) for(size_t j = 0; j < m; ++j)
    // { double res = 0; for(size_t k = 0; k < d; ++k) res += (X[i*d+k] - Z[j*d+k]) * (X[i*d+k] - Z[j*d+k]); D[i*m+j] = res; }
}

} // namespace qmml
