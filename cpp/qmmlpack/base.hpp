// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Basic declarations.

// Notes:
// * names starting with _ (followed by capital letter) or containing __ are reserved by C++ implementation
//   -> use namespace qmml::impl to hide symbols

#ifndef QMMLPACK_BASE_HPP_INCLUDED  // Include guard
#define QMMLPACK_BASE_HPP_INCLUDED

#include <stdexcept>
#include <sstream>

// BLAS/LAPACK
#if BLASLAPACK == 1
    #include <Accelerate/Accelerate.h>
#elif BLASLAPACK == 2
    #include <mkl.h>
#elif BLASLAPACK == 3
    #include <gsl_cblas.h>
#else  // Default
    #include <cblas.h>
    #include <lapack.h>
#endif

namespace qmml {

//  //////////////////
//  //  DLL Export  //
//  //////////////////

//  Cross-platform export of library symbols

// - See http://gcc.gnu.org/wiki/Visibility for reasons to selectively export (size, loading time, ...)
// - Other headers such as Mathematica's WolframLibrary redefine DLLEXPORT, so we use a different name
//   The name QMMLAPI emphasizes that the declaration is part of the interface
// - Mark all symbols to export to (and import from) the dynamic library and set default visibility to "hidden" for the compiler
// - All declarations of a symbol must be marked with QMMLAPI, but not the definition
// - Mathematica defines DLLEXPORT as either "__declspec(dllexport)" (Windows) or "__attribute__((__visibility__("default")))" (else)
//   (from /Applications/Mathematica.app/Contents/SystemFiles/IncludeFiles/C/dllexport.h)
// - qmmlpack code should use QMMLAPI, (Mathematica) binding code should use DLLEXPORT

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(WIN64) || defined(_WIN64) || defined(__WIN64__) || defined(_MSC_VER)
    #define QMMLAPI __declspec(dllexport)
#else
    #define QMMLAPI __attribute__ ((visibility ("default")))
#endif

//  //////////////////
//  //  Exceptions  //
//  //////////////////

class QMMLAPI error : public std::exception
{
public:
    explicit error(char const*const msg, char const*const expr, char const*const file, const int line)
    : msg_(msg), expr_(expr), file_(file), line_(line), what_()
    {
        std::ostringstream what;
        what << msg_ << "(" << expr_ << ", " << file_ << ", " << line_ << ")";
        what_ = what.str();
    }

    virtual ~error() throw() {};

    virtual char const* what() const noexcept { return what_.c_str(); }

protected:
    std::string msg_;   // Explanatory message what went wrong, violated condition, ...
    std::string expr_;  // The evaluated expression that caused this error, e.g., as from __func__
    std::string file_;  // The name of the source code file, e.g., as from __FILE__
    int line_;          // The line number within the file, e.g., as from __LINE__
    std::string what_;  // Summary string
};

// Convenience shorthand: if (* goes bad *) throw QMML_ERROR(msg);
#define QMML_ERROR(msg) qmml::error(msg, __func__, __FILE__, __LINE__);

}  // namespace qmml

#endif  // Include guard
