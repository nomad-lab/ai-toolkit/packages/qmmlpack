// qmmlpack
// (c) Matthias Rupp, 2006-2017.
// See LICENSE.txt for license.

// Gaussian kernel

#include <cmath>
#include <algorithm>

#include "qmmlpack/base.hpp"
#include "qmmlpack/distances_pnorm.hpp"
#include "qmmlpack/kernels_gaussian.hpp"

namespace qmml {

//  ///////////////////////
//  //  Gaussian kernel  //
//  ///////////////////////

// The special case sigma = 0 is not handled because it requires either
// distances to be exactly zero for identical samples or a threshold value
// that depends on dimensionality d and magnitude of x_i values.

void kernel_matrix_gaussian_d(double * const L, double const * const D, const size_t n, const size_t m, const double sigma)
{
    if( sigma <= 0 ) QMML_ERROR("invalid value for Gaussian kernel length scale sigma");

    double const c = - 1. / (2*sigma*sigma);
    for(size_t i = 0; i < n*m; ++i) L[i] = std::exp( c * D[i] );
}

void kernel_matrix_gaussian_k(double * const K, double const * const X, const size_t n, const size_t d, const double sigma)
{
    if( sigma <= 0 ) QMML_ERROR("invalid value for Gaussian kernel length scale sigma");

    distance_matrix_squared_euclidean_k(K, X, n, d);
    kernel_matrix_gaussian_d(K, K, n, n, sigma);
}

void kernel_matrix_gaussian_l(double * const L, double const * const X, double const * const Z, const size_t n, const size_t m, const size_t d, const double sigma)
{
    if( sigma <= 0 ) QMML_ERROR("invalid value for Gaussian kernel length scale sigma");

    distance_matrix_squared_euclidean_l(L, X, Z, n, m, d);
    kernel_matrix_gaussian_d(L, L, n, m, sigma);
}

void kernel_matrix_gaussian_m(double * const m, double const * const, const size_t n, const size_t, const double)
{
    std::fill_n(&m[0], n, 1.);
}

} // namespace qmml
